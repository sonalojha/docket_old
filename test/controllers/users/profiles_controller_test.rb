require 'test_helper'

class Users::ProfilesControllerTest < ActionController::TestCase
  setup do
    @users_profile = users_profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users_profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create users_profile" do
    assert_difference('Users::Profile.count') do
      post :create, users_profile: { about: @users_profile.about, cover_img: @users_profile.cover_img, email_verified: @users_profile.email_verified, first_name: @users_profile.first_name, last_login: @users_profile.last_login, last_name: @users_profile.last_name, middle_name: @users_profile.middle_name, mobile_verified: @users_profile.mobile_verified, profile_img: @users_profile.profile_img }
    end

    assert_redirected_to users_profile_path(assigns(:users_profile))
  end

  test "should show users_profile" do
    get :show, id: @users_profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @users_profile
    assert_response :success
  end

  test "should update users_profile" do
    patch :update, id: @users_profile, users_profile: { about: @users_profile.about, cover_img: @users_profile.cover_img, email_verified: @users_profile.email_verified, first_name: @users_profile.first_name, last_login: @users_profile.last_login, last_name: @users_profile.last_name, middle_name: @users_profile.middle_name, mobile_verified: @users_profile.mobile_verified, profile_img: @users_profile.profile_img }
    assert_redirected_to users_profile_path(assigns(:users_profile))
  end

  test "should destroy users_profile" do
    assert_difference('Users::Profile.count', -1) do
      delete :destroy, id: @users_profile
    end

    assert_redirected_to users_profiles_path
  end
end
