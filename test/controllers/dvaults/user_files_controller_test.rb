require 'test_helper'

class Dvaults::UserFilesControllerTest < ActionController::TestCase
  setup do
    @dvaults_user_file = dvaults_user_files(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dvaults_user_files)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dvaults_user_file" do
    assert_difference('Dvaults::UserFile.count') do
      post :create, dvaults_user_file: { dvault_id: @dvaults_user_file.dvault_id, filename: @dvaults_user_file.filename, mime_type: @dvaults_user_file.mime_type, profile_id: @dvaults_user_file.profile_id, size: @dvaults_user_file.size, tag: @dvaults_user_file.tag, thumbnail_image: @dvaults_user_file.thumbnail_image, uid: @dvaults_user_file.uid }
    end

    assert_redirected_to dvaults_user_file_path(assigns(:dvaults_user_file))
  end

  test "should show dvaults_user_file" do
    get :show, id: @dvaults_user_file
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dvaults_user_file
    assert_response :success
  end

  test "should update dvaults_user_file" do
    patch :update, id: @dvaults_user_file, dvaults_user_file: { dvault_id: @dvaults_user_file.dvault_id, filename: @dvaults_user_file.filename, mime_type: @dvaults_user_file.mime_type, profile_id: @dvaults_user_file.profile_id, size: @dvaults_user_file.size, tag: @dvaults_user_file.tag, thumbnail_image: @dvaults_user_file.thumbnail_image, uid: @dvaults_user_file.uid }
    assert_redirected_to dvaults_user_file_path(assigns(:dvaults_user_file))
  end

  test "should destroy dvaults_user_file" do
    assert_difference('Dvaults::UserFile.count', -1) do
      delete :destroy, id: @dvaults_user_file
    end

    assert_redirected_to dvaults_user_files_path
  end
end
