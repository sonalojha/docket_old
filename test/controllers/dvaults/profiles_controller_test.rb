require 'test_helper'

class Dvaults::ProfilesControllerTest < ActionController::TestCase
  setup do
    @dvaults_profile = dvaults_profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dvaults_profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dvaults_profile" do
    assert_difference('Dvaults::Profile.count') do
      post :create, dvaults_profile: { dvault_id: @dvaults_profile.dvault_id, name: @dvaults_profile.name }
    end

    assert_redirected_to dvaults_profile_path(assigns(:dvaults_profile))
  end

  test "should show dvaults_profile" do
    get :show, id: @dvaults_profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dvaults_profile
    assert_response :success
  end

  test "should update dvaults_profile" do
    patch :update, id: @dvaults_profile, dvaults_profile: { dvault_id: @dvaults_profile.dvault_id, name: @dvaults_profile.name }
    assert_redirected_to dvaults_profile_path(assigns(:dvaults_profile))
  end

  test "should destroy dvaults_profile" do
    assert_difference('Dvaults::Profile.count', -1) do
      delete :destroy, id: @dvaults_profile
    end

    assert_redirected_to dvaults_profiles_path
  end
end
