require 'test_helper'

class Dvaults::CategoriesControllerTest < ActionController::TestCase
  setup do
    @dvaults_category = dvaults_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dvaults_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dvaults_category" do
    assert_difference('Dvaults::Category.count') do
      post :create, dvaults_category: { name: @dvaults_category.name }
    end

    assert_redirected_to dvaults_category_path(assigns(:dvaults_category))
  end

  test "should show dvaults_category" do
    get :show, id: @dvaults_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dvaults_category
    assert_response :success
  end

  test "should update dvaults_category" do
    patch :update, id: @dvaults_category, dvaults_category: { name: @dvaults_category.name }
    assert_redirected_to dvaults_category_path(assigns(:dvaults_category))
  end

  test "should destroy dvaults_category" do
    assert_difference('Dvaults::Category.count', -1) do
      delete :destroy, id: @dvaults_category
    end

    assert_redirected_to dvaults_categories_path
  end
end
