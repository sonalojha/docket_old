require 'test_helper'

class Dvaults::SubCategoriesControllerTest < ActionController::TestCase
  setup do
    @dvaults_sub_category = dvaults_sub_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dvaults_sub_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dvaults_sub_category" do
    assert_difference('Dvaults::SubCategory.count') do
      post :create, dvaults_sub_category: { category_id: @dvaults_sub_category.category_id, name: @dvaults_sub_category.name }
    end

    assert_redirected_to dvaults_sub_category_path(assigns(:dvaults_sub_category))
  end

  test "should show dvaults_sub_category" do
    get :show, id: @dvaults_sub_category
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dvaults_sub_category
    assert_response :success
  end

  test "should update dvaults_sub_category" do
    patch :update, id: @dvaults_sub_category, dvaults_sub_category: { category_id: @dvaults_sub_category.category_id, name: @dvaults_sub_category.name }
    assert_redirected_to dvaults_sub_category_path(assigns(:dvaults_sub_category))
  end

  test "should destroy dvaults_sub_category" do
    assert_difference('Dvaults::SubCategory.count', -1) do
      delete :destroy, id: @dvaults_sub_category
    end

    assert_redirected_to dvaults_sub_categories_path
  end
end
