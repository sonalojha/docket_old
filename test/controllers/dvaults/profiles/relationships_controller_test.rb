require 'test_helper'

class Dvaults::Profiles::RelationshipsControllerTest < ActionController::TestCase
  setup do
    @dvaults_profiles_relationship = dvaults_profiles_relationships(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dvaults_profiles_relationships)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dvaults_profiles_relationship" do
    assert_difference('Dvaults::Profiles::Relationship.count') do
      post :create, dvaults_profiles_relationship: { name: @dvaults_profiles_relationship.name }
    end

    assert_redirected_to dvaults_profiles_relationship_path(assigns(:dvaults_profiles_relationship))
  end

  test "should show dvaults_profiles_relationship" do
    get :show, id: @dvaults_profiles_relationship
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dvaults_profiles_relationship
    assert_response :success
  end

  test "should update dvaults_profiles_relationship" do
    patch :update, id: @dvaults_profiles_relationship, dvaults_profiles_relationship: { name: @dvaults_profiles_relationship.name }
    assert_redirected_to dvaults_profiles_relationship_path(assigns(:dvaults_profiles_relationship))
  end

  test "should destroy dvaults_profiles_relationship" do
    assert_difference('Dvaults::Profiles::Relationship.count', -1) do
      delete :destroy, id: @dvaults_profiles_relationship
    end

    assert_redirected_to dvaults_profiles_relationships_path
  end
end
