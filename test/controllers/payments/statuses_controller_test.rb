require 'test_helper'

class Payments::StatusesControllerTest < ActionController::TestCase
  setup do
    @payments_status = payments_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:payments_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create payments_status" do
    assert_difference('Payments::Status.count') do
      post :create, payments_status: { name: @payments_status.name }
    end

    assert_redirected_to payments_status_path(assigns(:payments_status))
  end

  test "should show payments_status" do
    get :show, id: @payments_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @payments_status
    assert_response :success
  end

  test "should update payments_status" do
    patch :update, id: @payments_status, payments_status: { name: @payments_status.name }
    assert_redirected_to payments_status_path(assigns(:payments_status))
  end

  test "should destroy payments_status" do
    assert_difference('Payments::Status.count', -1) do
      delete :destroy, id: @payments_status
    end

    assert_redirected_to payments_statuses_path
  end
end
