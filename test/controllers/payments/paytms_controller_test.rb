require 'test_helper'

class Payments::PaytmsControllerTest < ActionController::TestCase
  setup do
    @payments_paytm = payments_paytms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:payments_paytms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create payments_paytm" do
    assert_difference('Payments::Paytm.count') do
      post :create, payments_paytm: { BANKNAME: @payments_paytm.BANKNAME, BANKTXNID: @payments_paytm.BANKTXNID, CHECKSUMHASH: @payments_paytm.CHECKSUMHASH, CURRENCY: @payments_paytm.CURRENCY, GATEWAYNAME: @payments_paytm.GATEWAYNAME, MID: @payments_paytm.MID, ORDERID: @payments_paytm.ORDERID, PAYMENTMODE: @payments_paytm.PAYMENTMODE, RESPCODE: @payments_paytm.RESPCODE, RESPMSG: @payments_paytm.RESPMSG, STATUS: @payments_paytm.STATUS, TXNAMOUNT: @payments_paytm.TXNAMOUNT, TXNDATE: @payments_paytm.TXNDATE, TXNID: @payments_paytm.TXNID }
    end

    assert_redirected_to payments_paytm_path(assigns(:payments_paytm))
  end

  test "should show payments_paytm" do
    get :show, id: @payments_paytm
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @payments_paytm
    assert_response :success
  end

  test "should update payments_paytm" do
    patch :update, id: @payments_paytm, payments_paytm: { BANKNAME: @payments_paytm.BANKNAME, BANKTXNID: @payments_paytm.BANKTXNID, CHECKSUMHASH: @payments_paytm.CHECKSUMHASH, CURRENCY: @payments_paytm.CURRENCY, GATEWAYNAME: @payments_paytm.GATEWAYNAME, MID: @payments_paytm.MID, ORDERID: @payments_paytm.ORDERID, PAYMENTMODE: @payments_paytm.PAYMENTMODE, RESPCODE: @payments_paytm.RESPCODE, RESPMSG: @payments_paytm.RESPMSG, STATUS: @payments_paytm.STATUS, TXNAMOUNT: @payments_paytm.TXNAMOUNT, TXNDATE: @payments_paytm.TXNDATE, TXNID: @payments_paytm.TXNID }
    assert_redirected_to payments_paytm_path(assigns(:payments_paytm))
  end

  test "should destroy payments_paytm" do
    assert_difference('Payments::Paytm.count', -1) do
      delete :destroy, id: @payments_paytm
    end

    assert_redirected_to payments_paytms_path
  end
end
