require 'test_helper'

class Payments::IcicisControllerTest < ActionController::TestCase
  setup do
    @payments_icici = payments_icicis(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:payments_icicis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create payments_icici" do
    assert_difference('Payments::Icici.count') do
      post :create, payments_icici: { approval_code: @payments_icici.approval_code, bname: @payments_icici.bname, cardnumber: @payments_icici.cardnumber, ccbin: @payments_icici.ccbin, ccbrand: @payments_icici.ccbrand, cccountry: @payments_icici.cccountry, chargetotal: @payments_icici.chargetotal, currency: @payments_icici.currency, endpointTransactionId: @payments_icici.endpointTransactionId, expmonth: @payments_icici.expmonth, expyear: @payments_icici.expyear, ipgTransactionId: @payments_icici.ipgTransactionId, oid: @payments_icici.oid, paymentMethod: @payments_icici.paymentMethod, processor_response_code: @payments_icici.processor_response_code, responseHash_validationFrom: @payments_icici.responseHash_validationFrom, response_code_3dsecure: @payments_icici.response_code_3dsecure, response_dump: @payments_icici.response_dump, response_hash: @payments_icici.response_hash, response_hash_validation: @payments_icici.response_hash_validation, status: @payments_icici.status, tdate: @payments_icici.tdate, terminal_id: @payments_icici.terminal_id, timezone: @payments_icici.timezone, transactionNotificationURL: @payments_icici.transactionNotificationURL, txndate_processed: @payments_icici.txndate_processed, txndatetime: @payments_icici.txndatetime, txnid: @payments_icici.txnid, txntype: @payments_icici.txntype }
    end

    assert_redirected_to payments_icici_path(assigns(:payments_icici))
  end

  test "should show payments_icici" do
    get :show, id: @payments_icici
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @payments_icici
    assert_response :success
  end

  test "should update payments_icici" do
    patch :update, id: @payments_icici, payments_icici: { approval_code: @payments_icici.approval_code, bname: @payments_icici.bname, cardnumber: @payments_icici.cardnumber, ccbin: @payments_icici.ccbin, ccbrand: @payments_icici.ccbrand, cccountry: @payments_icici.cccountry, chargetotal: @payments_icici.chargetotal, currency: @payments_icici.currency, endpointTransactionId: @payments_icici.endpointTransactionId, expmonth: @payments_icici.expmonth, expyear: @payments_icici.expyear, ipgTransactionId: @payments_icici.ipgTransactionId, oid: @payments_icici.oid, paymentMethod: @payments_icici.paymentMethod, processor_response_code: @payments_icici.processor_response_code, responseHash_validationFrom: @payments_icici.responseHash_validationFrom, response_code_3dsecure: @payments_icici.response_code_3dsecure, response_dump: @payments_icici.response_dump, response_hash: @payments_icici.response_hash, response_hash_validation: @payments_icici.response_hash_validation, status: @payments_icici.status, tdate: @payments_icici.tdate, terminal_id: @payments_icici.terminal_id, timezone: @payments_icici.timezone, transactionNotificationURL: @payments_icici.transactionNotificationURL, txndate_processed: @payments_icici.txndate_processed, txndatetime: @payments_icici.txndatetime, txnid: @payments_icici.txnid, txntype: @payments_icici.txntype }
    assert_redirected_to payments_icici_path(assigns(:payments_icici))
  end

  test "should destroy payments_icici" do
    assert_difference('Payments::Icici.count', -1) do
      delete :destroy, id: @payments_icici
    end

    assert_redirected_to payments_icicis_path
  end
end
