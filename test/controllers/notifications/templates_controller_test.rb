require 'test_helper'

class Notifications::TemplatesControllerTest < ActionController::TestCase
  setup do
    @notifications_template = notifications_templates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:notifications_templates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create notifications_template" do
    assert_difference('Notifications::Template.count') do
      post :create, notifications_template: { msg: @notifications_template.msg, name: @notifications_template.name }
    end

    assert_redirected_to notifications_template_path(assigns(:notifications_template))
  end

  test "should show notifications_template" do
    get :show, id: @notifications_template
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @notifications_template
    assert_response :success
  end

  test "should update notifications_template" do
    patch :update, id: @notifications_template, notifications_template: { msg: @notifications_template.msg, name: @notifications_template.name }
    assert_redirected_to notifications_template_path(assigns(:notifications_template))
  end

  test "should destroy notifications_template" do
    assert_difference('Notifications::Template.count', -1) do
      delete :destroy, id: @notifications_template
    end

    assert_redirected_to notifications_templates_path
  end
end
