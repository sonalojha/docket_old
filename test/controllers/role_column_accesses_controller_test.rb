require 'test_helper'

class RoleColumnAccessesControllerTest < ActionController::TestCase
  setup do
    @role_column_access = role_column_accesses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:role_column_accesses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create role_column_access" do
    assert_difference('RoleColumnAccess.count') do
      post :create, role_column_access: { column_name: @role_column_access.column_name, role_id: @role_column_access.role_id, table_name: @role_column_access.table_name }
    end

    assert_redirected_to role_column_access_path(assigns(:role_column_access))
  end

  test "should show role_column_access" do
    get :show, id: @role_column_access
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @role_column_access
    assert_response :success
  end

  test "should update role_column_access" do
    patch :update, id: @role_column_access, role_column_access: { column_name: @role_column_access.column_name, role_id: @role_column_access.role_id, table_name: @role_column_access.table_name }
    assert_redirected_to role_column_access_path(assigns(:role_column_access))
  end

  test "should destroy role_column_access" do
    assert_difference('RoleColumnAccess.count', -1) do
      delete :destroy, id: @role_column_access
    end

    assert_redirected_to role_column_accesses_path
  end
end
