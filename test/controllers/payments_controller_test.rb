require 'test_helper'

class PaymentsControllerTest < ActionController::TestCase
  setup do
    @payment = payments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:payments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create payment" do
    assert_difference('Payment.count') do
      post :create, payment: { amount: @payment.amount, bank_ref_num: @payment.bank_ref_num, bankcode: @payment.bankcode, cardnum: @payment.cardnum, discount: @payment.discount, hash: @payment.hash, mihpayid: @payment.mihpayid, mode: @payment.mode, name_on_card: @payment.name_on_card, payuMoneyId: @payment.payuMoneyId, pg_type: @payment.pg_type, status: @payment.status }
    end

    assert_redirected_to payment_path(assigns(:payment))
  end

  test "should show payment" do
    get :show, id: @payment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @payment
    assert_response :success
  end

  test "should update payment" do
    patch :update, id: @payment, payment: { amount: @payment.amount, bank_ref_num: @payment.bank_ref_num, bankcode: @payment.bankcode, cardnum: @payment.cardnum, discount: @payment.discount, hash: @payment.hash, mihpayid: @payment.mihpayid, mode: @payment.mode, name_on_card: @payment.name_on_card, payuMoneyId: @payment.payuMoneyId, pg_type: @payment.pg_type, status: @payment.status }
    assert_redirected_to payment_path(assigns(:payment))
  end

  test "should destroy payment" do
    assert_difference('Payment.count', -1) do
      delete :destroy, id: @payment
    end

    assert_redirected_to payments_path
  end
end
