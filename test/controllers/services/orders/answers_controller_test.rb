require 'test_helper'

class Services::Orders::AnswersControllerTest < ActionController::TestCase
  setup do
    @services_orders_answer = services_orders_answers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:services_orders_answers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create services_orders_answer" do
    assert_difference('Services::Orders::Answer.count') do
      post :create, services_orders_answer: { body: @services_orders_answer.body, order_id: @services_orders_answer.order_id, question_id: @services_orders_answer.question_id }
    end

    assert_redirected_to services_orders_answer_path(assigns(:services_orders_answer))
  end

  test "should show services_orders_answer" do
    get :show, id: @services_orders_answer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @services_orders_answer
    assert_response :success
  end

  test "should update services_orders_answer" do
    patch :update, id: @services_orders_answer, services_orders_answer: { body: @services_orders_answer.body, order_id: @services_orders_answer.order_id, question_id: @services_orders_answer.question_id }
    assert_redirected_to services_orders_answer_path(assigns(:services_orders_answer))
  end

  test "should destroy services_orders_answer" do
    assert_difference('Services::Orders::Answer.count', -1) do
      delete :destroy, id: @services_orders_answer
    end

    assert_redirected_to services_orders_answers_path
  end
end
