require 'test_helper'

class Services::QuestionsControllerTest < ActionController::TestCase
  setup do
    @services_question = services_questions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:services_questions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create services_question" do
    assert_difference('Services::Question.count') do
      post :create, services_question: { api_url: @services_question.api_url, name: @services_question.name, service_id: @services_question.service_id, type: @services_question.type }
    end

    assert_redirected_to services_question_path(assigns(:services_question))
  end

  test "should show services_question" do
    get :show, id: @services_question
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @services_question
    assert_response :success
  end

  test "should update services_question" do
    patch :update, id: @services_question, services_question: { api_url: @services_question.api_url, name: @services_question.name, service_id: @services_question.service_id, type: @services_question.type }
    assert_redirected_to services_question_path(assigns(:services_question))
  end

  test "should destroy services_question" do
    assert_difference('Services::Question.count', -1) do
      delete :destroy, id: @services_question
    end

    assert_redirected_to services_questions_path
  end
end
