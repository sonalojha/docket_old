require 'test_helper'

class Orders::FeedbacksControllerTest < ActionController::TestCase
  setup do
    @orders_feedback = orders_feedbacks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orders_feedbacks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orders_feedback" do
    assert_difference('Orders::Feedback.count') do
      post :create, orders_feedback: { message: @orders_feedback.message, note: @orders_feedback.note, order_id: @orders_feedback.order_id, status_id: @orders_feedback.status_id }
    end

    assert_redirected_to orders_feedback_path(assigns(:orders_feedback))
  end

  test "should show orders_feedback" do
    get :show, id: @orders_feedback
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @orders_feedback
    assert_response :success
  end

  test "should update orders_feedback" do
    patch :update, id: @orders_feedback, orders_feedback: { message: @orders_feedback.message, note: @orders_feedback.note, order_id: @orders_feedback.order_id, status_id: @orders_feedback.status_id }
    assert_redirected_to orders_feedback_path(assigns(:orders_feedback))
  end

  test "should destroy orders_feedback" do
    assert_difference('Orders::Feedback.count', -1) do
      delete :destroy, id: @orders_feedback
    end

    assert_redirected_to orders_feedbacks_path
  end
end
