require 'test_helper'

class Orders::DeliveryStatusesControllerTest < ActionController::TestCase
  setup do
    @orders_delivery_status = orders_delivery_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orders_delivery_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orders_delivery_status" do
    assert_difference('Orders::DeliveryStatus.count') do
      post :create, orders_delivery_status: { name: @orders_delivery_status.name }
    end

    assert_redirected_to orders_delivery_status_path(assigns(:orders_delivery_status))
  end

  test "should show orders_delivery_status" do
    get :show, id: @orders_delivery_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @orders_delivery_status
    assert_response :success
  end

  test "should update orders_delivery_status" do
    patch :update, id: @orders_delivery_status, orders_delivery_status: { name: @orders_delivery_status.name }
    assert_redirected_to orders_delivery_status_path(assigns(:orders_delivery_status))
  end

  test "should destroy orders_delivery_status" do
    assert_difference('Orders::DeliveryStatus.count', -1) do
      delete :destroy, id: @orders_delivery_status
    end

    assert_redirected_to orders_delivery_statuses_path
  end
end
