require 'test_helper'

class Orders::TracksControllerTest < ActionController::TestCase
  setup do
    @orders_track = orders_tracks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orders_tracks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orders_track" do
    assert_difference('Orders::Track.count') do
      post :create, orders_track: {  }
    end

    assert_redirected_to orders_track_path(assigns(:orders_track))
  end

  test "should show orders_track" do
    get :show, id: @orders_track
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @orders_track
    assert_response :success
  end

  test "should update orders_track" do
    patch :update, id: @orders_track, orders_track: {  }
    assert_redirected_to orders_track_path(assigns(:orders_track))
  end

  test "should destroy orders_track" do
    assert_difference('Orders::Track.count', -1) do
      delete :destroy, id: @orders_track
    end

    assert_redirected_to orders_tracks_path
  end
end
