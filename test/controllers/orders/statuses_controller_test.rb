require 'test_helper'

class Orders::StatusesControllerTest < ActionController::TestCase
  setup do
    @orders_status = orders_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orders_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orders_status" do
    assert_difference('Orders::Status.count') do
      post :create, orders_status: { name: @orders_status.name }
    end

    assert_redirected_to orders_status_path(assigns(:orders_status))
  end

  test "should show orders_status" do
    get :show, id: @orders_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @orders_status
    assert_response :success
  end

  test "should update orders_status" do
    patch :update, id: @orders_status, orders_status: { name: @orders_status.name }
    assert_redirected_to orders_status_path(assigns(:orders_status))
  end

  test "should destroy orders_status" do
    assert_difference('Orders::Status.count', -1) do
      delete :destroy, id: @orders_status
    end

    assert_redirected_to orders_statuses_path
  end
end
