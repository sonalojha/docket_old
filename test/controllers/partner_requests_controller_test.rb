require 'test_helper'

class PartnerRequestsControllerTest < ActionController::TestCase
  setup do
    @partner_request = partner_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:partner_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create partner_request" do
    assert_difference('PartnerRequest.count') do
      post :create, partner_request: { address: @partner_request.address, callback_request: @partner_request.callback_request, email: @partner_request.email, message: @partner_request.message, name: @partner_request.name, note: @partner_request.note, phone: @partner_request.phone, profession: @partner_request.profession, status_id: @partner_request.status_id }
    end

    assert_redirected_to partner_request_path(assigns(:partner_request))
  end

  test "should show partner_request" do
    get :show, id: @partner_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @partner_request
    assert_response :success
  end

  test "should update partner_request" do
    patch :update, id: @partner_request, partner_request: { address: @partner_request.address, callback_request: @partner_request.callback_request, email: @partner_request.email, message: @partner_request.message, name: @partner_request.name, note: @partner_request.note, phone: @partner_request.phone, profession: @partner_request.profession, status_id: @partner_request.status_id }
    assert_redirected_to partner_request_path(assigns(:partner_request))
  end

  test "should destroy partner_request" do
    assert_difference('PartnerRequest.count', -1) do
      delete :destroy, id: @partner_request
    end

    assert_redirected_to partner_requests_path
  end
end
