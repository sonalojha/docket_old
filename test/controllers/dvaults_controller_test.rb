require 'test_helper'

class DvaultsControllerTest < ActionController::TestCase
  setup do
    @dvault = dvaults(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dvaults)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dvault" do
    assert_difference('Dvault.count') do
      post :create, dvault: { pin: @dvault.pin, user_id: @dvault.user_id }
    end

    assert_redirected_to dvault_path(assigns(:dvault))
  end

  test "should show dvault" do
    get :show, id: @dvault
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dvault
    assert_response :success
  end

  test "should update dvault" do
    patch :update, id: @dvault, dvault: { pin: @dvault.pin, user_id: @dvault.user_id }
    assert_redirected_to dvault_path(assigns(:dvault))
  end

  test "should destroy dvault" do
    assert_difference('Dvault.count', -1) do
      delete :destroy, id: @dvault
    end

    assert_redirected_to dvaults_path
  end
end
