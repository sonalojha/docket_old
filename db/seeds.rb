# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
data = ['NEW', "ACTIVE", "INACTIVE", "DELETE"]
data.each do |c|
	status = Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

puts "Finished Status"

country = Country.find_or_initialize_by(name: 'India', url_name: 'in', phone_code: '91')
country.save!

state = State.find_or_initialize_by(name: 'Karnataka', url_name: 'ka', country_id: country.id)
state.save!

city = City.find_or_initialize_by(name:'Bangalore', url_name: 'bangalore', state_id: state.id, status_id: 2)
city.save!

delivery_partner = Orders::Delivery::Partner.find_or_initialize_by(name:'FedEx')
delivery_partner.save!

type_of_service = [
	{ 	name:"Affidavit",
		priority: 1,
		min_stamp: 50
	},
	{ 	name:"Bond",
		priority: 2,
		min_stamp: 100
	},
	{ 	name: "Agreement",
		priority: 3,
		min_stamp: 100
	},
	{ 	name: "Deed",
		priority: 4,
		min_stamp: 100
	},
	{
		name: "Startup",
	    priority: 5,
		min_stamp: 0
	},
	{
		name: "Marketplace",
		priority: 6,
		min_stamp: 0
	},
	{ 
	  	name: "On-Demand",
	  	priority: 7,
		min_stamp: 0
	},
	{ 
	  	name: "Attestation",
	  	priority: 8,
		min_stamp: 0
	},
	{ 
	  	name: "Notary",
	  	priority: 9,
		min_stamp: 0
	},
	{ 
	  	name: "Franking",
	  	priority: 10,
		min_stamp: 100
	},
	{ 
		name: "E-Stamp",
		priority: 11,
		min_stamp: 50
	},
	{ 
	  	name: "Paperwork",
	  	priority: 12,
		min_stamp: 0
	},
	{ 
	  	name: "Property",
	  	priority: 13,
		min_stamp: 0
	},
	]


type_of_service.each do |t|
	service_type = Service.find_or_initialize_by(name: t[:name])
	service_type.priority = t[:priority] if service_type.priority.nil?
	service_type.min_stamp = t[:min_stamp] if service_type.min_stamp.nil?
	if service_type.new_record?
		service_type.save
	else
		puts "#{t} exists"
	end
	service_type.save
end

data = ["INCOMPLETE", "INPROGRESS", 'COMPLETED', "DELETED"]
data.each do |c|
	status = Orders::Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

puts "Finished Order Status"

data = ['DISPATCHED', "DELIVERED", 'RETURNED']
data.each do |c|
	status = Orders::DeliveryStatus.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

puts "Finished Delivery Status"

data = ['SUCCESS', 'CANCELED', "REFUND", "FAILED"]
data.each do |c|
	status = Payments::Status.find_or_initialize_by(name: c)
	if status.new_record?
		status.save!
	else
		puts "#{c} exists"
	end
end

puts "Finished Payment Status"

data = ['Website', "Android", 'Ios']
data.each do |c|
	platform = Platform.find_or_initialize_by(name: c)
	if platform.new_record?
		platform.save!
	else
		puts "#{c} exists"
	end
end

basic_data = [ 
	{
		name: 'ios_version',
		value: '1'
	},
	{
		name: 'android_version',
		value: '5'
	},
	{
		name: 'api_version',
		value: '1'
	},
	{
		name: 'android_url',
		value: 'https://play.google.com/store/apps/details?id=com.docket'
	},
	{
		name: 'ios_url',
		value: 'https://appsto.re/in/sSvqab.i'
	}
]
basic_data.each do |t|
	app = App.find_or_initialize_by(name: t[:name])
	app.value = t[:value] if app.value.nil?
	if app.new_record?
		app.save!
	else
		puts "#{t} exists"
	end
	app.save!
end

Dir.glob("#{Rails.root}/db/seeds/*.rb").each { |f| require f }
