# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170803053830) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "locality_id"
    t.integer  "city_id"
    t.string   "landmark"
    t.string   "address1"
    t.string   "address2"
    t.string   "tag"
    t.integer  "status_id"
    t.integer  "user_id"
    t.string   "pincode"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "order_id"
  end

  add_index "addresses", ["city_id"], name: "index_addresses_on_city_id", using: :btree
  add_index "addresses", ["locality_id"], name: "index_addresses_on_locality_id", using: :btree
  add_index "addresses", ["tag", "user_id"], name: "index_addresses_on_tag_and_user_id", using: :btree
  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "apps", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "s3_url"
    t.integer  "file_id"
    t.string   "file_type"
    t.string   "tag"
    t.string   "name"
    t.boolean  "is_processed", default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "mime_type"
  end

  add_index "attachments", ["file_id", "file_type"], name: "index_attachments_on_file_id_and_file_type", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.string   "url_name"
    t.integer  "state_id"
    t.integer  "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree
  add_index "cities", ["status_id"], name: "index_cities_on_status_id", using: :btree

  create_table "contact_requests", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.string   "request_type"
    t.text     "note"
    t.integer  "status_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "contact_requests", ["status_id"], name: "index_contact_requests_on_status_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.string   "url_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "phone_code"
  end

  add_index "countries", ["name"], name: "index_countries_on_name", using: :btree

  create_table "coupons", force: :cascade do |t|
    t.string   "code"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "coupon_type", default: "fixed"
    t.string   "value"
    t.integer  "limit",       default: -1
    t.integer  "count",       default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "active",      default: true
  end

  create_table "device_registrations", force: :cascade do |t|
    t.string   "device_id"
    t.text     "registration_key"
    t.integer  "user_id"
    t.integer  "platform_id"
    t.string   "email"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "device_registrations", ["platform_id"], name: "index_device_registrations_on_platform_id", using: :btree
  add_index "device_registrations", ["user_id"], name: "index_device_registrations_on_user_id", using: :btree

  create_table "documents", force: :cascade do |t|
    t.string   "name"
    t.integer  "service_id"
    t.boolean  "stamp_required", default: false
    t.string   "default_price"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "tat"
    t.boolean  "automated",      default: false
    t.string   "template_url"
    t.integer  "status_id"
    t.text     "description"
    t.integer  "priority"
  end

  add_index "documents", ["service_id"], name: "index_documents_on_service_id", using: :btree
  add_index "documents", ["status_id"], name: "index_documents_on_status_id", using: :btree

  create_table "documents_answers", force: :cascade do |t|
    t.integer  "document_id"
    t.integer  "section_id"
    t.integer  "question_id"
    t.text     "body"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "order_id",    null: false
  end

  add_index "documents_answers", ["document_id"], name: "index_documents_answers_on_document_id", using: :btree
  add_index "documents_answers", ["question_id"], name: "index_documents_answers_on_question_id", using: :btree

  create_table "documents_questions", force: :cascade do |t|
    t.integer  "response_type_id"
    t.boolean  "is_mandatory"
    t.text     "body"
    t.text     "associated_text"
    t.string   "placeholder"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "document_id"
  end

  add_index "documents_questions", ["document_id"], name: "index_documents_questions_on_document_id", using: :btree
  add_index "documents_questions", ["response_type_id"], name: "index_documents_questions_on_response_type_id", using: :btree

  create_table "documents_questions_options", force: :cascade do |t|
    t.string   "name"
    t.string   "question_id"
    t.integer  "priority"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "documents_questions_options", ["question_id"], name: "index_documents_questions_options_on_question_id", using: :btree

  create_table "documents_questions_response_types", force: :cascade do |t|
    t.string   "name"
    t.string   "regex"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents_sections", force: :cascade do |t|
    t.string   "name"
    t.integer  "section_no"
    t.text     "description"
    t.integer  "document_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "documents_sections", ["document_id"], name: "index_documents_sections_on_document_id", using: :btree

  create_table "documents_sections_question_groups", force: :cascade do |t|
    t.integer  "section_id"
    t.integer  "question_id"
    t.integer  "priority"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "documents_sections_question_groups", ["question_id"], name: "index_documents_sections_question_groups_on_question_id", using: :btree
  add_index "documents_sections_question_groups", ["section_id"], name: "index_documents_sections_question_groups_on_section_id", using: :btree

  create_table "dvaults", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "pin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "dvaults", ["user_id"], name: "index_dvaults_on_user_id", using: :btree

  create_table "dvaults_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dvaults_profiles", force: :cascade do |t|
    t.string   "name"
    t.integer  "relationship_id"
    t.integer  "dvault_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "dvaults_profiles", ["relationship_id"], name: "index_dvaults_profiles_on_relationship_id", using: :btree

  create_table "dvaults_profiles_relationships", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dvaults_sub_categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "dvaults_sub_categories", ["category_id"], name: "index_dvaults_sub_categories_on_category_id", using: :btree

  create_table "dvaults_user_files", force: :cascade do |t|
    t.string   "filename"
    t.string   "tag"
    t.string   "mime_type"
    t.string   "vault_uid"
    t.string   "size"
    t.integer  "profile_id"
    t.integer  "dvault_id"
    t.string   "thumbnail_image"
    t.integer  "sub_category_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "dvaults_user_files", ["dvault_id"], name: "index_dvaults_user_files_on_dvault_id", using: :btree
  add_index "dvaults_user_files", ["profile_id"], name: "index_dvaults_user_files_on_profile_id", using: :btree
  add_index "dvaults_user_files", ["sub_category_id"], name: "index_dvaults_user_files_on_sub_category_id", using: :btree

  create_table "estamps_orders", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "lat"
    t.float    "lng"
    t.string   "total_amount"
    t.integer  "total_quantity"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "status_id"
    t.integer  "vendor_id"
    t.string   "txnid"
    t.integer  "address_id"
    t.integer  "payment_id"
    t.string   "delivered_by"
    t.string   "tracking_no"
    t.integer  "delivery_status_id"
    t.string   "platform"
    t.string   "product"
    t.integer  "delivery_charge"
  end

  create_table "estamps_orders_answers", force: :cascade do |t|
    t.string   "first_party"
    t.string   "second_party"
    t.text     "description"
    t.string   "purchased_by"
    t.integer  "item_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "purchaser"
    t.string   "hf_name"
    t.string   "for_whom"
    t.string   "sno"
    t.string   "relationship"
  end

  create_table "estamps_orders_details", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "mobile"
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "id_type"
    t.string   "id_no"
  end

  create_table "estamps_orders_items", force: :cascade do |t|
    t.integer  "quantity"
    t.integer  "order_id"
    t.integer  "vendor_product_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "product_id"
  end

  create_table "estamps_orders_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estamps_products", force: :cascade do |t|
    t.string   "name"
    t.integer  "price"
    t.integer  "service_charge"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "estamps_vendor_products", force: :cascade do |t|
    t.integer  "vendor_id"
    t.integer  "product_id"
    t.integer  "stock_quantity"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "estamps_vendors", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "address1"
    t.string   "address2"
    t.string   "landmark"
    t.string   "city_id"
    t.string   "state_id"
    t.string   "locality_id"
    t.string   "pincode"
    t.float    "lat"
    t.float    "lng"
    t.string   "status_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "delivery_time",      default: "2 or 3 hours"
    t.string   "estamp_vendor_type", default: "Stamp Vendor"
    t.string   "tech_savy"
  end

  create_table "estamps_vendors_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "localities", force: :cascade do |t|
    t.string   "name"
    t.integer  "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "pincode"
  end

  add_index "localities", ["city_id"], name: "index_localities_on_city_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.boolean  "read"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "template_id"
    t.string   "notice_type"
    t.integer  "order_id"
  end

  add_index "notifications", ["order_id"], name: "index_notifications_on_order_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "notifications_templates", force: :cascade do |t|
    t.string   "name"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "document_id"
    t.integer  "user_id"
    t.integer  "status_id"
    t.string   "stamp_amount"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "service_id"
    t.string   "total_amount"
    t.string   "delivery_amount"
    t.string   "txnid"
    t.string   "invoice_url"
    t.boolean  "draft_confirmed",      default: false
    t.string   "soft_copy_url"
    t.boolean  "draft_required",       default: true
    t.integer  "delivery_status_id"
    t.integer  "delivery_partner_id"
    t.string   "delivery_tracking_no"
    t.string   "product"
    t.integer  "platform_id",          default: 1
    t.integer  "address_id"
    t.string   "pickup_amount"
    t.boolean  "draft_created",        default: false
    t.string   "draft_url"
    t.text     "correction"
    t.integer  "coupon_id"
    t.string   "discount"
    t.text     "summary",              default: ""
    t.integer  "quantity",             default: 0
    t.boolean  "furlenco_offer",       default: true
    t.string   "gst"
    t.string   "gst_amount"
    t.string   "final_amount"
  end

  add_index "orders", ["address_id"], name: "index_orders_on_address_id", using: :btree
  add_index "orders", ["delivery_partner_id"], name: "index_orders_on_delivery_partner_id", using: :btree
  add_index "orders", ["delivery_status_id"], name: "index_orders_on_delivery_status_id", using: :btree
  add_index "orders", ["document_id"], name: "index_orders_on_document_id", using: :btree
  add_index "orders", ["platform_id"], name: "index_orders_on_platform_id", using: :btree
  add_index "orders", ["service_id"], name: "index_orders_on_service_id", using: :btree
  add_index "orders", ["status_id"], name: "index_orders_on_status_id", using: :btree
  add_index "orders", ["txnid"], name: "index_orders_on_txnid", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "orders_delivery_partners", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders_delivery_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders_details", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "mobile"
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders_feedbacks", force: :cascade do |t|
    t.integer  "order_id"
    t.text     "message"
    t.integer  "status_id"
    t.text     "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "orders_feedbacks", ["order_id"], name: "index_orders_feedbacks_on_order_id", using: :btree

  create_table "orders_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders_tracks", force: :cascade do |t|
    t.integer  "order_id",                     null: false
    t.boolean  "verified",     default: false
    t.boolean  "printed",      default: false
    t.boolean  "drafted",      default: false
    t.boolean  "delivered",    default: false
    t.boolean  "signed",       default: false
    t.boolean  "notary",       default: false
    t.datetime "verified_at"
    t.datetime "printed_at"
    t.datetime "drafted_at"
    t.datetime "delivered_at"
    t.datetime "signed_at"
    t.datetime "notary_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "user_status",  default: false
  end

  add_index "orders_tracks", ["order_id"], name: "index_orders_tracks_on_order_id", using: :btree

  create_table "partner_requests", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "profession"
    t.text     "address"
    t.text     "message"
    t.string   "status_id"
    t.text     "note"
    t.boolean  "callback_request"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "degree"
    t.string   "firm_name"
    t.string   "registration_no"
    t.string   "id_proof_type"
    t.string   "id_proof_number"
    t.string   "referral"
    t.text     "services"
    t.string   "city"
    t.string   "state"
    t.string   "pincode"
    t.string   "pan"
    t.string   "tan"
    t.string   "service_tax"
    t.string   "account_holder"
    t.string   "account_number"
    t.string   "ifsc_code"
    t.string   "bank_name"
    t.string   "branch_name"
    t.string   "business_category"
    t.string   "business_address"
    t.text     "bank_address"
  end

  add_index "partner_requests", ["status_id"], name: "index_partner_requests_on_status_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.string   "mihpayid"
    t.string   "mode"
    t.string   "amount"
    t.string   "secret_hash"
    t.string   "pg_type"
    t.string   "bank_ref_num"
    t.string   "bankcode"
    t.string   "name_on_card"
    t.string   "cardnum"
    t.string   "payuMoneyId"
    t.string   "discount"
    t.string   "txnid"
    t.string   "payu_status"
    t.integer  "status_id"
    t.integer  "order_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "email"
    t.string   "phone"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "type"
  end

  add_index "payments", ["order_id"], name: "index_payments_on_order_id", using: :btree
  add_index "payments", ["status_id"], name: "index_payments_on_status_id", using: :btree

  create_table "payments_icicis", force: :cascade do |t|
    t.string   "endpointTransactionId"
    t.string   "approval_code"
    t.string   "terminal_id"
    t.string   "ipgTransactionId"
    t.string   "currency"
    t.string   "chargetotal"
    t.string   "timezone"
    t.string   "cccountry"
    t.string   "oid"
    t.string   "txnid"
    t.string   "ccbin"
    t.string   "tdate"
    t.string   "response_hash"
    t.string   "transactionNotificationURL"
    t.string   "txndatetime"
    t.string   "status"
    t.string   "cardnumber"
    t.string   "processor_response_code"
    t.string   "expmonth"
    t.string   "expyear"
    t.string   "ccbrand"
    t.string   "txntype"
    t.string   "txndate_processed"
    t.string   "paymentMethod"
    t.string   "response_hash_validation"
    t.string   "responseHash_validationFrom"
    t.string   "response_code_3dsecure"
    t.string   "bname"
    t.text     "response_dump"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "payments_paytms", force: :cascade do |t|
    t.string   "MID"
    t.string   "ORDERID"
    t.string   "TXNAMOUNT"
    t.string   "CURRENCY"
    t.string   "TXNID"
    t.string   "BANKTXNID"
    t.string   "STATUS"
    t.string   "RESPCODE"
    t.string   "RESPMSG"
    t.string   "TXNDATE"
    t.string   "GATEWAYNAME"
    t.string   "BANKNAME"
    t.string   "PAYMENTMODE"
    t.string   "CHECKSUMHASH"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "payments_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "resource_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "permissions", ["role_id", "resource_id"], name: "index_permissions_on_role_id_and_resource_id", using: :btree

  create_table "platforms", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.string   "name"
    t.string   "category"
    t.date     "valid_by"
    t.text     "offer"
    t.string   "offer_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "resources", force: :cascade do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "resources", ["name", "parent_id"], name: "index_resources_on_name_and_parent_id", using: :btree

  create_table "role_column_accesses", force: :cascade do |t|
    t.integer  "role_id"
    t.string   "table_name"
    t.string   "column_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "role_column_accesses", ["role_id", "table_name"], name: "index_role_column_accesses_on_role_id_and_table_name", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "role_type"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "roles", ["parent_id"], name: "index_roles_on_parent_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "priority"
    t.text     "description"
    t.integer  "delivery_charges"
    t.integer  "min_stamp"
    t.integer  "print_price",        default: 0
    t.integer  "notary_price",       default: 0
    t.integer  "registration_price", default: 0
  end

  create_table "services_orders_answers", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "question_id"
    t.text     "body"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "services_orders_answers", ["order_id"], name: "index_services_orders_answers_on_order_id", using: :btree
  add_index "services_orders_answers", ["question_id"], name: "index_services_orders_answers_on_question_id", using: :btree

  create_table "services_questions", force: :cascade do |t|
    t.string   "name"
    t.integer  "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "services_questions", ["service_id"], name: "index_services_questions_on_service_id", using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "url_name"
    t.integer  "country_id"
    t.integer  "status_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree
  add_index "states", ["status_id"], name: "index_states_on_status_id", using: :btree

  create_table "statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.integer  "city_id"
    t.string   "profile_type"
    t.integer  "profile_id"
    t.integer  "status_id"
    t.integer  "parent_id"
    t.string   "phone"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "dvault_activated", default: false
  end

  add_index "users", ["city_id"], name: "index_users_on_city_id", using: :btree
  add_index "users", ["email", "phone"], name: "index_users_on_email_and_phone", unique: true, using: :btree
  add_index "users", ["parent_id"], name: "index_users_on_parent_id", using: :btree
  add_index "users", ["profile_id", "profile_type"], name: "index_users_on_profile_id_and_profile_type", using: :btree

  create_table "users_profiles", force: :cascade do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "gender"
    t.string   "profile_img"
    t.string   "cover_img"
    t.datetime "last_login"
    t.boolean  "mobile_verified",    default: false
    t.boolean  "email_verified",     default: false
    t.text     "about"
    t.integer  "sign_in_count",      default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.date     "birthday"
    t.boolean  "profile_verified",   default: false
    t.integer  "country_id"
    t.string   "address_proof_url"
    t.string   "id_proof_url"
  end

  add_index "users_profiles", ["country_id"], name: "index_users_profiles_on_country_id", using: :btree

  create_table "users_roles", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "users_roles", ["role_id", "user_id"], name: "index_users_roles_on_role_id_and_user_id", using: :btree

  create_table "verifications", force: :cascade do |t|
    t.string   "code"
    t.string   "verification_type"
    t.integer  "status_id"
    t.datetime "valid_till"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "verifications", ["user_id"], name: "index_verifications_on_user_id", using: :btree

  add_foreign_key "permissions", "resources", name: "fk_resources"
  add_foreign_key "permissions", "roles", name: "fk_roles"
  add_foreign_key "resources", "resources", column: "parent_id", name: "fk_resources_parent"
  add_foreign_key "role_column_accesses", "roles", name: "fk_roles"
  add_foreign_key "roles", "roles", column: "parent_id", name: "fk_roles_parent"
  add_foreign_key "users", "users", column: "parent_id", name: "fk_users_parent"
  add_foreign_key "users_roles", "roles", name: "fk_roles"
  add_foreign_key "users_roles", "users", name: "fk_users"
end
