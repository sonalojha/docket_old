class ChangeColumnTypeInContactRequest < ActiveRecord::Migration
  def change
  	rename_column :contact_requests, :type, :request_type
  end
end
