class AddProductToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :product, :string
    add_column :orders, :platform_id, :integer, default: 1
  end
end
