class CreateVerifications < ActiveRecord::Migration
  def change
    create_table :verifications do |t|
      t.string :code
      t.string :verification_type
      t.integer :status_id
      t.datetime :valid_till
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
