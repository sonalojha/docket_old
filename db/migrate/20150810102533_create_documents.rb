class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.integer :service_id
      t.boolean :stamp_required, default: false
      t.string :default_price
      
      t.timestamps null: false
    end
    add_index :documents, :service_id
  end
end
