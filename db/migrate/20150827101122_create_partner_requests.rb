class CreatePartnerRequests < ActiveRecord::Migration
  def change
    create_table :partner_requests do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :profession
      t.text   :address
      t.text   :message
      t.string :status_id
      t.text   :note
      t.boolean :callback_request

      t.timestamps null: false
    end
    add_index :partner_requests, :status_id
  end
end
