class CreateRoleColumnAccesses < ActiveRecord::Migration
  def up
    create_table :role_column_accesses do |t|
      t.integer :role_id
      t.string :table_name
      t.string :column_name

      t.timestamps null: false
    end
    change_table :role_column_accesses do |t|
      t.index [:role_id, :table_name]
    end
    
    execute <<-SQL
      ALTER TABLE role_column_accesses
        ADD CONSTRAINT fk_roles
        FOREIGN KEY (role_id)
        REFERENCES roles(id)
    SQL
  end
  
  def down
  	execute <<-SQL
      ALTER TABLE role_column_accesses
        DROP CONSTRAINT fk_roles
    SQL
    
    drop_table :role_column_accesses
  end
end
