class CreateContactRequests < ActiveRecord::Migration
  def change
    create_table :contact_requests do |t|
      t.string  :name
      t.string  :email
      t.string  :phone
      t.text    :message
      t.string  :type
      t.text    :note
      t.integer :status_id

      t.timestamps null: false
    end
    add_index :contact_requests, :status_id
  end
end
