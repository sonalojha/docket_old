class CreateUserRoles < ActiveRecord::Migration
  def up
    create_table :users_roles do |t|
      t.integer :role_id
      t.integer :user_id
      t.timestamps null: false
    end
    
    change_table :users_roles do |t|
      t.index [:role_id, :user_id], :unique =>false
    end

    execute <<-SQL
      ALTER TABLE users_roles
        ADD CONSTRAINT fk_roles
        FOREIGN KEY (role_id)
        REFERENCES roles(id)
    SQL
    
    execute <<-SQL
      ALTER TABLE users_roles
        ADD CONSTRAINT fk_users
        FOREIGN KEY (user_id)
        REFERENCES users(id)
    SQL
  end

  def down
  	execute <<-SQL
      ALTER TABLE users_roles
        DROP CONSTRAINT fk_roles
    SQL
    
    execute <<-SQL
      ALTER TABLE users_roles
        DROP CONSTRAINT fk_users
    SQL
    
    drop_table :users_roles
  end
end
