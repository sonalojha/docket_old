class CreateOrdersTracks < ActiveRecord::Migration
  def change
    create_table :orders_tracks do |t|
      t.integer :order_id, null: false

      t.boolean :verified, default: false
      t.boolean :printed, default: false
      t.boolean :drafted, default: false
      t.boolean :delivered, default: false
      t.boolean :signed, default: false
      t.boolean :notary, default: false
      t.datetime :verified_at
      t.datetime :printed_at
      t.datetime :drafted_at
      t.datetime :delivered_at
      t.datetime :signed_at
      t.datetime :notary_at

      t.timestamps null: false
    end
    add_index :orders_tracks, :order_id
  end
end
