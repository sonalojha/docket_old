class AddIndexes < ActiveRecord::Migration
  	def change
        add_index :documents_questions, :response_type_id
        add_index :documents_questions, :document_id
        add_index :orders, :platform_id
        add_index :notifications, :user_id
        add_index :users_profiles, :country_id
        add_index :verifications, :user_id
    end
end