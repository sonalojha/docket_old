class AddIndexesToTables < ActiveRecord::Migration
  def change
  	add_index :payments, :order_id
	add_index :dvaults_user_files, :dvault_id
	add_index :dvaults_user_files, :profile_id
	add_index :dvaults_user_files, :sub_category_id
	add_index :dvaults, :user_id
	add_index :orders, :delivery_partner_id
	add_index :dvaults_sub_categories, :category_id
	add_index :dvaults_profiles, :relationship_id
	add_index :users, [:profile_id, :profile_type]
	add_index :attachments, [:file_id, :file_type]
  end
end
