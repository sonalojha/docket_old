class ChangeUidToOrderIdInDocumentsAnswers < ActiveRecord::Migration
  def change
  	remove_column :documents_answers, :uuid
  	add_column :documents_answers, :order_id, :integer, null: false
  end
end
