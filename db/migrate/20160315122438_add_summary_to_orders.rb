class AddSummaryToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :summary, :text, default: ''
  end

end
