class ChangesInNotification < ActiveRecord::Migration
  def change
  	remove_column :notifications, :msg
  	add_column :notifications, :template_id, :integer
  	remove_column :notifications, :type
  	add_column :notifications, :notice_type, :string
  	add_column :notifications, :order_id, :integer
  	add_index :notifications, :order_id
  end
end
