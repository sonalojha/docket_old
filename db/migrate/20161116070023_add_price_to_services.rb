class AddPriceToServices < ActiveRecord::Migration
  def change
  	add_column :services, :print_price, :integer, default: 0
	add_column :services, :notary_price, :integer, default: 0
	add_column :services, :registration_price, :integer, default: 0
  end
end
