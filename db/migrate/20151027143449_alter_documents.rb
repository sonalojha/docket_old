class AlterDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :automated, :boolean, default: false
    add_column :documents, :template_url, :string      
    add_column :documents, :status_id, :integer      
    add_index :documents, :status_id
  end
end
