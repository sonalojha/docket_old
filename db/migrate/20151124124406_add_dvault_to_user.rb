class AddDvaultToUser < ActiveRecord::Migration
  def change
    add_column :users, :dvault_activated, :boolean, default: false
  end
end
