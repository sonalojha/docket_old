class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string  :code
      t.date    :start_date
      t.date    :end_date
      t.string  :coupon_type, default: 'fixed'
      t.string  :value
      t.integer :limit, default: -1
      t.integer :count, default: 0

      t.timestamps null: false
    end
  end
end
