class CreateServicesQuestions < ActiveRecord::Migration
  def change
    create_table :services_questions do |t|
      t.string  :name
      t.integer :service_id

      t.timestamps null: false
    end
  end
end

