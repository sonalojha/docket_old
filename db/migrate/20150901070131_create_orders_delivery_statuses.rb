class CreateOrdersDeliveryStatuses < ActiveRecord::Migration
  def change
    create_table :orders_delivery_statuses do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
