class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :locality_id
      t.integer :city_id
      t.string  :landmark
      t.string  :address1
      t.string  :address2
      t.string  :tag
      t.integer :status_id
      t.integer :user_id
      t.string  :pincode

      t.timestamps null: false
    end
    add_index :addresses, :user_id
    add_index :addresses, [:tag, :user_id], unique: true
  end
end
