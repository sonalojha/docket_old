class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string   :mihpayid
      t.string   :mode
      t.string   :amount
      t.string   :secret_hash
      t.string   :pg_type
      t.string   :bank_ref_num
      t.string   :bankcode
      t.string   :name_on_card
      t.string   :cardnum
      t.string   :payuMoneyId
      t.string   :discount
      t.string   :txnid
      t.string   :payu_status
      t.integer  :status_id
      t.integer  :order_id

      t.timestamps null: false
    end
    add_index :payments, :status_id
  end
end
