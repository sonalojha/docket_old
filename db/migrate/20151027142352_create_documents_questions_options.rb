class CreateDocumentsQuestionsOptions < ActiveRecord::Migration
  def change
    create_table :documents_questions_options do |t|
      t.string :name
      t.string :question_id
      t.integer :priority

      t.timestamps null: false
    end
    add_index :documents_questions_options, :question_id
  end
end
