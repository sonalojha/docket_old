class CreateDocumentsAnswers < ActiveRecord::Migration
  def change
    create_table :documents_answers do |t|
      t.integer :document_id
      t.integer :section_id
      t.integer :question_id
      t.string  :uuid
      t.text :body

      t.timestamps null: false
    end
    add_index :documents_answers, :document_id
    add_index :documents_answers, :question_id
  end
end
