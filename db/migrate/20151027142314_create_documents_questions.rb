class CreateDocumentsQuestions < ActiveRecord::Migration
  def change
    create_table :documents_questions do |t|
      t.integer :response_type_id
      t.boolean :is_mandatory
      t.text :body
      t.text :associated_text
      t.string :placeholder
      
      t.timestamps null: false
    end
  end
end
