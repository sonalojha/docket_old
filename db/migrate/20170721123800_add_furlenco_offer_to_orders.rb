class AddFurlencoOfferToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :furlenco_offer, :boolean, default: false
  end
end
