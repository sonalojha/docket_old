class AddIndexToDeviceRegistrations < ActiveRecord::Migration
  def change
  	add_index :device_registrations, :user_id
  	add_index :device_registrations, :platform_id
  end
end
