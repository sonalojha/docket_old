class AddToPartnerRegistration < ActiveRecord::Migration
  def change
  	add_column :partner_requests, :degree, :string
  	add_column :partner_requests, :firm_name, :string
  	add_column :partner_requests, :registration_no, :string
  	add_column :partner_requests, :id_proof_type, :string
  	add_column :partner_requests, :id_proof_number, :string
  	add_column :partner_requests, :referral, :string
  	add_column :partner_requests, :services, :text

  end
end
