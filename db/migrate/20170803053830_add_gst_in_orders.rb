class AddGstInOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :gst, :string
  	add_column :orders, :gst_amount, :string
  	add_column :orders, :final_amount, :string
  end
end