class CreateDocumentsSections < ActiveRecord::Migration
  def change
    create_table :documents_sections do |t|
      t.string 	:name
      t.integer :section_no
      t.text 	:description
      t.integer :document_id

      t.timestamps null: false
    end
    add_index :documents_sections, :document_id
  end
end
