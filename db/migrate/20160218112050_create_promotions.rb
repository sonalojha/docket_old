class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :name
      t.string :category
      t.date :valid_by
      t.text :offer
      t.string :offer_type

      t.timestamps null: false
    end
  end
end
