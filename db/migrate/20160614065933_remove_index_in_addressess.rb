class RemoveIndexInAddressess < ActiveRecord::Migration
  def change
  	remove_index :addresses, [:tag, :user_id]
  	add_index :addresses, [:tag, :user_id]
  end
end
