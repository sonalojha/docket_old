class AddDocumentIdToDocumentsQuestions < ActiveRecord::Migration
  def change
  	add_column :documents_questions, :document_id, :integer
  end
end
