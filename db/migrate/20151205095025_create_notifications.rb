class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.text :msg
      t.boolean :read
      t.string :type
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
