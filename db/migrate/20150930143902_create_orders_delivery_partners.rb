class CreateOrdersDeliveryPartners < ActiveRecord::Migration
  def change
    create_table :orders_delivery_partners do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
