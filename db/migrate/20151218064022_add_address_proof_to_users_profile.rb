class AddAddressProofToUsersProfile < ActiveRecord::Migration
  def change
  	add_column :users_profiles, :address_proof_url, :string
  	add_column :users_profiles, :id_proof_url, :string
  end
end
