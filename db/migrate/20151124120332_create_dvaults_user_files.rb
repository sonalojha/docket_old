class CreateDvaultsUserFiles < ActiveRecord::Migration
  def change
    create_table :dvaults_user_files do |t|
      t.string :filename
      t.string :tag
      t.string :mime_type
      t.string :vault_uid
      t.string :size
      t.integer :profile_id
      t.integer :dvault_id
      t.string :thumbnail_image
      t.integer :sub_category_id

      t.timestamps null: false
    end
  end
end
