class CreateDocumentsQuestionsResponseTypes < ActiveRecord::Migration
  def change
    create_table :documents_questions_response_types do |t|
      t.string :name
      t.string :regex

      t.timestamps null: false
    end
  end
end
