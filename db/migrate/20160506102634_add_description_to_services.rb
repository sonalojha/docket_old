class AddDescriptionToServices < ActiveRecord::Migration
  def change
  	add_column :services, :description, :text
  	add_column :services, :delivery_charges, :integer
  end
end
