class AddUuidToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :delivery_status_id, :integer
    add_column :orders, :delivery_partner_id, :integer
    add_column :orders, :delivery_tracking_no, :string
    add_index :orders, :delivery_status_id
  end
end