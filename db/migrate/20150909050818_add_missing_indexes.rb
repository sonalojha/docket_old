class AddMissingIndexes < ActiveRecord::Migration
	def change
	    add_index :orders, :service_id
	    add_index :addresses, :city_id
	    add_index :addresses, :locality_id
	    add_index :services_orders_answers, :order_id
	    add_index :services_orders_answers, :question_id
	    add_index :users, :parent_id
	    add_index :users, :city_id
	    add_index :roles, :parent_id
	    add_index :services_questions, :service_id
	end
end
