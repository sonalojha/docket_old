class CreateResources < ActiveRecord::Migration
  def up
    create_table :resources do |t|
      t.string :name
      t.integer :parent_id

      t.timestamps null: false
    end
    change_table :resources do|t|
      t.index [:name, :parent_id]
    end
    
    execute <<-SQL
      ALTER TABLE resources
        ADD CONSTRAINT fk_resources_parent
        FOREIGN KEY (parent_id)
        REFERENCES resources(id)
    SQL
  end
  
  def down
  	execute <<-SQL
      ALTER TABLE resources
        DROP CONSTRAINT fk_resources_parent
    SQL
    
    drop_table :resources
  end
end
