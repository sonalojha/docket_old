class CreateRoles < ActiveRecord::Migration
  def up
    create_table :roles do |t|
      t.string :name, null: false
      t.string :role_type
      t.integer :parent_id
      t.timestamps null: false

    end
    execute <<-SQL
      ALTER TABLE roles
        ADD CONSTRAINT fk_roles_parent
        FOREIGN KEY (parent_id)
        REFERENCES roles(id)
    SQL
  end

  def down
  	execute <<-SQL
      ALTER TABLE roles
        DROP CONSTRAINT fk_roles_parent
    SQL
    
    drop_table :roles
  end
end
