class AddPriorityToDocuments < ActiveRecord::Migration
  def change
  	add_column :documents, :priority, :integer
  end
end
