class AddUserStatusToOrderTrack < ActiveRecord::Migration
  def change
  	add_column :orders_tracks, :user_status, :boolean, default: false
  end
end
