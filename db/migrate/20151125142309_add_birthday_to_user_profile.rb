class AddBirthdayToUserProfile < ActiveRecord::Migration
  def change
  	add_column :users_profiles, :birthday, :date
  	add_column :users_profiles, :profile_verified, :boolean, default: false
  	
  end
end
