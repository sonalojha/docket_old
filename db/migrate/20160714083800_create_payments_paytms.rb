class CreatePaymentsPaytms < ActiveRecord::Migration
  def change
    create_table :payments_paytms do |t|
      t.string :MID
      t.string :ORDERID
      t.string :TXNAMOUNT
      t.string :CURRENCY
      t.string :TXNID
      t.string :BANKTXNID
      t.string :STATUS
      t.string :RESPCODE
      t.string :RESPMSG
      t.string :TXNDATE
      t.string :GATEWAYNAME
      t.string :BANKNAME
      t.string :PAYMENTMODE
      t.string :CHECKSUMHASH

      t.timestamps null: false
    end
  end
end
