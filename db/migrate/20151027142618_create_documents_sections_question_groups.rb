class CreateDocumentsSectionsQuestionGroups < ActiveRecord::Migration
  def change
    create_table :documents_sections_question_groups do |t|
      t.integer :section_id
      t.integer :question_id
      t.integer :priority

      t.timestamps null: false
    end
    add_index :documents_sections_question_groups, :section_id
    add_index :documents_sections_question_groups, :question_id

  end
end
