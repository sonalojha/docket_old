class CreatePaymentsIcicis < ActiveRecord::Migration
  def change
    create_table :payments_icicis do |t|
      t.string :endpointTransactionId
      t.string :approval_code
      t.string :terminal_id
      t.string :ipgTransactionId
      t.string :currency
      t.string :chargetotal
      t.string :timezone
      t.string :cccountry
      t.string :oid
      t.string :txnid
      t.string :ccbin
      t.string :tdate
      t.string :response_hash
      t.string :transactionNotificationURL
      t.string :txndatetime
      t.string :status
      t.string :cardnumber
      t.string :processor_response_code
      t.string :expmonth
      t.string :expyear
      t.string :ccbrand
      t.string :txntype
      t.string :txndate_processed
      t.string :paymentMethod
      t.string :response_hash_validation
      t.string :responseHash_validationFrom
      t.string :response_code_3dsecure
      t.string :bname
      t.text   :response_dump

      t.timestamps null: false
    end
  end
end
