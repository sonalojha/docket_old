class CreateDvaults < ActiveRecord::Migration
  def change
    create_table :dvaults do |t|
      t.integer :user_id
      t.string :pin

      t.timestamps null: false
    end
  end
end
