class AddDraftCreatedOnOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :draft_created, :boolean, default: false
  	add_column :orders, :draft_url, :string
  end
end
