class AddCountryIdToUserProfiles < ActiveRecord::Migration
  def change
  	add_column :users_profiles, :country_id, :integer
  end
end
