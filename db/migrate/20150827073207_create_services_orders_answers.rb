class CreateServicesOrdersAnswers < ActiveRecord::Migration
  def change
    create_table :services_orders_answers do |t|
      t.integer :order_id
      t.integer :question_id
      t.text    :body

      t.timestamps null: false
    end
  end
end
