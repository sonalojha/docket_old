class CreateOrdersDetails < ActiveRecord::Migration
  def change
    create_table :orders_details do |t|
      t.string :name
      t.string :email
      t.string :mobile
      t.integer :order_id

      t.timestamps null: false
    end
  end
end
