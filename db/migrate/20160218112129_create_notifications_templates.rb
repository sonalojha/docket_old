class CreateNotificationsTemplates < ActiveRecord::Migration
  def change
    create_table :notifications_templates do |t|
      t.string :name
      t.text :message

      t.timestamps null: false
    end
  end
end
