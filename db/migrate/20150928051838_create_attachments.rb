class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string  :s3_url
      t.integer :file_id
      t.string  :file_type
      t.string  :tag
      t.string  :name
      t.boolean :is_processed, default: false

      t.timestamps null: false
    end
  end
end
