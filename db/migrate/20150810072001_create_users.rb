class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password
      t.integer :city_id
      t.string :profile_type
      t.integer :profile_id
      t.integer :status_id
      t.integer :parent_id
      t.string :phone

      t.timestamps null: false
    end
    change_table :users do |t|
       t.index [:email, :phone], :unique => true
    end

    execute <<-SQL
      ALTER TABLE users
        ADD CONSTRAINT fk_users_parent
        FOREIGN KEY (parent_id)
        REFERENCES users(id)
    SQL
  end

  def down
    execute <<-SQL
      ALTER TABLE users
        DROP CONSTRAINT fk_users_parent
    SQL
    drop_table :users
  end
end
