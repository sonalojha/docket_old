class CreateUsersProfiles < ActiveRecord::Migration
  def change
    create_table :users_profiles do |t|
      t.string   :first_name
      t.string   :middle_name
      t.string   :last_name
      t.string   :gender
      t.string   :profile_img
      t.string   :cover_img
      t.datetime :last_login
      t.boolean  :mobile_verified, default: false
      t.boolean  :email_verified, default: false
      t.text     :about

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
      
      t.timestamps null: false
    end
  end
end