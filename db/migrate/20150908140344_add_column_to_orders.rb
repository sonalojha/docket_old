class AddColumnToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :total_amount, :string
  	add_column :orders, :delivery_amount, :string
  	add_column :orders, :txnid, :string
  	add_column :orders, :invoice_url, :string
  	add_column :orders, :draft_confirmed, :boolean, default: false
  	add_column :orders, :soft_copy_url, :string
  	add_column :orders, :draft_required, :boolean, default: true

  	add_index  :orders, :txnid
  end
end
