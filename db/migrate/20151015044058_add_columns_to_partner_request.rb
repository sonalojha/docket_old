class AddColumnsToPartnerRequest < ActiveRecord::Migration
  def change
  	add_column :partner_requests, :city, :string
  	add_column :partner_requests, :state, :string
  	add_column :partner_requests, :pincode, :string
  	add_column :partner_requests, :pan, :string
  	add_column :partner_requests, :tan, :string
  	add_column :partner_requests, :service_tax, :string
    add_column :partner_requests, :account_holder, :string
    add_column :partner_requests, :account_number, :string
    add_column :partner_requests, :ifsc_code, :string
    add_column :partner_requests, :bank_name, :string
    add_column :partner_requests, :branch_name,:string
    add_column :partner_requests, :business_category, :string
	add_column :partner_requests, :business_address, :string
	add_column :partner_requests, :bank_address, :text
  end
end
