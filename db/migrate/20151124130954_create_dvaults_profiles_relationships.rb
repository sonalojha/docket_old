class CreateDvaultsProfilesRelationships < ActiveRecord::Migration
  def change
    create_table :dvaults_profiles_relationships do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
