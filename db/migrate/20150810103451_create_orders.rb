class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :document_id
      t.integer :user_id
      t.integer :status_id
      t.string  :stamp_amount

      t.timestamps null: false
    end
    add_index :orders, :user_id
    add_index :orders, :document_id
    add_index :orders, :status_id
  end
end
