class CreateDvaultsProfiles < ActiveRecord::Migration
  def change
    create_table :dvaults_profiles do |t|
      t.string :name
      t.integer :relationship_id
      t.integer :dvault_id

      t.timestamps null: false
    end
  end
end
