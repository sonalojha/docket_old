class CreateDvaultsCategories < ActiveRecord::Migration
  def change
    create_table :dvaults_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
