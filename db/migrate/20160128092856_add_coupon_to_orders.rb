class AddCouponToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :coupon_id, :integer
  	add_column :orders, :discount, :string
  end
end
