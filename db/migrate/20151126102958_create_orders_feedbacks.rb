class CreateOrdersFeedbacks < ActiveRecord::Migration
  def change
    create_table :orders_feedbacks do |t|
      t.integer :order_id
      t.text 	:message
      t.integer :status_id
      t.text 	:note

      t.timestamps null: false
    end
    add_index :orders_feedbacks, :order_id
  end
end