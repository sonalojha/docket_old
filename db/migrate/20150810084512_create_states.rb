class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name
      t.string :url_name
      t.integer :country_id
      t.integer :status_id

      t.timestamps null: false
    end
    add_index :states, :status_id
    add_index :states, :country_id
  end
end
