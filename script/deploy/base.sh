export RAILS_DIR=${RAILS_DIR:-/home/ubuntu/docket}
export RAILS_ENV=${RAILS_ENV:-production}
export GIT_BRANCH=${GIT_BRANCH:-master}
set -e
cd $RAILS_DIR
mkdir -p tmp/{pids,sockets}

rake resque:work QUEUE=* BACKGROUND=yes

echo "== git pull"
git checkout "$GIT_BRANCH"
git pull origin "$GIT_BRANCH"
echo "== bundle install"
bundle install -j6 --without development test
echo "== rake assets:precompile use (Rbenv for ubuntu server)"
bundle exec rake assets:precompile
echo "== rake db:migrate"
bundle exec rake db:migrate
echo "== rake db:seed"
bundle exec rake db:seed
echo "== rake admin:populate_resources"
bundle exec rake admin:populate_resources > log/deployment.log 2>&1
echo "== Restarting nginx server"
service nginx restart
echo "== rails cache clear"
bundle exec rails runner "Rails.cache.clear"
echo "== for restarting the background jobs"
bundle exec rake resque:restart_workers