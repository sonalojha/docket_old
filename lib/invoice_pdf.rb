require "render_anywhere"
 
class InvoicePdf
  include RenderAnywhere
 
  def initialize(order_id)
    @order = Order.find(order_id)
    @base_url = ENV['email_base_url']
    if (['Notary', 'Attestation','Franking'].include? @order.service.name)
      @no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i 
    else
      @no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i + 1
    end
  end
 
  def to_pdf
    invoice_path = "invoice_#{@order.txnid}.pdf"
    new_invoice = PDFKit.new(invoice, page_size: 'A4')
    new_invoice.to_file("#{Rails.root}/tmp/#{invoice_path}")
    return paths = {invoice: invoice_path}
  end
 
  def filename
    "Invoice #{invoice.id}.pdf"
  end
 
  private
    attr_reader :order, :base_url, :no_of_copies
    
    def as_html
      render template: "order_mailer/order_invoice", layout: false, locals: { order: order, base_url: base_url, no_of_copies: no_of_copies }
    end

    def invoice
      render template: "order_mailer/order_invoice", layout: false, locals: { order: order, base_url: base_url, no_of_copies: no_of_copies }
    end
end