module Exceptions
  class RowAccessControlViolation < StandardError
    def message
      "You don't have the permission to access this record"
    end
  end
end
   