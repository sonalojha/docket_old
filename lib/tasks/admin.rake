namespace :admin do
  def ask(message)
    print message
    STDIN.gets.chomp
  end

  desc "Populating the resource table"
  task :populate_resources => :environment do
    Resource.where(:name => "root").first_or_create()
    Resource.populate_resources
  end
  
  desc "Creating Roles (Admin and open_user) and assigning default permissions"
  task :init_roles => :environment do
    admin = Role.where(:name => "admin", :role_type => "A").first_or_create()
    open_user = Role.where(:name => "open_user", :role_type => "U", :parent_id => admin.id).first_or_create()
    
    if !admin.resources.include?(Resource.where(:name => "root").first)
      admin.resources.push(Resource.where(:name => "root").first)
    end
    # if !open_user.resources.include?(Resource.where(:name => "user_sign_in").first)
    #   open_user.resources.push(Resource.where(:name => "user_sign_in").first)
    #   open_user.resources.push(Resource.where(:name => "user_login").first)
      open_user.resources.push(Resource.where(:name => "employee_login").first)
      open_user.resources.push(Resource.where(:name => "employee_sign_in").first)
      open_user.resources.push(Resource.where(:name => "employee_logout").first)
      open_user.resources.push(Resource.where(:name => "payu_response").first)
      open_user.resources.push(Resource.where(:name => "payu_mobile").first)
      open_user.resources.push(Resource.where(:name => "paytm_response").first)
    #   # open_user.resources.push(Resource.where(:name => "logout").first)
    # end
  end
  
  
  desc "Creating the admin user"
  task :create_admin => :environment do
    create_admin = ask("Do you want to create an admin user (You can create it later by runnin rake admin:create_admin) \n(y/n)\n")
    if create_admin == "y"
      name = ask("Please Enter the name for admin\n")
      email = ask("Please Enter the email address for admin (must be of the form abc@z.x)\n")
      phone_num = ask("Please Enter the phone number for admin (only numbers)\n")
      while true
        password = ask("Enter the password\n")
        cpassword = ask("Enter the password again\n")
        if password == cpassword
          break
        else
          print "Passwords don't match\n"
        end
      end

      admin = User.create(name: name, :email => email, :phone => phone_num, :password => password, :profile_type => 'Employee' )
      admin.roles.push(Role.where(:name => "admin", :role_type => "A").first_or_create)
    end 
  end

  desc "set_admin_password"
  task :set_admin_password => :environment do
    user = User.where(:email => "goldie@notarymama.com").first_or_create
    user.name = "Admin"
    user.password = "businessmanIII"
    user.phone = "8088204614"
    user.profile_type = "Employee"
    user.save!
  end
  
  desc "Run all admin tasks"
  task :all => [:populate_resources, :init_roles, :create_admin]#, :add_bounds_to_mumbai]
end