
namespace :template_test do  
  desc "Testing the document format and verifyiing the content"
  task :check_templates => :environment do
  	verify_format
  end

  def verify_format
  	i = 0
  	Document.includes(:questions).where(service_id: [1,2,3,4], automated: true).each do |document|
  		template_url = document.template_url
  		doc = ::DocxReplace::Doc.new(template_url, "#{Rails.root}/tmp/orders/template/")
  		actual_questions = document.questions.pluck(:placeholder).uniq.count
  		question_in_format = doc.document_content.scan(/(\$[\S]*\$\$)/).flatten.uniq.count
  		if actual_questions > question_in_format
  			i +=1
        # puts "\n"
        xls = []
        document.questions.pluck(:placeholder).map{|place| xls << "#{'$$'+place+'$$'}"}
        docs = doc.document_content.scan(/(\$[\S]*\$\$)/).flatten.uniq
        puts "#{document.name}, #{document.questions.count}"
        puts "#{xls - docs}, #{doc.document_content.scan(/(\$[\S]*\$)/).flatten.uniq.count} "
      end 
  		puts doc.document_content.scan(/(\$[\S]*\$\$)/).flatten.uniq
		doc = nil
		template_url = nil
  	end
    puts "#{i}"
  end


  desc "Testing the document format For filling details"
  task :check_templates_placeholder => :environment do
    verify_format_template
  end

  def verify_format_template
    Document.all.where(service_id: [1,2,3,4], automated: true).each do |document|
      # puts "#{document.name}"
      template_url = document.template_url
      doc = ::DocxReplace::Doc.new(template_url, "#{Rails.root}/tmp/orders/template/")
      document.questions.each do |question|
        count = doc.document_content.scan("$$#{question.placeholder.upcase}$$").count
        # puts "#{question.id}" if count == 0
        value = SecureRandom.urlsafe_base64
        count.times do
          doc.document_content.force_encoding("UTF-8").sub!("$$#{question.placeholder.upcase}$$", value.upcase)
        end
        #### doc.replace("$$#{key.upcase}$$", value.upcase, true)
      end
      # (doc.document_content.scan(/(\$[\S]*\$\$)/)).flatten.each do |key|
      #   count = doc.document_content.scan("$$#{key.upcase}$$").count
      #   value = SecureRandom.urlsafe_base64
      #   count.times do
      #     doc.document_content.force_encoding("UTF-8").sub!("$$#{key.upcase}$$", value.upcase)
      #   end
      # end
      tmp_file = Tempfile.new('word_tempate', "#{Rails.root}/tmp/orders/template/")
      name = document.name.split(' ').join('_')+ '_' + Random.rand(100000).to_s + '.docx'
      new_file_path = "#{Rails.root}/tmp/orders/template/" + name
      # doc.commit(new_file_path) if (doc.document_content.scan(/(\$[\S]*\$)/).flatten.count > 0)
      # puts "#{document.name}" if (doc.document_content.scan(/(\$[\S]*\$)/).flatten.count <= 0)
      doc.commit(new_file_path) if (doc.document_content.scan('$').count > 0)
    end
  end
end
 