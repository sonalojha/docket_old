// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap-sprockets
//= require bootstrap-datetimepicker
//= require_tree ./external
//= require ./material/material.min
//= require ./extras/smoothscroll

$(document).ready(function() {
	$.material.init();

	var $icon = $(".ion");
	var $down = $("#go-down");
	var $up = $("#go-up");

	var str = 'Docket understands any document request and process through a robust platform which communicates with an expert in the backend and delivers instantly to you. To assist you get the hang of our exceptional services better, we brought together all the procedures and concepts on our platform to guide and help you on choosing the best for a solution. We have passionately designed our services and have categorized them in a creative way to provide transparancy on our subject matter. Worry no further ! In the impending pages you have fine points to know us and our products well. We hope you enjoy and appreciate our services under this exquisite cutomer program.';

	var spans = '<span>' + str.split('').join('</span><span>') + '</span>';
	$(spans).appendTo('.css-typing').each(function (i) {
	    $(this).delay(8 * i).css({
	        display: 'inline',
	        opacity: 0
	    }).animate({
	        opacity: 1
	    }, 1);
	});

	var scroll_start = 0;
	var startchange = $('#startchange');
	var offset = startchange.offset();
	if (startchange.length){
	   $(document).scroll(function() { 
	      scroll_start = $(this).scrollTop();
	      if(scroll_start > (offset.top - 50)) {
	          $(".nav-home").hide();
	          $(".nav-home2").show();

	       } else {
	          $(".nav-home").show();
	          $(".nav-home2").hide();
	       }
	   });
	}
	$(".nav-home2").hide();
	$window = $(window);
	$('section[data-type="background"]').each(function(){
		var $bgobj = $(this); // assigning the object
    	$(window).scroll(function() {
			var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
			var coords = '50% '+ yPos + 'px';
			$bgobj.css({ backgroundPosition: coords });
		});
 	});	

	$("#myModal").modal('hide');
	$('.login-pop #loginModal').on('click', function(){
		$("#myModal").modal('show');
	});

	function msieversion() {
	    var ua = window.navigator.userAgent;
	    var msie = ua.indexOf("MSIE ");
	    if (msie > 0) // If Internet Explorer, return version number
	        {$('body').html("<div class='non-supported-browser'>This Browser Is Not Supported. The browser supported are IE 10 and above, Safari, Chrome and Firefox</div>");
	        	    	$('.non-supported-browser').css({"background": '#966262',"height": '100vh',"text-align": 'center',"padding": '25%',
	            			"position": 'absolute',"width": '100%',"color": 'white',"font-size": '32px'});}
	}
	msieversion()
	// for screen size smaller than 564 px 
	// function appversion() {
	//     var windowsize = $(window).width();
	//     if (windowsize < 564)
	//         {$('body').html("<div class='app_link'><div class='download_app'><a href='/' target='_blank'><img src='/assets/new_images/logo.png' class='googleplay'></a><h4>Download our app</h4><span class='icon_wrap'><a href='https://play.google.com/store/apps/details?id=com.docket' target='_blank'><img src='/assets/new_images/googleplay.png' class='googleplay'></a><a href='https://appsto.re/in/sSvqab.i' title='Docket iOS app' target='_blank'><img src='/assets/new_images/appstore.png' class='appstore'></a></span></div></div>");
	//        	$('.app_link').css({"background": 'rgba(242, 242, 242, 0.35)',"height": '100vh',"text-align": 'center',"padding": '20%',
	//             "position": 'absolute',"width": '100%',"color": 'black',"font-size": '32px'});}
	// }
	// appversion()
});
function setNotification(type, notif_txt){
    switch(type){
        case 'success':
            $('#notification').removeClass('red').addClass('green');
            break;
        case 'error':
            $('#notification').addClass('red').removeClass('green');
            break;
    }
    $('#notification').html(notif_txt);
    $('#notification').slideDown('slow').delay(7000).slideUp('slow');
}

function openCity(evt, cityName) {

	var selectValues = {
		"ANNUAL COMPLIANCES": {
			"1":"Annual Compliances for Private Limited Companies",
			"2":"Annual Filings for LLPs"
		},
		"EVENT BASED COMPLIANCES": {
			"1": "Increase in Authorized Capital",
			"2": "Adding a Director OR Remove a Director OR Add a Designated Partner",
			"3": "Change Objectives of Your Business",
			"4": "Change Official Address",
			"5": "Change Company Name",
			"6": "Change LLP Agreement"
		},
		"CLOSURE" : {
			"1": "Private Limited Company",
			"2": "Limited Liability Partnership"
		}
	};

	var $vendor = $('#sel1');
	var $model = $('#sel2');

    //
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";

    if(cityName == "London"){
        $('#sel1 option:contains("ANNUAL")').prop('selected',true);
        $model.empty().append(function() {
			var output = '';
			$.each(selectValues[$vendor.val()], function(key, value) {
				output += '<option>' + value + '</option>';
			});
			return output;
		});
    }else if(cityName == "Paris"){
        $('#sel1 option:contains("EVENT")').prop('selected',true);
        $model.empty().append(function() {
			var output = '';
			$.each(selectValues[$vendor.val()], function(key, value) {
				output += '<option>' + value + '</option>';
			});
			return output;
		});
    }else{
        $('#sel1 option:contains("CLOSURE")').prop('selected',true);
        $model.empty().append(function() {
			var output = '';
			$.each(selectValues[$vendor.val()], function(key, value) {
				output += '<option>' + value + '</option>';
			});
			return output;
		});
    }
}

