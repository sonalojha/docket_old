// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap-sprockets
//= require bootstrap-datetimepicker
//= require_tree ./external 
//= require_tree ./js
//= require ./new_js/dragdealer.min
function setNotification(type, notif_txt){
	switch(type){
		case 'success':
			$('#notification').removeClass('red').addClass('green');
			break;
		case 'error':
			$('#notification').addClass('red').removeClass('green');
			break;
	}
	$('#notification').html(notif_txt);
	$('#notification').slideDown('slow').delay(4000).slideUp('slow');
}
function formHide(e){
	$('.contact_form').hide('fast');
	$('.thanks').show('slow');
	setNotification('success', "We will get back to you shortly!")
};
