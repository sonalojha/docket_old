// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jsapi
//= require chartkick
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require jquery-fileupload
//= require bootstrap-sprockets
//= require turbolinks
//= require jquery.tokeninput
//= require cocoon
//= require_tree ./employee
//= require_tree ./external

function setNotification(type, notif_txt){
    switch(type){
        case 'success':
            $('#notification').removeClass('error_notif');
            break;
        case 'error':
            $('#notification').addClass('error_notif');
            break;
    }
    $('#notification').html(notif_txt);
    $('#notification').slideDown('slow').delay(3000).slideUp('slow');
}