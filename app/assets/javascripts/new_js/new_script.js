$(document).ready(function() {
    /****************** Header and Footer JS Starts ************************/
        // $("#header").load("header.html");
        // $("#footer").load("footer.html");
    /**************** Header and Footer JS Ends ************************/
    // $("#stamp_amount").keydown(function (e) {
    //     // Allow: backspace, delete, tab, escape, enter and .
    //     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    //          // Allow: Ctrl+A, Command+A
    //         (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
    //          // Allow: home, end, left, right, down, up
    //         (e.keyCode >= 35 && e.keyCode <= 40)) {
    //              // let it happen, don't do anything
    //              return;
    //     }

    //     // if ( event.keyCode == 46 || event.keyCode == 8) {
    //     //     // let it happen, don't do anything
           
    //     // }
    //     // else {
    //     //     // Ensure that it is a number and stop the keypress
    //     //     if ((event.keyCode !==9) && (event.keyCode < 48 || event.keyCode > 57 )) {
    //     //         event.preventDefault(); 
    //     //     }   
    //     //         else{
                 
    //     //       if($.trim($(this).val()) =='')
    //     //     {
    //     //         if(event.keyCode == 48 || event.keyCode == 57 || event.keyCode == 96 || event.keyCode == 105){
    //     //             event.preventDefault(); 
    //     //         }
    //     //     }
                    
    //     //     }
    //     // }

    //     $(this).val($(this).val().replace(/^0+|[^0-9.]+/, ''));
    // });

    $("#stamp_amount").keypress(function (e) {
        $(this).val($(this).val().replace(/^0+|[^0-9.]+/, ''));
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $(e.target.parentElement).find(".errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    /******************** Login Popup JS Starts ***************************/
    $(".login").on("click",function(){
        $(".login_wrap").show( "slide", { direction: "right"  }, 200);
        $(".signup_wrap").hide();
        $(".signup").show( "slide", { direction: "right"  }, 200);
    })

    $(".arrow_img").on("click",function(){
        $(".login_wrap").hide();
        $(".signup_wrap").show( "slide", { direction: "left"  }, 200);
        $(".signup").show( "slide", { direction: "left"  }, 200);
    })

    $(".arrow_signup").on("click",function(){
        $(".login_wrap").show( "slide", { direction: "left"  }, 200);
        $(".signup_wrap").hide();
        $(".login").show( "slide", { direction: "left"  }, 200);
    })

    $(".signup").on("click",function(){
        $(".signup_wrap").show( "slide", { direction: "right"  }, 200);
        $(".login_wrap").hide();
        
    })


    $(".otp_btn").on("click",function(){
        $(".otp_wrap").show();
        $(".otp_btn").hide();
    })

    /******************** Login Popup JS Ends ***************************/


        /************ Stamp Duty Amount JS Starts *****************/

        // $(".stamp_study").keypress(function (e) {
        //     if (e.keyCode == 13 && (parseInt($("#amount_validation").val()) >= 20)) {
        //         $('.stamp_study').hide();
        //         $('.stamp_details').show();
        //         $(".stamp-value").html(e.target.value);
                // $('#dis').slideDown().html('<span id="error">Please enter minimum Rs. 20</span>');
        //     }
        // });

        // $(".edit_option").click(function() {
        //     $('.stamp_study').show();
        //     $('.stamp_details').hide();
        // });


        /************ Stamp Duty Amount JS Endss *****************/

        /************ Date Selection JS Starts *****************/

        $('.left_arrow').on('click', function(e){
           var value = $('.time-slot input[type=radio]:checked').attr('data-attr');
            // console.log(e.target, value);
           if (parseInt(value) > 0 ){
                $($('.time-slot input[type=radio]')[(parseInt(value) - 1)]).prop("checked", true)
            
           } 
        });
        $('.right_arrow').on('click', function(e){
           var value = $('.time-slot input[type=radio]:checked').attr('data-attr');
           if (parseInt(value) < 6){
               $($('.time-slot input[type=radio]')[(parseInt(value) + 1)]).prop("checked", true)
           }
        });

        $("input[type=radio]").on("swiperight",function(){
          var value = $('.time-slot input[type=radio]:checked').attr('data-attr');
            // console.log(e.target, value);
           if (parseInt(value) > 0 ){
                $($('.time-slot input[type=radio]')[(parseInt(value) - 1)]).prop("checked", true)
            
           } 
        });
        /************ Date Selection JS Ends *****************/


        /************ Checkbox Validation JS Starts ***************/

        $("input[name='terms_condition']").on("change", function(){
            var isChecked = $(this).prop("checked"); 
            if(isChecked){
                $(".pay_btn").show(); 
            } else {
                $(".pay_btn").hide(); 
            }
        }); 


        // $(function () {
        //     var button = $('.terms_condition').prop('disabled', true);
        //     var radios = $('input[type="radio"]');
        //     var arr    = $.map(radios, function(el) { 
        //                 return el.name; 
        //               });

        //     var groups = $.grep(arr, function(v, k){
        //             return $.inArray(v ,arr) === k;
        //     }).length;

        //     radios.on('change', function () {
        //         button.prop('disabled', radios.filter(':checked').length < groups);
        //     });
        // });

        // function buttonState(){
        //     $("input").each(function(){
        //         $('.terms_condition').attr('disabled', 'disabled');
        //         if($(this).val() == "" ) return false;
        //         $('.terms_condition').attr('disabled', '');
        //     })
        // }

        // $(function(){
        //     $('.terms_condition').attr('disabled', 'disabled');
        //     $('input').change(buttonState);
        // })
        /************ Checkbox Validation JS Ends ***************/



        /************ Contact Details JS Starts ***************/
        
        // $('.payment_form').submit(function(e){
        //          e.preventDefault();
        //     $('.contact_summary').show();
        //     $('.payment_details').hide();
            
        //     var fullName = $("#fullName").val();
        //     var email = $("#email").val();
        //     var mobileNumber = $("#mobileNumber").val();
            
        //     $("#validatefullName").html(fullName);
        //     $("#validateemail").html(email);
        //     $("#validatemobileNumber").html(mobileNumber);
        // });

        $('.edit_option').on('click', function(){
            $('.contact_summary').hide();
            $('.payment_details').show();
        });
        
        $('#changeType').click(function(e){
            if ($('#discountCode').val().length) {
                $('#discountCode').attr('type', 'password');
            } else {
                $('#discountCode').attr('type', 'text');
            }
        });

    /************ Contact Details JS Ends ***************/



    /********************* Login JS Starts ***********************/

    // $(".pwd_btn").on('click', function(){
    //     $(".pwd_wrap").show();
        // $(".otp_wrap").hide();
    // })

    // $(".otp_btn").on('click', function(){
    //     $(".otp_wrap").show();
        // $(".pwd_wrap").hide();
    // })

    /********************* Login JS Starts ***********************/

    /******************** Local Storage For Order Page Starts ***********************/

        $('.proceed_btn').on('click', function(){
            // Blank to start with
            
            var order = {};

            // Loop through all inputs...
            $('input[type="text"], input[type="hidden"]').each(function(){
              // ...adding their values as properties
              order[this.name] = this.value;
            }); 

            // Store that object in JSON format
            localStorage.setItem("order", JSON.stringify(order));
        });

        var order = JSON.parse(localStorage.getItem("order") || "null");
        // if (order) {
        //     $('input[type="text"], input[type="hidden"]').each(function(){
        //         if (this.name in order) {
        //             this.value = order[this.name];
        //         } else {
        //             this.value = "";
        //         }
        //     });
        // }

        // var objectToSave = {};
        // $('.proceed_btn').on('click', function(){
        //     $('input[type="text"],input[type="hidden"]').each(function(){
        //       var elem = $(this);
        //       var id = elem.attr('id');
        //       var value = elem.val();
        //       objectToSave[id] = value;
        //     }); 
        //     localStorage.setItem('order', JSON.stringify(objectToSave));
        // });
        // $('#load').on('click', function(){
        //     $('input[type="text"]').each(function(){    
        //         var id = $(this).attr('id');
        //         var value = localStorage.getItem(id);
        //         $(this).val(value);
        //     }); 

        // });

    /******************** Local Storage For Order Page Ends ***********************/

    /************** Number Of Copies Notary Slider JS Starts ********************/

    $(function() {
        new Dragdealer('notary-slider', { 
        speed: 1,
        loose: true,
        steps: 10,
        animationCallback: function(x, y) {
           
         $('#notary-slider .value').text(Math.round((x * 9)+1));
         $("#notary-slider input").val(Math.round((x * 9)+1));
        }
    });
    })
    /************* Number of Notary Copies Slider JS Ends *********************/

    /************* for Input box in property starts *********************/

   
    /************* for Input box in property ends *********************/
});
