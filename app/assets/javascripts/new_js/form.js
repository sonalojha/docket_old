$(document).ready(function() {
	$('.next_form').on("click", function(){
			if(($('input').val() == '')){
			// alert('Input can not be left blank');
			return false;
			
		}
		else{
			$('.step_one').hide( "slide", { direction: "left"  }, 1000);
			$('.step_two').show( "slide", { direction: "left"  }, 1000);
		  	$("body").scrollTop(0);
			
		}
		
	})

	$('.previous_btn').on("click", function(){
		$('.step_one').show( "slide", { direction: "left"  }, 1000);
		$('.step_two').hide( "slide", { direction: "left"  }, 1000);
		  $("body").scrollTop(0);
	})

	$('.next_btn').on("click", function(){
		if(($('input').val() == '')){
			// alert('Input can not be left blank');
			return false;
			
		}
		else{
			$('.step_two').hide( "slide", { direction: "left"  }, 1000);
			$('.step_three').show( "slide", { direction: "left"  }, 1000);
			$("body").scrollTop(0);
			
		}
	})

	$('.previous_frm').on("click", function(){
		$('.step_three').hide( "slide", { direction: "left"  }, 1000);
		$('.step_two').show( "slide", { direction: "left"  }, 1000);
		  $("body").scrollTop(0);
	})

	$(".quantity").keypress(function (e) {
	    //if the letter is not digit then display error and don't type anything
	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	       	//display error message
	       	$("#errmsg").html("Digits Only").show().fadeOut("slow");
	        return false;
	    }
	});

})