json.array!(@services_orders_answers) do |services_orders_answer|
  json.extract! services_orders_answer, :id, :order_id, :question_id, :body
  json.url services_orders_answer_url(services_orders_answer, format: :json)
end
