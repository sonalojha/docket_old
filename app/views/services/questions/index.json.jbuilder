json.array!(@services_questions) do |services_question|
  json.extract! services_question, :id, :name, :service_id
  json.url services_question_url(services_question, format: :json)
end
