json.array!(@notifications) do |notification|
  json.extract! notification, :id, :template_id, :read, :notice_type, :user_id, :order_id
  json.url notification_url(notification, format: :json)
end
