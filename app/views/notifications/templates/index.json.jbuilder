json.array!(@notifications_templates) do |notifications_template|
  json.extract! notifications_template, :id, :name, :message
  json.url notifications_template_url(notifications_template, format: :json)
end
