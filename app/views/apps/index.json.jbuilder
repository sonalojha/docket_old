json.array!(@apps) do |app|
  json.extract! app, :id, :name, :value
  json.url app_url(app, format: :json)
end
