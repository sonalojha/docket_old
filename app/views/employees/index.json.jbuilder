json.array!(@employees) do |employee|
  json.extract! employee, :id, :email, :password, :phone, :parent_id, :city_id, :name
  json.url employee_url(employee, format: :json)
end
