json.array!(@partner_requests) do |partner_request|
  json.extract! partner_request, :id, :name, :email, :phone, :profession, :address, :message, :status_id, :note, :callback_request
  json.url partner_request_url(partner_request, format: :json)
end
