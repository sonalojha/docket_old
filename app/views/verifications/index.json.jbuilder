json.array!(@verifications) do |verification|
  json.extract! verification, :id, :code, :type, :status_id, :valid_till, :user_id
  json.url verification_url(verification, format: :json)
end
