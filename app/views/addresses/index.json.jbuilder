json.array!(@addresses) do |address|
  json.extract! address, :id, :locality_id, :city_id, :landmark, :address1, :address2, :tag, :status_id, :user_id, :pincode
  json.url address_url(address, format: :json)
end
