json.array!(@documents_questions) do |documents_question|
  json.extract! documents_question, :id, :question_number, :default_option_id, :response_type_id, :is_mandatory, :body, :associated_text
  json.url documents_question_url(documents_question, format: :json)
end
