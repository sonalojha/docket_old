json.array!(@documents_questions_options) do |documents_questions_option|
  json.extract! documents_questions_option, :id, :name, :question_id, :priority
  json.url documents_questions_option_url(documents_questions_option, format: :json)
end
