json.array!(@documents_questions_response_types) do |documents_questions_response_type|
  json.extract! documents_questions_response_type, :id, :name, :regex
  json.url documents_questions_response_type_url(documents_questions_response_type, format: :json)
end
