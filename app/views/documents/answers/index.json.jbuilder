json.array!(@documents_answers) do |documents_answer|
  json.extract! documents_answer, :id, :document_id, :section_id, :question_id, :body
  json.url documents_answer_url(documents_answer, format: :json)
end
