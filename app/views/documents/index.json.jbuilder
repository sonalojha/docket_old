json.array!(@documents) do |document|
  json.extract! document, :id, :name, :service_id, :stamp_required, :default_price, :tat, :automated, :description
  json.url document_url(document, format: :json)
end
