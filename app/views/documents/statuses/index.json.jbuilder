json.array!(@documents_statuses) do |documents_status|
  json.extract! documents_status, :id, :name
  json.url documents_status_url(documents_status, format: :json)
end
