json.array!(@documents_sections) do |documents_section|
  json.extract! documents_section, :id, :name, :section_no, :description
  json.url documents_section_url(documents_section, format: :json)
end
