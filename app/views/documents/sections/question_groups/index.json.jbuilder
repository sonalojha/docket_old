json.array!(@documents_sections_question_groups) do |documents_sections_question_group|
  json.extract! documents_sections_question_group, :id, :section_id, :question_id, :priority
  json.url documents_sections_question_group_url(documents_sections_question_group, format: :json)
end
