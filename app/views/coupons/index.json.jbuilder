json.array!(@coupons) do |coupon|
  json.extract! coupon, :id, :code, :start_date, :end_date, :coupon_type, :value, :limit, :count, :active
  json.url coupon_url(coupon, format: :json)
end
