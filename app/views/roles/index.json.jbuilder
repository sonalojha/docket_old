json.array!(@roles) do |role|
  json.extract! role, :id, :name, :role_type, :parent_id
  json.url role_url(role, format: :json)
end
