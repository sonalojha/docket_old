json.array!(@payments_paytms) do |payments_paytm|
  json.extract! payments_paytm, :id, :MID, :ORDERID, :TXNAMOUNT, :CURRENCY, :TXNID, :BANKTXNID, :STATUS, :RESPCODE, :RESPMSG, :TXNDATE, :GATEWAYNAME, :BANKNAME, :PAYMENTMODE, :CHECKSUMHASH
  json.url payments_paytm_url(payments_paytm, format: :json)
end
