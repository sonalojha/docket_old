json.array!(@payments) do |payment|
  json.extract! payment, :id, :mihpayid, :mode, :status, :amount, :hash, :pg_type, :bank_ref_num, :bankcode, :name_on_card, :cardnum, :payuMoneyId, :discount
  json.url payment_url(payment, format: :json)
end
