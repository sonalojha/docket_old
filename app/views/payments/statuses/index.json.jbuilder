json.array!(@payments_statuses) do |payments_status|
  json.extract! payments_status, :id, :name
  json.url payments_status_url(payments_status, format: :json)
end
