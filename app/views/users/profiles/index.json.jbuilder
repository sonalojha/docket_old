json.array!(@users_profiles) do |users_profile|
  json.extract! users_profile, :id, :first_name, :last_name, :middle_name, :profile_img, :cover_img, :last_login, :mobile_verified, :email_verified, :about
  json.url users_profile_url(users_profile, format: :json)
end
