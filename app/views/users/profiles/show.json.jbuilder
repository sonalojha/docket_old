json.extract! @users_profile, :id, :first_name, :last_name, :middle_name, :profile_img, :cover_img, :last_login, :mobile_verified, :email_verified, :about, :created_at, :updated_at
