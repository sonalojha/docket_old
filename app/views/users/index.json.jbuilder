json.array!(@users) do |user|
  json.extract! user, :id, :name, :email, :password, :city_id, :profile_type, :profile_id, :status_id, :parent_id, :phone
  json.url user_url(user, format: :json)
end
