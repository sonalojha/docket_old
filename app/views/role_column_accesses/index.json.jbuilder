json.array!(@role_column_accesses) do |role_column_access|
  json.extract! role_column_access, :id, :role_id, :column_name, :table_name
  json.url role_column_access_url(role_column_access, format: :json)
end
