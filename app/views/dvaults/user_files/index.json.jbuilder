json.array!(@dvaults_user_files) do |dvaults_user_file|
  json.extract! dvaults_user_file, :id, :filename, :tag, :mime_type, :vault_id, :size, :profile_id, :dvault_id, :thumbnail_image
  json.url dvaults_user_file_url(dvaults_user_file, format: :json)
end
