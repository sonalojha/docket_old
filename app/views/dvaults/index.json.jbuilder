json.array!(@dvaults) do |dvault|
  json.extract! dvault, :id, :user_id, :pin
  json.url dvault_url(dvault, format: :json)
end
