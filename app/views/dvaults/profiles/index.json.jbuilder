json.array!(@dvaults_profiles) do |dvaults_profile|
  json.extract! dvaults_profile, :id, :name, :dvault_id
  json.url dvaults_profile_url(dvaults_profile, format: :json)
end
