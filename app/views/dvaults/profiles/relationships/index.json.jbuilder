json.array!(@dvaults_profiles_relationships) do |dvaults_profiles_relationship|
  json.extract! dvaults_profiles_relationship, :id, :name
  json.url dvaults_profiles_relationship_url(dvaults_profiles_relationship, format: :json)
end
