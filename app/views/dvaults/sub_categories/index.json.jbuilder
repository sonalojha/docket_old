json.array!(@dvaults_sub_categories) do |dvaults_sub_category|
  json.extract! dvaults_sub_category, :id, :name, :category_id
  json.url dvaults_sub_category_url(dvaults_sub_category, format: :json)
end
