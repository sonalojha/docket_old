json.array!(@dvaults_categories) do |dvaults_category|
  json.extract! dvaults_category, :id, :name
  json.url dvaults_category_url(dvaults_category, format: :json)
end
