json.array!(@orders_delivery_partners) do |orders_delivery_partner|
  json.extract! orders_delivery_partner, :id, :name
  json.url orders_delivery_partner_url(orders_delivery_partner, format: :json)
end
