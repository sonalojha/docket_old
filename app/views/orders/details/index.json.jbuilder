json.array!(@orders_details) do |orders_detail|
  json.extract! orders_detail, :id, :name, :email, :mobile, :order_id
  json.url orders_detail_url(orders_detail, format: :json)
end
