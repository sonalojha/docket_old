json.array!(@orders_statuses) do |orders_status|
  json.extract! orders_status, :id, :name
  json.url orders_status_url(orders_status, format: :json)
end
