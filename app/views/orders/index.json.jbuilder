json.array!(@orders) do |order|
  json.extract! order, :id, :document_id, :user_id, :payment_id, :status_id, :stamp_amount, :service_id
  json.url order_url(order, format: :json)
end
