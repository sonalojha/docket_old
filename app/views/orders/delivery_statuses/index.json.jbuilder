json.array!(@orders_delivery_statuses) do |orders_delivery_status|
  json.extract! orders_delivery_status, :id, :name
  json.url orders_delivery_status_url(orders_delivery_status, format: :json)
end
