json.array!(@orders_feedbacks) do |orders_feedback|
  json.extract! orders_feedback, :id, :order_id, :message, :status_id, :note
  json.url orders_feedback_url(orders_feedback, format: :json)
end
