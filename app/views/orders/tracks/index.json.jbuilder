json.array!(@orders_tracks) do |orders_track|
  json.extract! orders_track, :id, :user_status
  json.url orders_track_url(orders_track, format: :json)
end
