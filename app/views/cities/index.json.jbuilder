json.array!(@cities) do |city|
  json.extract! city, :id, :name, :url_name, :state_id, :status_id
  json.url city_url(city, format: :json)
end
