json.extract! @attachment, :id, :s3_url, :file_id, :file_type, :tag, :name, :is_processed, :created_at, :updated_at
