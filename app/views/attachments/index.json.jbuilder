json.array!(@attachments) do |attachment|
  json.extract! attachment, :id, :s3_url, :file_id, :file_type, :tag, :name, :is_processed
  json.url attachment_url(attachment, format: :json)
end
