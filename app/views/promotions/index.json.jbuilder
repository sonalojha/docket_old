json.array!(@promotions) do |promotion|
  json.extract! promotion, :id, :name, :category, :valid_by, :offer, :offer_type
  json.url promotion_url(promotion, format: :json)
end
