class OrderJob < ActiveJob::Base
	queue_as :order_queue
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end

	def perform(*args)
		order_id,type,path = args[0], args[1], args[2]
		# order_id, type, user_id = args[0], args[1], args[2]
		# # order = Order.includes(:status, :delivery_info, payment: [:status]).where(id: order_id)
		case type
		when 'generate_invoice'
			OrderMailer.generate_invoice(order_id).deliver_later
		when 'order_invoice'
			OrderMailer.order_invoice(order_id).deliver_later
		when 'order_dispatch'
			OrderMailer.order_dispatched(order_id).deliver_later
		# when 'order_payment_received'
		# 	OrderMailer.order_payment(order_id).deliver_later
		when 'order_template'
			order = Order.find(order_id)
			verification = Verification.create(verification_type: 'Draft',user_id: order.user_id)
			activation_link = ENV['base_url'] + '/draft/' + order.txnid.to_s  + '/' + verification.code
	 		OrderMailer.order_template(order_id,path,activation_link).deliver_later
			puts "Order Draft #{order_id}"
		when 'error_template'
			order = Order.find(order_id)
	 		OrderMailer.error_template(order_id,path).deliver_now
			puts "Error In Draft #{order_id}"
		when 'order_thankyou'
			OrderMailer.thankyou_email(order_id).deliver_later
		else
			puts 'no matching type'
		end
	end

	protected
	def notify_manager
		puts 'draft sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception + 'order email sent'
	end
end
	
