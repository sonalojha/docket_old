class UserSignupJob < ActiveJob::Base
	queue_as :user_email
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end
  
	def perform(*args)
		user_id,type,activation_link = args[0], args[1], args[2]
		if type == 'signup'	
			# UserMailer.signup_email(user_id,activation_link).deliver_later
			UserMailer.welcome_email(user_id,activation_link).deliver_later
		elsif type == 'password'
			UserMailer.password_reset(user_id,activation_link).deliver_later
		elsif type == 'activated'
			UserMailer.activation_email(user_id).deliver_later
		elsif type == 'email_verify'
			UserMailer.activate_email(user_id,activation_link).deliver_later
		elsif type == 'file_email'
			UserMailer.send_file(user_id,email,file_id).deliver_later
		elsif type == 'verify_account'
			UserMailer.verify_account(user_id).deliver_later
		elsif type == 'otp'
			UserMailer.verify_otp(user_id,activation_link).deliver_later
		elsif type == 'dvault_active'
			UserMailer.dvault_email(user_id).deliver_later
		elsif type == 'pin_reset'
			UserMailer.pin_reset(user_id,activation_link).deliver_later
		end
	end
	protected
	def notify_manager
		puts 'email sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception + 'user_email'
	end

end