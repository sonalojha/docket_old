class SmsJob < ActiveJob::Base
	queue_as :sms_queue

	def perform(*args)
		user,code,type,order_id = args[0], args[1], args[2], args[3]
		user = User.find(user)
		if order_id && Order.find(order_id).detail
			mobile = Order.find(order_id).detail.mobile
		end
		if code
			msg1 = "OTP for Docket is #{code}. Please do not share it with anyone."
		end
		if order_id
			order = Order.find(order_id)
			msg2 = "Your order for #{order.product} has been successfully placed. Payment of Rs. #{order.final_amount} is received. Your order ID is #{order.txnid}."
			msg3 = "Hi! Your order #{order.txnid} will be delivered within 24 to 48 working hours by #{order.delivery_partner ? order.delivery_partner.name : 'FedEx'}. Your order tracking ID is #{order.delivery_tracking_no }."
		end
		mobile_no = user ? user.phone : mobile
		case type
			when 'sms_verify'
				msg = msg1
				send_sms(msg, mobile_no)
			when 'order_invoice'
				msg = msg2
				send_sms(msg, mobile_no)
			when 'order_dispatched'
				msg = msg3
				send_sms(msg, mobile_no)
			else
				puts 'no msg found'
			end
	end
	def send_sms msg,mobile
		uri = URI(ENV['sms_trxn_url'])
		params = { 	user: ENV['sms_username'], 
					pwd: ENV['sms_password'],
					to:  mobile,
					sid: ENV['sms_sender_id'], 
					msg: msg, 
					fl: 0,
					gwid: 2
				}
		uri.query = URI.encode_www_form(params)
		res = Net::HTTP.get_response(uri)
		puts "sms sent #{mobile}"
	end
end