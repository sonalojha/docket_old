class TemplateUploadJob < ActiveJob::Base
  queue_as :template_upload

  def perform(*args)
  	path = args[0]
    s3 = AWS::S3.new(
	  credentials: Aws::Credentials.new(ENV['S3_KEY'], ENV['S3_SECRET']),
	  region: ENV['S3REGION']
	)
	key = 'test_template.docx'
	obj = s3.bucket(ENV['S3_BUCKET']).object('key')
	obj.upload_file(path, acl:'public-read')
	obj.public_url
  end
end
