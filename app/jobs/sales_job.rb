class SalesJob < ActiveJob::Base
	queue_as :sales_queue

	def perform(*args)
		id = args[0]
		SalesMailer.new_request(id).deliver_later
	end
	
end