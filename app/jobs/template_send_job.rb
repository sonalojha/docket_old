class TemplateSendJob < ActiveJob::Base
  queue_as :template_send

  def perform(*args)
	order_id = args[0]
  	Document.create_template(order_id)
  	puts "Document created #{order_id}"
  end
end
