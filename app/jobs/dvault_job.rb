class DvaultJob < ActiveJob::Base
	queue_as :dvault
	after_perform :notify_manager

	rescue_from(StandardError) do |exception|
    	notify_failed_job_to_manager(exception)
  	end
  
	def perform(*args)
		user_id,email,file_ids = args[0], args[1], args[2]
		# if type == 'file_email'
		UserMailer.send_file(user_id,email,file_ids).deliver_later
		# else
			# 'nothing found to do'
		# end
	end
	protected
	def notify_manager
		puts 'dvault file sent'
	end
	private
	def notify_failed_job_to_manager(exception)
		puts exception + 'dvault email'
	end

end