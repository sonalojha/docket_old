class HomeController < ApplicationController
	before_action :init_variables
	include HomeHelper
	def check_permission
		return false
	end

	def check_login
		if current_user
			return true
		else
			redirect_to user_login_path
		end
	end

	##### initialize the following variables ######
	def init_variables
		if Rails.env.production?
		  @include_analytics = true
		elsif Rails.env.development?
		  @include_analytics = false
		end
		@payu = ENV['payu_url']
		@base_url = ENV['base_url']
		@site_state = params[:site_state] || params[:action] 
		@localities = Locality.includes(city: [state: [:country]]).all.order('name ASC')
		@cities = City.includes(state: [:country]).all.order('name ASC')
		@states = State.includes(:country).all.order('name ASC')
		@countries = Country.all.order('name ASC')
		@services = Service.all
		@meta = "Docket is an online platform for quick and affordable documentation and registrations.Create your legal documents instantly in a matter of minutes."
		if current_user && (User.exists? current_user)
		  @current_user = User.includes(:addresses, :profile).where(id: current_user).first
		else
			session[:user] = nil
		end
	end

	def campaign_order
		order = Order.new(service_id: params[:service_id])
		order.stamp_amount = params[:stamp_amount]
		order.document_id = params[:document_id]
		order.total_amount = params[:total_amount]
		order.delivery_amount = params[:delivery_amount] || '99'
		order.pickup_amount = params[:pickup_amount] #|| '99'
		order.product = params[:product]
		order.quantity = params[:quantity]
		order.user_id = params[:user_id] #if params[:user_id].nil?
		order.summary = params[:summary]
		if params[:coupon] && !params[:coupon].blank?
			coupon = Coupon.validate_coupon(params[:coupon])
			if coupon
				discount = Coupon.get_discount_amount(coupon,nil,params[:net_amount])
				if (discount.round != params[:discount]) && !(params[:net_amount].to_i > params[:total_amount].to_i) 
					order.total_amount = params[:net_amount].to_i
					order.discount = discount
				else
					# order.discount = params[:discount] #for this to work alter in invoice also
					order.coupon_id = coupon
				end
			end
		end
		order.gst = ENV['gst']
		order.gst_amount = (ENV['gst'].to_f/100) * order.total_amount.to_f
		order.final_amount = order.total_amount.to_f + order.gst_amount.to_f
		if order.save!
			if params[:attachment] && !params[:attachment].blank?
				url = Order.upload(params[:attachment].path,false,params[:attachment].original_filename.split('.').last)
				attachment = order.attachments.new(s3_url: url, name: params[:attachment].original_filename, 
							tag: 'User Format', mime_type: params[:attachment].original_filename.split('.').last)
				attachment.save!
				order.draft_required = false
				order.save!
			end
			details = Orders::Detail.find_or_initialize_by(order_id: order.id)
			details.name = params[:name]
			details.email = params[:email]
			details.mobile = params[:mobile]
			details.save!
			if !order.user
				user = User.check_user_status details
				order.user_id = user.id
				order.save!
			end
			if params[:answer]
				params[:answer].each do |k,v|
					if !v.nil?
						answer = order.answers.new(question_id: k, body: v)
						answer.save!
					end
				end
			end

			# redirect_to root_path, notice: 'Order Successfully Placed'
			# paytm order.id
			# icici order.id
			payu_money order.id
		end
	end

	def paytm order_id
		@options = Payment.generate_paytm_hash(order_id)
		@order = Order.find(order_id)
		@url = ENV['paytm_server']
		redirect_to "https://#{@options.host}#{@options.request_uri}"
		# render layout: false
	end

	def icici order_id
		# order_id=Order.last.id
		@options = Payments::Icici.icici_payment(order_id)
		@order = Order.find(order_id)
		@url = ENV['ICICI_SERVER']
		# redirect_to "https://#{@options.host}#{@options.request_uri}"
		render 'payment_icici', layout: false
	end

	def payu_money order_id
		@order = Order.find(order_id)
		uri = URI(ENV['payu_url'])
		@options =  Order.generate_payu_url(order_id)
		uri.query = URI.encode_www_form(@options)
		@url = ENV['payu_url']
		render 'payment_icici', layout: false
		# redirect_to "https://#{uri.host}#{uri.request_uri}"
	end

	###### / #######
	def index
		@site_state = 'home_bg'
		flash[:notice] = nil
		flash[:alert] = nil
	end
	

	def dvault_dashboard
		@site_state = 'evault_dashboard_inner_bg'
	end
	
	def checklist
		@site_state = 'evault_dashboard_bg'
		flash[:notice] = nil
		flash[:alert] = nil
	end

	######## Service page ########
	def service
		@slots = time_slots # generates time slots
		if current_user
			@service = Service.where(name: params[:service].split('-')
					  .map{|a| a.capitalize}.join('-')).first
			@site_state = 'flip_dashboard_bg'
			render "#{@service.name.split('-').join('').downcase}"
		else
			store_location
			redirect_to user_login_path, alert: 'Login required to place an order'
		end
	end

	####### User Login ##########
	def user_login
		# if params[:password] && !params[:password].empty?
		# 	user = User.authenticate(params[:email], params[:password], params[:mobile])
		# elsif params[:otp]
		# 	users = User.where(phone: params[:mobile], profile_type: "Users::Profile")
		# if !(params[:email].include? '@')
		# 	params[:mobile] = params[:email]
		# end
		# if params[:mobile]
			# users = User.where(phone: params[:mobile], profile_type: "Users::Profile")
		# else
			users = User.where(email: params[:email], profile_type: "Users::Profile")
		# end
		if users && (users.length == 1)
			verification = Verification.where(user_id: users.first.id, code: params[:otp], verification_type: 'Login').first
			if verification#.code == params[:otp]
				@user = users.first
			end
		end
		# @user = User.authenticate(params[:email],params[:password])

		if @user #&& @user.profile_type == 'Users::Profile'
			create_user_session @user.id
			# location_to_redirect = status['redirect']
			# if location_to_redirect.nil?
			redirect_to root_path, notice: 'Logged in successfully'
			# else
				# redirect_to location_to_redirect, notice: 'Logged in successfully'
			# end
		else
			render json: ::Api::ApiStatusList::UNKNOWN_ERROR, status: 400
		  # redirect_to user_login_path, alert: 'Email/Password provided is incorrect'
		end
	end

	######## User SignUp ##########
	def user_signup
		# if !(params[:user][:email].include? '@')
		# 	params[:user][:mobile] = params[:user][:email]
		# end
		# user = User.where(email: params[:user][:email], profile_type: 'Users::Profile')
		# if !user 
		# 	user = User.where(phone: params[:user][:mobile], profile_type: 'Users::Profile')
		# 	alert = 'Mobile Already Registered'
		# end
		if User.check_valid(params[:user][:email], params[:user][:mobile]) #(user.length == 0)
			user = User.new(user_params)
			user.profile_type = 'Users::Profile'
			user.name = params[:user][:full_name]
			profile = Users::Profile.new(first_name: params[:user][:full_name].split(' ').first, last_name: params[:user][:full_name].split(' ').last)
			if params[:phone_code]
				country = Country.where(phone_code: params[:phone_code].strip)
				profile.country_id = country.first.id if country.length > 0
			end
			profile.save!
			user.profile_id = profile.id
			user.save!
			create_user_session user.id
			redirect_to root_path, notice: 'Welcome to Docket'
			# user.verifications.create(verification_type: 'Login')
			# if (params[:user][:password] == params[:user][:confirm_password]) && user.save!
				# create_user_session user.id
			# 	redirect_to root_path, notice: 'Logged in successfully'
			# else
			# 	errors = user.errors
			# 	redirect_to user_signup_path, alert: 'Server Error! Try again!'
			# end
		else
			redirect_to root_path, alert: 'Email/Mobile already exist!'
		end	
	end

	######### Update User Profile #######
	def update_profile
		url = User.upload(params[:attachment].path, params[:attachment].original_filename.split('.').last) if !params[:attachment].nil?
		if params[:user_id] && User.exists?(params[:user_id]) && (current_user.to_s == params[:user_id])
			user = User.find(params[:user_id])
			if params[:phone]
				users = User.where(phone: params[:phone])
				if (users.length == 0)
					user.phone = params[:phone] 
				end
			end
			profile = user.profile
			profile.first_name = params[:first_name] if params[:first_name]
			profile.middle_name = params[:middle_name] if params[:middle_name]
			profile.last_name = params[:last_name] if params[:last_name]
			profile.gender = params[:gender] if params[:gender]
			profile.birthday = Date.parse(params[:birthday]) if params[:birthday]
			profile.profile_img = url if url
			profile.save!
			user.save
		end
		redirect_to profile_path, notice: 'Profile updated successfully'
	end

	######### Upload Id Proof #######
	def upload_idproof
		if params[:user_id] && (current_user.to_s == params[:user_id]) && (User.exists? params[:user_id]) 
			user = User.find(params[:user_id])
			if user 
				id_proof = User.upload(params[:id_proof].path, params[:id_proof].original_filename.split('.').last) if !params[:id_proof].nil?
				address_proof = User.upload(params[:address_proof].path, params[:address_proof].original_filename.split('.').last) if !params[:address_proof].nil?
				user.profile.address_proof_url = address_proof
				user.profile.id_proof_url = id_proof
				user.profile.save!
				redirect_to edit_profile_path, notice: 'Success', message: 'Id Proof updated successfully'
			else
				render json: ::Api::ApiStatusList::USER_NOT_FOUND, status: 400
			end
		else
		  render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	###### ADD Address #########
	def add_address
		response = Hash.new
		@shipping = true if (params[:shipping] == 'true')
		if !params[:tag].empty? && !params[:user_id].empty?
		  if !params[:country].empty?
			country = Country.find_or_initialize_by(name: params[:country].capitalize)
			country.save!
		  end
		  if !params[:state].empty?
			state = State.find_or_initialize_by(name: params[:state].capitalize)
			state.country_id = country.id
			state.save!
		  end
		  if !params[:city].empty?
			city = City.find_or_initialize_by(name: params[:city].capitalize)
			city.state_id = state.id
			city.save!
		  end
		  if !params[:locality].empty?
			locality = Locality.find_or_initialize_by(name: params[:locality].capitalize, city_id: city.id)
			locality.save!
		  end
		  address = Address.find_or_initialize_by(tag: params[:tag], user_id: params[:user_id])
		  address.pincode = params[:pincode] if params[:pincode]
		  address.address1 = params[:address1] if params[:address1]
		  address.address2 = params[:address2] if params[:address2]
		  address.city_id = city.id if !city.nil?
		  address.locality_id = locality.id if !locality.nil?
		  address.landmark = params[:landmark] if params[:landmark]
		  @address = address
		  if address.save!
			render partial: 'add_address', layout: false
		  else
			render json: ::Api::ApiStatusList::UNKNOWN_ERROR, status: 400
		  end
		else
		  render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	###### Edit Address #########
	def edit_address
		response = Hash.new
		if params[:user_id] && params[:address_id]
		  	@address = Address.where(id: params[:address_id], user_id: params[:user_id])
			if @address && (@address.length > 0)
				@address = @address.first
				render partial: 'edit_address', layout: false
			else
				render json: ::Api::ApiStatusList::UNKNOWN_ERROR, status: 400
			end
		else
			render json: ::Api::ApiStatusList::UNKNOWN_ERROR
		end
	end

	###### Update Address #########
	def update_address
		response = Hash.new
		if params[:user_id] && params[:address_id]
		  	@address = Address.where(id: params[:address_id], user_id: params[:user_id])
			if @address && (@address.length > 0)
				if !params[:city].empty?
					city = City.find_or_initialize_by(name: params[:city].capitalize)
					city.save!
				end
				if !params[:locality].empty?
					locality = Locality.find_or_initialize_by(name: params[:locality].capitalize, city_id: city.id)
					locality.save!
				end
		  		@address = @address.first
				@address.tag = params[:tag] if params[:tag]
				@address.pincode = params[:pincode] if params[:pincode]
				@address.address1 = params[:address1] if params[:address1]
				@address.address2 = params[:address2] if params[:address2]
		  		@address.landmark = params[:landmark] if params[:landmark]
				@address.city_id = city.id if !city.nil?
				@address.locality_id = locality.id if !locality.nil?
				@address.save!
				render json: ::Api::ApiStatusList::OK
			else
				render json: ::Api::ApiStatusList::UNKNOWN_ERROR, status: 400
			end
		else
			render json: ::Api::ApiStatusList::UNKNOWN_ERROR
		end
	end

	###### Delete Address #########
	def delete_address
		response = Hash.new
		if params[:user_id] && (current_user.to_s == params[:user_id]) && params[:address_id]
		  	@address = Address.where(id: params[:address_id], user_id: params[:user_id])
			if @address && (@address.length > 0)
				@address = @address.destroy_all
				render json: ::Api::ApiStatusList::OK
			else
				render json: ::Api::ApiStatusList::UNKNOWN_ERROR, status: 400
			end
		else
			render json: ::Api::ApiStatusList::UNKNOWN_ERROR
		end
	end

	####### change phone ######
	def change_mobile_number
		response = Hash.new
		if params[:user_id] && params[:old_mobile] && params[:new_mobile] && User.exists?(params[:user_id])
			user = User.find(params[:user_id])
			users = User.where(phone: params[:new_mobile], profile_type: 'Users::Profile')
			if users && (users.length == 0)
				if (params[:new_mobile] != params[:old_mobile]) 
					if params[:phone_code]
						country = Country.where(phone_code: params[:phone_code].strip)
						user.profile.country_id = country.first.id if country.length > 0
					else
						user.profile.country_id = Country.where(name: 'India').first.id
					end
					user.phone = params[:new_mobile]
					user.profile.mobile_verified = false
					# user.profile.country_id = params[:country_id].to_i
					user.profile.save!
					user.save!
					render json: Api::ApiStatusList::OK
				else
					render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
				end
			else
				render json: Api::ApiStatusList::ALREADY_EXIST, status: 400
			end
		else
			render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
		# render json: response
	end


	####### change email #######
	def change_email
		response = Hash.new
		if params[:user_id] && params[:new_email] && User.exists?(params[:user_id]) && (params[:new_email] == params[:confirm_email])
			user = User.find(params[:user_id])
			users = User.where(email: params[:new_email], profile_type: 'Users::Profile')
			if users && (users.length == 0)
				user.email = params[:new_email]
				user.profile.email_verified = false
				user.profile.save!
				user.save!
				render json: Api::ApiStatusList::OK
			else
				render json: Api::ApiStatusList::ALREADY_EXIST, status: 400
			end
		else
			render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
		# render json: response
	end

	####### change email #######
	def activate_email
		response = Hash.new
		if params[:user_id] && User.exists?(params[:user_id]) && (params[:user_id] == current_user.to_s)
			user = User.find(params[:user_id])
			if user
				user.verifications.create(verification_type: 'Activate')
				render json: Api::ApiStatusList::OK
			else
				render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
			end
		else
			render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
		# render json: response
	end

	####### change Password #####
	def change_password
		response = Hash.new
		if params[:user_id] && params[:old_password] && params[:new_password] && User.exists?(params[:user_id])
			user = User.find(params[:user_id])
			if (User.authenticate(user.email,params[:old_password])) && (params[:new_password] != params[:old_password]) && (params[:new_password] == params[:confirm_password]) 
				user.password = params[:new_password] 
				user.save!
				render json: Api::ApiStatusList::OK
			else

				render json: Api::ApiStatusList::INCORRECT_PASSWORD, status: 400
			end
		else
			render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
		# render json: response
	end

	####### verify mobile #####
	def verify_mobile
		response = Hash.new
		if params[:user_id] && User.exists?(params[:user_id])
			verify = User.find(params[:user_id]).verifications.create(verification_type: 'Otp')
			verify.save!
			render json: Api::ApiStatusList::OK
		else
			render json: Api::ApiStatusList::USER_NOT_FOUND, status: 400
		end
		# render json: response
	end

	####### Verfiy Otp #######
	def verify_otp
		response = Hash.new
		if params[:user_id] && User.exists?(params[:user_id]) && params[:otp]
			user = User.where(id: params[:user_id]).first
			if user && (current_user.to_s == params[:user_id])
				verify = Verification.where(code: params[:otp], verification_type: 'Otp', user_id: params[:user_id], status_id: Status.where(name: 'ACTIVE').first.id)
				if verify.length >= 1
					user.profile.mobile_verified = true
					user.profile.save!
					verify.first.status_id = Status.where(name: 'INACTIVE').first.id
					verify.first.save!
					render json: Api::ApiStatusList::OK
				else
					render json: Api::ApiStatusList::NOT_FOUND, status: 400
				end
			end
		else
			render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
		# render json: response
	end
	
	###### Track any Order ########	
	def track
		@site_state = 'track_dashboard_inner_bg'
		flash[:notice] = nil
		flash[:alert] = nil
	end

	####### Profile Page #########
	def profile
		@site_state = 'profile_dashboard_bg'
		flash[:notice] = nil
		flash[:alert] = nil
	end

	###### Edit Profile ######
	def edit_profile
		@site_state = 'profile_dashboard_inner_bg'
		flash[:notice] = nil
		flash[:alert] = nil
	end

	###### List all Order of User #######	
	def order
		@order_statuses = Orders::Status.all.where('name !=?', 'DELETED')
		@orders = Order.includes(:status,:address,:service,:document,:automated_answers,:delivery_partner,payment:[:status])
					.where(user_id: current_user, status_id: @order_statuses.pluck(:id))
					.order('id DESC')
		@site_state = 'order_dashboard_inner_bg'
		# flash[:notice] = nil 
		# flash[:alert] = nil
	end

	####### Checklist Dashboard #########	
	def checklist_dashboard
		@site_state = 'order_dashboard_inner_bg'
		flash[:notice] = nil
		flash[:alert] = nil
	end

	########## Create New Order ############
	def neworder
		if (current_user.to_s == params[:user_id].to_s)
			order = Order.create(user_id: params[:user_id], service_id: params[:service_id])
			order.stamp_amount = params[:stamp_amount]
			order.document_id = params[:document_id]
			order.total_amount = params[:total_amount]
			order.delivery_amount = params[:delivery_amount]
			order.pickup_amount = params[:pickup_amount]
			order.product = params[:product]
			if order.save!
				if !params[:attachment].nil?
					url = Order.upload(params[:attachment][:file].path,false,params[:attachment][:file].original_filename.split('.').last)
					attachment = order.attachments.new(s3_url: url, name: params[:attachment][:file].original_filename, 
								tag: 'User Format', mime_type: params[:attachment][:file].original_filename.split('.').last)
					attachment.save!
					order.draft_required = false
					order.save!
				end
				##### current order into session #####
				create_current_order order.id
				params[:answer].each do |k,v|
				  if !v.nil?
					answer = order.answers.new(question_id: k, body: v)
					answer.save!
				  end
				end
				if @current_user.addresses && (@current_user.addresses.length > 0)
					flash[:notice] = 'Select Address for delivery'
				else
					flash[:alert] = 'Add Delivery Address'
				end
				redirect_to shipping_address_path(txnid: order.txnid)
			else
				redirect_to shipping_address_path(txnid: order.txnid)
			end
		else
			redirect_to root_path
		end
	end

	####### Add shipping address to the Order ######
	def delivery
		response = Hash.new
		if !params[:order_id].empty? && !params[:address_id].empty? && (User.exists? params[:user_id])
			order = Order.where(id: params[:order_id], user_id: params[:user_id])
			if order && order.first
				order.first.address_id = params[:address_id].to_i
				if order.first.save!
					render json: ::Api::ApiStatusList::OK
				else
					render json: ::Api::ApiStatusList::UNKNOWN_ERROR, status: 400
				end
			else
				render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
			end
		else
			render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	########## Select Shipping Addresss ############
	def shipping_address
		@shipping = true
		@order = Order.where(txnid: params[:txnid], user_id: current_user, status_id: Orders::Status.where(name: 'INCOMPLETE')).first
		if @order 
			payment @order.id
			render 'neworder'
		elsif @order && @order.payment && (@order.payment.status.name == 'SUCCESS')
			redirect_to orders_path
		else
			redirect_to root_path, notice: 'Order Not Found'
		end
	end

	########## Apply Coupon ##########
	def apply_coupon
		response = Hash.new

		if params[:coupon] #&& params[:order_id]
			# orders = Order.where(id: params[:order_id])
			# if orders.length == 1
				coupon = Coupon.validate_coupon(params[:coupon])
				if coupon
					discount = Coupon.get_discount_amount(coupon,nil,params[:total_amount])
					# order = Order.find(order_id)
					# order.total_amount = order.total_amount.to_f - discount.to_f
					# @order = orders.first
					# @order.discount = discount
					# @order.coupon_id = coupon
					# @order.save!
					# add_coupon_to_order @order.first.id,coupon
					# payment @order.first.id
					# @coupon = Coupon.find(coupon)
					response.merge! ::Api::ApiStatusList::OK
					response['discount'] = discount
					render json: response, status: 200
				else
					render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
				end
			# else
				# render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
			# end
		else
			render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	def add_coupon_to_order order_id,coupon_id
		discount = Coupon.get_discount_amount(coupon_id,order_id)
		order = Order.find(order_id)
		# order.total_amount = order.total_amount.to_f - discount.to_f
		order.discount = discount
		order.coupon_id = coupon_id
		order.save!
		Coupon.update_count(coupon_id)
	end

	def remove_coupon
		if params[:order_id] #&& params[:user_id] && (current_user.to_s == params[:user_id])
			@order = Order.where(id: params[:order_id]).first #, user_id: params[:user_id]).first
			if @order.coupon
				@order.coupon.count -= 1
				@order.coupon.save! 
				@order.coupon_id = nil
				@order.discount = nil
				@order.save!
			end
			# payment order.id
			render partial: 'new_info_url'
			# render partial: 'new_payu_url'
		else
			render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end
	########## Create Payment Links ###########
	def payment order_id
		@options = Order.generate_payu_url(order_id)
		@order = Order.find(order_id)
		@key = ENV['payu_key']
		@url = ENV['payu_url']
		@salt =  ENV['payu_salt']
		# render 'payment'
	end
	
	########## Status of an Order ############
	def track_status
		@order = Order.where(txnid: params[:txnid]).first
		render layout: false
	end

	# def order_status
	# 	@order = Order.where(txnid: params[:txnid]).first
	# 	render layout: false
	# end
	########## Track an Order ############
	def track_order
		@order = Order.where(txnid: params[:txnid]).first
		@site_state = 'track_dashboard_inner_bg'
		render 'track'
	end

	def track_order_status
		@order = Order.where(txnid: params[:txnid]).first
		render :partial => 'layouts/order_status'
	end
	########### Cancel an Order ##########
	def cancel_order
		if params[:order_id]
			order = Order.find(params[:order_id])
			order.status_id = Orders::Status.where(name: 'DELETED').first.id
			order.save!
			if params[:redirect]
				redirect_to root_path, notice: 'Your Order has been cancelled'
			else
				render json: ::Api::ApiStatusList::OK
			end
		else
			render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end


	########### Order automation Questions ##########
	def order_automation
		# document_id = params[:id] || 5
		if params[:id]
			@order = Order.find(params[:id])
		end
		@document = Document.includes(sections:[questions: [:options, :response_type]]).where(id: @order.document_id).first
		@site_state = 'flip_dashboard_bg'
	end
	
	######### Generate Document from Template #########
	def generate_docx
		order = Order.find(params[:order_id])
		if !params[:attachment].nil?
			url = Order.upload(params[:attachment][:file].path)
			attachment = order.attachments.new(s3_url: url, name: params[:attachment][:file].original_filename, 
						tag: 'Draft attachment', mime_type: params[:attachment][:file].original_filename.split('.').last)
			attachment.save!
			answer = Documents::Answer.find_or_initialize_by(order_id: params[:order_id], document_id:params[:document_id],
						question_id: params[:attachment][:question_id], section_id: params[:attachment][:section_id])
			answer.body = url
			answer.save!
		end
		params[:answer].each do |answer, value|
			answer = Documents::Answer.find_or_initialize_by(order_id: params[:order_id], document_id:params[:document_id],
								question_id: answer.to_i, section_id: value[:section_id])
			answer.body = value[:body] || 'N/A'
			answer.save!
		end
		if (order.automated_answers.count == order.document.questions.count)
			TemplateSendJob.perform_later(params[:order_id])
			notice = 'You will receive an email shortly'
		else
			notice = 'Our customer support team will get back to you'
		end
		redirect_to orders_path, notice: notice
	end

	def draft_confirm
		if params[:order_id] && params[:user_id] && params[:confirm_draft]
			order = Order.find(params[:order_id])
			if order 
				if params[:confirm_draft] == 'yes'
					order.draft_confirmed = true
					flash[:notice] = 'Draft has been confirmed'
				elsif params[:confirm_draft] == 'no'
					order.correction = params[:corrections]
					flash[:notice] = 'Your correction has been noted'
				end
				order.save!
				redirect_to orders_path
			else
				redirect_to activate_path
			end
		else
			redirect_to activate_path
		end
	end

	########## Dvault Login ########
	def dvault_login
		if current_user && @current_user.dvault_activated && @current_user.dvault
			id = @current_user.dvault.id
			authenticate = Dvault.authenticate(id, params[:password])
			if authenticate
				redirect_to dvault_path
			else
				redirect_to dlogin_path, alert: 'Invalid Pin! Try again'
			end
		else
			redirect_to dlogin_path, alert: 'INVALID REQUEST! Try again'
		end
	end

	######### create dvault for user ######
	def create_dvault
		if params[:user_id] && current_user && current_user.to_s == params[:user_id]
			if !@current_user.dvault
				dvault = Dvault.new(pin: params[:password])
				dvault.user_id = params[:user_id]
				if dvault.save!
					@current_user.dvault_activated = true
					@current_user.save!
					UserSignupJob.perform_later(current_user,'dvault_active')
					redirect_to dvault_path, notice: 'Logged in to DVault'
				else
					redirect_to dlogin_path, alert: 'Invalid Pin'
				end
			else
				redirect_to dlogin_path, alert: 'Dvault not activated'
			end
		else
			redirect_to dlogin_path, alert: 'INVALID REQUEST'
		end
	end

	######### dvault reset pin #########
	# generate otp
	def reset_pin_request 
		response = Hash.new
		if params[:user_id] && User.exists?(params[:user_id]) && current_user.to_s ==  params[:user_id]
			user = User.find(params[:user_id])
			if user.dvault_activated
				verify = user.verifications.create(verification_type: 'Pin::Reset')
				verify.save!
				render json: Api::ApiStatusList::OK
			else
				render json: Api::ApiStatusList::INVALID_REQUEST, status: 400	
			end
		else
			render json: Api::ApiStatusList::USER_NOT_FOUND, status: 400
		end
	end
	##### VALIDATE OTP #######
	def pin_reset_otp
		response = Hash.new
		if params[:user_id] && User.exists?(params[:user_id]) && params[:otp]
			user = User.where(id: params[:user_id]).first
			if user && (current_user.to_s == params[:user_id])
				verify = Verification.where(code: params[:otp], verification_type: 'Pin::Reset', user_id: params[:user_id], status_id: Status.where(name: 'ACTIVE').first.id)
				if verify.length > 0
					verify.first.status_id = Status.where(name: 'INACTIVE').first.id
					verify.first.save!
					render partial: 'new_pin', layout: false
				else
					render json: Api::ApiStatusList::NOT_FOUND, status: 400
				end
			end
		else
			render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end
	##### RESET PIN ######
	def pin_reset
		if params[:user_id] && current_user && (current_user.to_s == params[:user_id])
			if @current_user.dvault && (params[:password] == params[:confirm_password])
				dvault = Dvault.where(user_id: params[:user_id]).first
				dvault.pin = params[:password]
				if dvault.save!
					redirect_to dvault_path, notice: 'PIN RESET SUCCESSFULL'
				else
					redirect_to dlogin_path, alert: 'Invalid Pin'
				end
			else
				redirect_to dlogin_path, alert: 'Dvault not activated'
			end
		else
			redirect_to dlogin_path, alert: 'INVALID REQUEST'
		end
	end

	###### List of all dvault files ######
	def dvault_files
		@category = Dvaults::Category.where(name: params[:category].capitalize).first
		@sub_categories = Dvaults::SubCategory.all.where(category_id: @category.id)
		@user = User.find current_user
		@files = ::Dvaults::UserFile.joins(:sub_category)
					.joins('INNER JOIN dvaults_categories on dvaults_categories.id=dvaults_sub_categories.category_id')
					.where("dvaults_user_files.dvault_id=#{@user.dvault.id}")
					.where("dvaults_categories.id=#{@category.id}")
					.select('dvaults_user_files.*')
		@site_state = 'evault_dashboard_bg'
		if @files && @files.length > 0
			flash[:notice] = "#{@files.length} files found"
		else
			flash[:notice] = "Upload Files to your DVault"
		end
	end

	###### Upload files into Dvault ######
	def upload_doc
		if params[:file]
			extension = params[:file].original_filename.split('.').last
			url = Dvaults::UserFile.upload(params[:file].path,extension)
			file = ::Dvaults::UserFile.new(
				filename: params[:filename],
				tag: params[:tag],
				mime_type: extension,
				size: params[:file].size,
				vault_uid: url,
				dvault_id: @current_user.dvault.id,
				thumbnail_image: params[:thumbnail_image],
				sub_category_id: params[:sub_category_id]
			)
			if file.save!
				redirect_to dvault_category_path(category: params[:category]), notice: 'File uploaded successfully'
			else
				redirect_to dvault_category_path(category: params[:category]), alert: 'Server Error! File uploading failed'
			end
		else
			redirect_to dvault_category_path, alert: 'INVALID REQUEST'
		end
	end

	###### List all Dvault category #######
	def dvault_category
		@categories = Dvaults::Category.all
		@site_state = 'dvault_category_inner_bg'
		flash[:notice] = "Select Category to upload/view files" if flash[:notice].nil?
	end

	####### Dvault File Edit ########
	def file_edit
		if params[:file_id] && (current_user == params[:user_id])
			file = Dvaults::UserFile.find(params[:file_id])
		else
			
		end
	end

	######### Dvault File Delete #########
	def file_delete
		if params[:user_id] && (current_user.to_s == params[:user_id]) && params[:file_ids]
			user = User.find(params[:user_id])
			if user.dvault
				dvault_id = user.dvault.id
				Dvaults::UserFile.where(id: params[:file_ids].split(','),dvault_id: dvault_id).destroy_all
				render json: ::Api::ApiStatusList::OK
			else
				render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
			end
		else
			render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	######## Dvault File Email
	def file_email
		if params[:user_id] && (current_user.to_s == params[:user_id]) && params[:file_ids]
			DvaultJob.perform_later(current_user,params[:email],params[:file_ids])
			render json: ::Api::ApiStatusList::OK
		else
			render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	######## Dvault File Email
	def file_zip
		
	end

	def failure
		@site_state = 'failure'
	end
	
	######### Order Feedback #########
	def order_feedback
		@feedback = Orders::Feedback.new(orders_feedback_params)
		if @feedback.save!
			render json: ::Api::ApiStatusList::OK
		else
			render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	def dvault_welcomemail
		render 'user_mailer/dvault_welcomemail', layout: false
	end
	#########################################
	############# New Design ################	
	#########################################
	def home
		render layout: "cart_app"
	end

	def know_more
		render layout: "cart_app"
	end

	def services
		@service = Service.where(name: params[:service].split('-')
					  .map{|a| a.capitalize}.join('-')).first
		if ['Notary', 'E-stamp', 'Attestation', 'Franking'].include? params[:service].capitalize 
			@document = Document.where(name: params[:service].capitalize).first
			render params[:service], layout: "cart_app"
		else
			case params[:service].to_s.strip
			when 'agreement'
				@meta = "An Agreement is an act of coming to a mutual decision, position or arrangement with the awareness and confirmation of corresponding legal rights and duties for a specific work and responsibility between two parties."
			when 'deeds'
				@meta = "An Affidavit is a written statement made by the person under oath, in front of notary public or authorized officer. Contact Docket Tech Bangalore for Affidavits."
			when 'bond'
				@meta = "A Bond is a legal document where two parties sign the contract and one of them has to satisfy the particular requirement stated in the contract. In other words, you can say a bond is a written and signed document where the person fulfills a specified condition."
			when 'deed'
				@meta = "Deeds are written documents that are signed, sealed, attested and delivered between parties affirming and confirming the interest or right of a company or individual for/against a property, followed by binding the parties into a legal commitment"
			when 'startup'
				@meta = "Docket Tech brings to you Fast Track Startup Registration Process. Get your company registered, Apply Online Now."
			when 'property'
				@meta = 'We deal with all property related legal documentation and registration services like Encumbrance Certificate, Gift Deed, Will Registration, Partition Deed, Legal Opinion, Rental Agreement Registration etc.'
			end
			# [ 'Startup', 'Marketplace', 'Paperwork', 'Property'].include? params[:service].capitalize
			@documents = Document.where(service_id: @service.id).order('priority ASC').limit(10)
			# @document = Document.where(name: params[:service].capitalize).first
			render 'top_documents', layout: "cart_app"
		# else
		# 	render params[:service].pluralize, layout: "cart_app"
		end
	end

	def create_order
		order = Order.new(service_id: params[:service_id])
		order.stamp_amount = params[:stamp_amount]
		order.document_id = params[:document_id]
		order.total_amount = params[:total_amount]
		order.delivery_amount = params[:delivery_amount] || '99'
		order.pickup_amount = params[:pickup_amount] || '99'
		order.product = params[:product]
		order.user_id = params[:user_id] #if params[:user_id].nil?
		order.summary = params[:summary]
		if order.save!
			create_current_order order.id
			params[:answer].each do |k,v|
			  if !v.nil?
				answer = order.answers.new(question_id: k, body: v)
				answer.save!
			  end
			end
			redirect_to payment_new_path(txnid: order.txnid)
		else
			redirect_to payment_new_path(txnid: order.txnid)
		end
	end

	def update_order_details
		@order = Order.where(txnid: params[:txnid], status_id: Orders::Status.where(name: 'INCOMPLETE')).first
		if  @order
			details = Orders::Detail.find_or_initialize_by(order_id: @order.id)
			details.name = params[:name]
			details.email = params[:email]
			details.mobile = params[:mobile]
			details.save!
			if !@order.user
				user = User.check_user_status details
				@order.user_id = user.id
				@order.save!
			end
			(create_user_session @order.user_id) if !current_user
			@options = Order.generate_payu_url(@order.id)
			@key = 	 ENV['payu_key']
			@url = 	 ENV['payu_url']
			@salt =  ENV['payu_salt']
			render partial: 'payu_form', layout: false
		else
			redirect_to root_path, alert: 'Order is being Processed'
		end
	end

	def update_order_address
		@order = Order.where(txnid: params[:txnid]).first
		if @order.id == current_order
			(create_user_session @order.user_id) if !current_user
		end
		if !@order
			@order = Order.where(id: params[:order_id]).first
		end
		if @order
		  	if !params[:country].empty?
				country = Country.find_or_initialize_by(name: params[:country].capitalize)
				country.save!
		  	end
		  	if !params[:state].empty?
				state = State.find_or_initialize_by(name: params[:state].capitalize)
				state.country_id = country.id
				state.save!
		  	end
		  	if !params[:city].empty?
				city = City.find_or_initialize_by(name: params[:city].capitalize)
				city.state_id = state.id
				city.save!
		  	end
		  	if !params[:locality].empty?
				locality = Locality.find_or_initialize_by(name: params[:locality].capitalize, city_id: city.id)
				locality.save!
		  	end
			address = Address.find_or_initialize_by(tag: params[:tag], order_id: @order.id)
			address.user_id = params[:user_id] if params[:user_id]
			address.pincode = params[:pincode] if params[:pincode]
			address.address1 = params[:address1] if params[:address1]
			address.address2 = params[:address2] if params[:address2]
			address.city_id = city.id if !city.nil?
			address.locality_id = locality.id if !locality.nil?
			address.landmark = params[:landmark] if params[:landmark]
			@address = address
		  	if address.save!
			  	@order.address_id = address.id
				@order.furlenco_offer = ((params[:furlenco_offer] == 'on') ? true : false)
			  	@order.save!
			  	OrderJob.perform_later(@order.id,'order_thankyou')
			  	OrderJob.perform_later(@order.id,'order_invoice')
        		SmsJob.perform_later(@order.user_id,nil,'order_invoice',@order.id)
				# conditon for automation redirection
				if (['Agreement', 'Affidavit', 'Deed', 'Bond'].include? @order.service.name) && (@order.document.automated == true)
					redirect_to automation_path(:id => @order.id), notice: 'Enter Details For draft!'
				else
					redirect_to root_path, notice: 'Order successfully Placed'
				end	
		  	else
				render thankyou_path(txnid: @order.txnid), alert: 'Network error! Try Again'
		  	end
		else
		  redirect_to root_path, alert: 'Order Not found'
		end
	end

	def product_details
		@document = Document.find(params[:document_id].split('-').first)
		@service = @document.service
		@meta = @document.description || "Docket helps you create your own <%= @document.name %>. It's simple and easy. It only takes a few minutes! Fast Online Process ·Transparent Pricing."
		if [ 'Startup', 'Marketplace', 'Paperwork', 'Property'].include? @document.service.name
			render @document.service.name.pluralize.downcase,layout: "cart_app"
		elsif ['Notary', 'E-stamp', 'Attestation', 'Franking'].include? @document.service.name.capitalize 
			render @document.service.name.downcase, layout: "cart_app"
		else
			render 'orders', layout: 'cart_app'
		end
	end

	def payments
		@order = Order.where(txnid: params[:txnid], status_id: Orders::Status.where(name: 'INCOMPLETE')).first
		if @order
			render layout: "cart_app"
		else
			redirect_to root_path
		end
	end

	def thankyou
		@order = Order.where(txnid: params[:txnid]).first if params[:txnid]
		# if (current_user == @order.user_id)
			if @order && @order.status.name == 'INPROGRESS' && !@order.address_id
				render layout: "cart_app"
			else
				redirect_to root_path, alert: 'Order not found'
			end
		# else
			# redirect_to root_path, alert: 'Order not found'
		# end
	end
	
	def welcome_email
		@user = User.last
		@url = ENV['base_url']
		render "user_mailer/welcome_email" , layout: false
	end

	def thankyou_email
		render "order_mailer/thankyou_email" , layout: false
	end

	def dvault_email
		render "user_mailer/dvault_email" , layout: false
	end	 

	def about_us
		render layout: "cart_app"
	end	 

	def helpcenter
		@meta = "Docket Helpdesk provides you an overview of the products and services. Each Section of this Helpdesk also gives you the definitions purpose and usage of the products with a prompt information."
		render layout: "cart_app"
	end

	def order_details
		render layout: "cart_app"
	end

	def team
		@meta = "Founded in 2015 by Managing Director Nandish Domlur, Amit Sandill as CEO and Co-founded Aditya Pratap, Docket has grown organically and now comprises of a fifteen strong team of Developers, Professionals and Operations' personnel."
		render layout: "cart_app"
	end

	def partners
		@meta = "Partner with DocketTech, an online platform for quick and affordable documentation and registrations.Create your legal documents instantly in a matter of minutes."
		render layout: "cart_app"
	end

	def privacy
		@meta = "Privacy Policy governs data collection and usage of the products, services, software, platform and websites (collectively, 'Services') provided by Docket Tech Solutions Private Limited and any of our affiliates (collectively, 'Docket')."
		render layout: "cart_app"
	end

	def terms
		@meta = "Please read Docket Tech Terms of Use carefully before accessing or using website/mobile app."
		render layout: "cart_app"
	end

	def contact_us
		@meta = "Contact DocketTech for quick and affordable documentation and registrations.Create your legal documents instantly in a matter of minutes."
		render layout: "cart_app"
	end

	def disclaimer
		@meta = "Docket is not a law firm and/or a chartered accountancy and/or a company secretary firm or run by any advocate or chartered accountant or company secretary and does not provide any legal advice/service/accounting and auditing service and users of this website should consult with their own advocate/lawyer/chartered accountant/company secretary for advice."
		render layout: "cart_app"
	end

	def mobile
		render layout: "cart_app"
	end

	def rental_agreement
		@service = Service.where(name: 'Agreement').first
		@document = Document.where(name: 'Campaign-01-R').length>0 ? Document.where(name: 'Campaign-01-R').first : Document.last
		render layout: false
	end

	def company_registration
		@base_url = ENV['base_url']
		@service = Service.where(name: 'Startup').first
		@document = Document.where(name: 'Campaign-02-C').length>0 ? Document.where(name: 'Campaign-02-C').first : Document.last
		render layout: false
	end
	def campaign_affidavit
		@service = Service.where(name: 'Affidavit').first
		@document = Document.where(name: 'Campaign-03-A').length>0 ? Document.where(name: 'Campaign-03-A').first : Document.last
		render layout: false
	end
	def campaign_estamp
		@service = Service.where(name: 'E-Stamp').first
		@document = Document.where(name: 'Campaign-04-E').length>0 ? Document.where(name: 'Campaign-04-E').first : Document.last
		render layout: false
	end
	def campaign_bond
		@service = Service.where(name: 'Bond').first
		@document = Document.where(name: 'Campaign-06-B').length>0 ? Document.where(name: 'Campaign-06-B').first : Document.last
		render layout: false
	end
	def gpa_spa
		@service = Service.where(name: 'Paperwork').first
		@document = Document.where(name: 'Campaign-05-P').length>0 ? Document.where(name: 'Campaign-05-P').first : Document.last
		render layout: false
	end

	####### verify mobile #####
	def login_otp
		response = Hash.new
		if params[:email] 
			# if !(params[:email].include? '@')
			# 	params[:mobile] = params[:email]
			# end
			# if params[:mobile]
				# user = User.where(phone: params[:mobile], profile_type: 'Users::Profile')
			# else
			user = User.where(email: params[:email], profile_type: 'Users::Profile')
			# end
			if user.length != 0
				verify = user.first.verifications.create(verification_type: 'Login')
				verify.save!
				render json: Api::ApiStatusList::OK
			else
				render json: Api::ApiStatusList::USER_NOT_FOUND, status: 400
			end
		else
			render json: Api::ApiStatusList::INVALID_REQUEST, status: 400
		end
	end

	def sitemap
		@document = Document.all
		respond_to do |format|
		  format.html { render layout: false }
		  format.xml  { render 'home/sitemap', layout: false  }
		end
	end

	def apple_app_site_association 
		association_json = File.read("#{Rails.root}/" + "apple-app-site-association")
		render :json => association_json, :content_type => "application/pkcs7-mime"
	end

	private
	def user_params
		params.require(:user).permit(:email, :password, :phone)
	end

	def delivery_info_params
		params.permit(:name, :email, :phone, :address1, :address2, :landmark, :pincode, :message, :order_id)
	end
    def orders_feedback_params
      params.permit(:order_id, :message, :status_id, :note)
    end

end