class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  def check_permission
    return true
  end
  # GET /orders
  # GET /orders.json 
  def index
    @orders = Order.all.includes(:user, :detail, :status,:document,:platform, payment: [:status]).where(status_id: Orders::Status.where(name: ['INPROGRESS', 'COMPLETED']).pluck(:id)).paginate(:page => params[:page], :per_page => 30).order('id Desc')
    @order_statuses = Orders::Status.all
    @delivery_statuses = Orders::DeliveryStatus.all 
    respond_to do |format|  
      format.html
      format.xls do
        send_data(
          Order.all.joins(:detail, :status, :payment, :platform).
          select(' orders.id, orders.txnid, orders.created_at, orders.product, orders_statuses.name as order_status, 
          orders.total_amount, orders.discount, platforms.name as platform_name, orders.document_id as document_id, 
          orders_details.name, orders_details.email, orders_details.mobile, orders.furlenco_offer as offer, 
          orders.final_amount as final_amount, orders.gst_amount as gst_amount')
          .to_xls,
          content_type: 'application/vnd.ms-excel',
          filename: 'orders.xls' 
        )
    end 
  end 
end

  # GET /orders/1
  # GET /orders/1.json 
  def show 
  end

  def order_invoice
    @base_url = ENV['base_url']
    @order = Order.includes(:status, :user, payment: [:status]).where(id: params[:id]).first
    if (['Notary', 'Attestation','Franking'].include? @order.service.name)
      @no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i 
    else
      @no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i + 1
    end
    render 'order_mailer/order_invoice',layout: false
  end

  def filter_order
    @statuses = Orders::Status.all
    render 'search'
  end

  def search_by
    if params[:status_id]
      @orders = Order.includes(:status,:document,:platform, payment: [:status]).where(status_id: params[:status_id]).order('created_at DESC').paginate(:page => params[:page], :per_page => 30).order('id Desc')
    elsif params[:txnid]
      @orders=Order.where(txnid: params[:txnid]).paginate(:page => params[:page], :per_page => 30).order('id Desc')
    else
      render json: ::Api::ApiStatusList::INVALID_REQUEST
    end
    @delivery_statuses = Orders::DeliveryStatus.all
    if !params[:page] || params[:page] == '1'
      render 'orders', layout: false
    else
      render 'orders'#, layout: false
    end
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  def edit_draft
    if params[:order_id] && params[:attachment][:file]
      order = Order.find(params[:order_id])
      key = order.draft_url.split('/').last
      s3_url = Order.upload(params[:attachment][:file].path,key)
      order.draft_url = s3_url
      order.save!
      redirect_to orders_path, notice: 'Draft Uploaded'
    else
      redirect_to orders_path, alert: 'Error Occured'
    end
  end

  def upload_soft_copy
    if params[:order_id] && params[:attachment][:file]
      order = Order.find(params[:order_id])
      key = 'SOFT_COPY_' + order.product.split(' ').join('_') + '_' + order.txnid + '.' + params[:attachment][:file].original_filename.split('.').last
      s3_url = Order.upload(params[:attachment][:file].path,key)
      order.soft_copy_url = s3_url
      order.save!
      redirect_to '/admin/orders', notice: 'Soft copy uploaded'
    else
      redirect_to '/admin/orders', alert: 'Error Occured'
    end
  end

  def get_track_details
    if params[:order_id]
      order = Order.where(id: params[:order_id]).first
      @orders_track = order.track
      render partial: 'orders/update_track_status'
    else
      render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 404
    end
  end
  
  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_status
    if params[:order_id] && params[:status_id]
      order = Order.find(params[:order_id])
      order.status_id = params[:status_id].to_i
      if order.save!
        render json: ::Api::ApiStatusList::OK
      else
        render json: ::Api::ApiStatusList::UNKOWN_ERROR
      end
    else
      render json: ::Api::ApiStatusList::INVALID_REQUEST
    end
  end

  def update_delivery
    if params[:order_id] && params[:status_id]
      order = Order.find(params[:order_id])
      order.delivery_status_id = params[:status_id].to_i
      if !params[:tracking_no].nil?
        order.delivery_tracking_no = params[:tracking_no] 
        order.status_id = Orders::Status.where(name: 'COMPLETED').first.id
        order.save!
      end
      if order.save!
        render json: ::Api::ApiStatusList::OK
      else
        render json: ::Api::ApiStatusList::UNKOWN_ERROR
      end
    else
      render json: ::Api::ApiStatusList::INVALID_REQUEST
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # def order_invoice
  #   if params[:id]
  #     OrderJob.perform_later(params[:id],'generate_invoice')
  #     redirect_to order_details_path, notice: 'Email Sent'
  #   else
  #     redirect_to order_details_path, notice: 'Email Not Sent'
  #   end
  # end

  def order_details
    @order = Order.includes(:user, :status, :service, answers: [:question], automated_answers: [question: [:sections]]).where(id: params[:id]).first
    @paytm = Payments::Paytm.all.where(ORDERID: @order.txnid)
    @icici = Payments::Icici.all.where(txnid: @order.txnid)
    @user = @order.user
  end

  def order_email
    if params[:body] && params[:order_id]
      OrderMailer.error_mailer(params[:order_id],params[:body]).deliver_later
      redirect_to orders_details_path(params[:order_id]), notice: 'Email send'
    else
      redirect_to orders_details_path(params[:order_id]), alert: 'Error sending mail'
    end
  end

  def create_draft
    if params[:order_id]
      @order = Order.find(params[:order_id])
      if @order.document.automated && (@order.automated_answers.length > 0)
        TemplateSendJob.perform_later(params[:order_id])
        # Document.create_template params[:order_id]
        redirect_to order_details_path(params[:order_id]), notice: 'Draft creation in queue'
      else
        redirect_to order_details_path(params[:order_id]), alert: 'Automation Not available'  
      end
    else
      redirect_to order_details_path(params[:order_id]), alert: 'Invalid Request'
    end
  end

  def order_sms
    
  end

  def order_notification
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:document_id, :user_id, :payment_id, :status_id, 
                      :stamp_amount, :total_amount, :delivery_amount,:service_id, :quantity,
                      :txnid, :invoice_url, :draft_confirmed, :soft_copy_url, :draft_required, :delivery_status_id, :delivery_partner_id, :delivery_tracking_no, :product, :platform_id, :address_id, :pickup_amount, :draft_created, :draft_url, :correction, :coupon_id, :discount,
                      order_attachments_attributes: [:id, :_destroy, :attachment])
    end
end
