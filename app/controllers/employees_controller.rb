class EmployeesController < ApplicationController
  layout 'employee'

  before_filter :require_login, :except => [:employee_sign_in, :employee_logout, :employee_login]
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  # before_action :check_for_login
  
  def require_login
    if loggedin_employee.nil?
      @errors = "Please login for this action"
      render action: "employee_sign_in"
    end
  end

  # GET /employees
  # GET /employees.json
  def index
    employee = User.find(loggedin_employee)
    if employee.roles.include?(Role.where(:name => 'admin').first)
      @employees = User.where(profile_type: 'Employee').order("email ASC").paginate(:page => params[:page], :per_page => 30)
    else
      @employees = User.find(loggedin_employee).get_children#.paginate(:page => params[:page], :per_page => 30)
    end
    # @employees = User.all
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    employee = User.find(params[:id])
    if (employee.check_parent loggedin_employee) || (employee.id == loggedin_employee)
      @employee = employee
    else
      @errors = "You can't view this employee profile as it is not assigned to you!!"
    end
  end

  # GET /employees/new
  def new
    @employee = User.new
  end

  # GET /employees/1/edit
  def edit
    employee = User.find(params[:id])
    if (employee.check_parent loggedin_employee) || (employee.id == loggedin_employee)
      @employee = employee
    else
      @errors = "You can't edit this employee profile as it is not assigned to you!!"
    end
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = User.new(employee_params)
    @employee.parent_id = loggedin_employee.to_i
    password = params[:employee][:password]
    unless password.nil? or password.empty?
      @employee.password = password
    else
      @employee.password = nil
    end
    if @employee.save
      #employeeMailer.welcome_email(@employee).deliver
      redirect_to @employee, notice: 'employee was successfully created.'# We have dispatched the password on the corresponding email address.'
    else
      render action: "new"
    end
    # @employee = User.new(employee_params)

    # respond_to do |format|
    #   if @employee.save
    #     format.html { redirect_to @employee, notice: 'User was successfully created.' }
    #     format.json { render action: 'show', status: :created, location: @employee }
    #   else
    #     format.html { render action: 'new' }
    #     format.json { render json: @employee.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    employee = User.find(params[:id])
    if (employee.check_parent loggedin_employee) || (employee.id == loggedin_employee)
      @employee = employee
      password = params[:employee][:password]
      unless password.nil? or password.empty?
        @employee.password = password
      end

      if @employee.update_attributes(employee_params)
        redirect_to @employee, notice: 'User was successfully updated.'
      else
        render action: "edit"
      end
    else
      @errors = "You can't edit this employee profile as it is not assigned to you!!"
      render action: "edit"
    end
    # respond_to do |format|
    #   if @employee.update(employee_params)
    #     format.html { redirect_to @employee, notice: 'User was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: 'edit' }
    #     format.json { render json: @employee.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    employee = User.find(params[:id])
    if (employee.check_parent loggedin_employee) || (employee.id == loggedin_employee)
      employee.destroy
    else
      @errors = "You can't delete this employee profile as it is not assigned to you!!"
    end
    
    redirect_to employees_url
    # @employee.destroy
    # respond_to do |format|
    #   format.html { redirect_to employees_url }
    #   format.json { head :no_content }
    # end
  end

  # GET /employees/1/edit_role
  def edit_role
    employee = User.find(params[:id])
    if (employee.check_parent loggedin_employee) || (employee.id == loggedin_employee)
      @employee = employee
      @roles = User.find(loggedin_employee).roles
      @current_roles = @employee.roles
      @available_roles = Array.new
      @roles.each do |role|
        role.get_children.each do |cur_role|
          if !@current_roles.include?(cur_role)
            @available_roles.push(cur_role)
          end
        end
      end
    else
      @errors = "You can't edit roles of this employee profile as it is not assigned to you!!"
    end
  end
  
  def add_role
    employee  = User.find(params[:id])
    if (employee.check_parent loggedin_employee) || (employee.id == loggedin_employee)
      @employee = employee
      @employee.roles.push(Role.find(params[:role_id]))
      if @employee.save
        redirect_to @employee, notice: 'role was successfully added.'
      else
        render action: "edit_role"
      end
    else
      @errors = "You can't edit roles of this employee profile as it is not assigned to you!!"
      render action: "edit_role"
    end
  end
  
  def remove_role
    employee  = User.find(params[:id])
    if (employee.check_parent loggedin_employee) || (employee.id == loggedin_employee) || employee.roles.include?(Role.where(:name => 'admin').first)
      @employee = employee
      if @employee.roles.include?(Role.find(params[:role_id]))
        @employee.roles.delete(Role.find(params[:role_id]))
        if @employee.save
          redirect_to @employee, notice: 'role was successfully removed.'
        else
          render action: "edit_role"
        end
      else
        redirect_to @role, notice: 'you don\'t have it .'
      end
    else
      @errors = "You can't edit roles of this employee profile as it is not assigned to you!!"
      render action: "edit_role"
    end
  end
  
  
  def employee_login
    if @authenticated_employee = User.authenticate(params[:email], params[:password])
      status = create_employee_session(@authenticated_employee.id)
      location_to_redirect = status['redirect']
      if location_to_redirect.nil?
        redirect_to "/admin/employees"
      else
        redirect_to location_to_redirect
      end
    else
      render action: "employee_sign_in"
    end
  end
  
  def employee_sign_in
    if loggedin_employee.nil?
      respond_to do |format|
        format.html # employee_sign_in.html.erb
      end
    else
      redirect_to employee_path(loggedin_employee)
    end
  end
  
  def employee_logout
    destroy_employee_session
    redirect_to action: "employee_sign_in"
  end

  private

    # def check_for_login
    #   @employee = User.find(loggedin_employee) if loggedin_employee
    # end
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:email, :phone, :parent_id, :city_id, :name, :profile_type)
    end
end
