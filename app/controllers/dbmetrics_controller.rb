class DbmetricsController < ApplicationController
	layout 'employee'

  def check_permission
    true
  end

  def index
    @incomplete_orders_count    =    Order.all.where(status_id: Orders::Status.where(name: 'INCOMPLETE').first.id).count
    @inprogress_orders_count    =    Order.all.where(status_id: Orders::Status.where(name: 'INPROGRESS').first.id).count
    @completed_orders_count     =    Order.all.where(status_id: Orders::Status.where(name: 'COMPLETED').first.id).count
  	@canceled_orders_count 		  =    Order.all.where(status_id: Orders::Status.where(name: 'DELETED').first.id).count
  	@localities_count 	        =    Locality.all.count
  	@attachments_count 	        =    Attachment.all.count
  	@documents_count 		        =    Document.all.count
  	@dvault_count 		          =    Dvault.all.count
  	@dvault_files_count 			  =    Dvaults::UserFile.all.count
    @successfull_payments_count =    Payment.all.where(status_id: Payments::Status.where(name: 'SUCCESS').first.id).count
  	@failed_payments_count      =    Payment.all.where(status_id: Payments::Status.where(name: 'FAILED').first.id).count
  	@employees_count 			      =    User.where(profile_type: 'Employee').count
  	@user_count				          =    User.all.where('profile_type != ?', 'Employee').count
  	@contact_request_count			=    ContactRequest.all.count
    @android_device_count       =    DeviceRegistration.all.where(platform_id: 2).count
  	@ios_device_count	          =    DeviceRegistration.all.where(platform_id: 3).count
  	@partner_request_count	    =    PartnerRequest.count
  end

  def service_order_count 
    @services = Service.all.joins(:orders => :payment).where('payments.status_id = 1')
    @documents = Document.joins(:orders).where('orders.status_id = 2 or orders.status_id = 3').uniq
  end

end
