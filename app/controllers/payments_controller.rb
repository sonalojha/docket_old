class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy]
  layout 'employee', :except => :payu_response
  protect_from_forgery :except => [:payu_response, :payu_mobile, :failure, :paytm_response, :paytm_mobile_response, :icici_response]
  before_action :check_permission, :except => [:payu_response, :payu_mobile, :failure, :paytm_response, :paytm_mobile_response, :icici_response]

  def check_permission
    true
  end

  # GET /payments
  # GET /payments.json
  def index
    @payments = Payment.all.includes(:status).paginate(:page => params[:page], :per_page => 30).order('id')
  end

  # GET /payments/1
  # GET /payments/1.json
  def show 
  end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit
  end
  def generate_url(url, params = {})
    uri = URI(url)
    uri.query = params.to_query
    uri.to_s
  end

  def paytm_response
    paytm_response = Payments::Paytm.new(paytm_params)
    paytm_response.save!
    payment = Payment.find_or_initialize_by(txnid: params['ORDERID'],
                        amount: params['TXNAMOUNT'], #: params['CURRENCY'], 
                        mihpayid: params['TXNID'],#: params['BANKTXNID'],
                        pg_type: params['GATEWAYNAME'],
                        bankcode: params['BANKNAME'], mode: params['PAYMENTMODE'], 
                        secret_hash: params['CHECKSUMHASH']
                         #: params['RESPCODE'], : params['RESPMSG'], : params['TXNDATE'],
                        )
    # payment.status_id = 
    order = Order.where(txnid: params[:ORDERID]).first
    payment.order_id = order.id
    if payment.save!
      if params['RESPCODE'] == '01'
        payment.status_id = Payments::Status.where(name: 'SUCCESS').first.id
        payment.save!
        Order.update_status(params[:ORDERID],params[:status]='success')
        redirect_to thankyou_path(txnid: order.txnid), notice: 'Payment Successfully done!'
      else
        payment.status_id = Payments::Status.where(name: 'FAILED').first.id
        payment.save!
        redirect_to failure_path(txnid: order.txnid), alert: 'Payment Error. We are resolving It'
      end
      # if order.draft_required && order.document_id
      # else
        # redirect_to thankyou_path(txnid: order.txnid), notice: 'Payment Successfully done!'
      # end
    else
        redirect_to rental_agreement_path, alert: 'Payment Error. We are resolving It'
        # redirect_to failure_path(txnid: order.txnid), alert: 'Payment Error. We are resolving It'
    end
  end

  def paytm_mobile_response
    paytm_response = Payments::Paytm.new(paytm_params)
    paytm_response.save!
    paytmHASH = Hash.new
    paytm_params.keys.each do |k|
      paytmHASH[k] = params[k]
    end
    if paytmHASH['ORDERID'] && !paytmHASH['ORDERID'].blank?
      @paytmHASH = ::Paytm::ChecksumTool.new.get_checksum_verified_array(paytmHASH).to_json
      payment = Payment.find_or_initialize_by(txnid: params['ORDERID'],
                          amount: params['TXNAMOUNT'],
                          mihpayid: params['TXNID'],
                          pg_type: params['GATEWAYNAME'],
                          bankcode: params['BANKNAME'], mode: params['PAYMENTMODE'], 
                          secret_hash: params['CHECKSUMHASH']
                          )
      order = Order.where(txnid: params[:ORDERID]).first
      payment.order_id = order.id
      if Payments::Paytm.check_paytm_status(params['ORDERID'])
        if payment.save!
          if (params['RESPCODE'] == '01')
            payment.status_id = Payments::Status.where(name: 'SUCCESS').first.id
            payment.save!
            Order.update_status(params[:ORDERID],params[:status]='success')
            # redirect_to thankyou_path(txnid: order.txnid), notice: 'Payment Successfully done!'
          else
            payment.status_id = Payments::Status.where(name: 'FAILED').first.id
            payment.save!
            # redirect_to failure_path(txnid: order.txnid), alert: 'Payment Error. We are resolving It'
          end
          puts "#{@paytmHASH}"
        # else
            # redirect_to rental_agreement_path, alert: 'Payment Error. We are resolving It'
        end
      else
        payment.status_id = Payments::Status.where(name: 'FAILED').first.id
        payment.save!
        @paytmHASH = {"IS_CHECKSUM_VALID":"N"}
      end
    else
      @paytmHASH = {"IS_CHECKSUM_VALID":"N"}
    end
    render layout: false
  end

  def payu_response
    payment = Payment.find_or_initialize_by(mihpayid: params[:mihpayid],mode: params[:mode], amount: params[:amount],
                      secret_hash: params[:hash], pg_type: params[:PG_TYPE],bank_ref_num: params[:bank_ref_num],
                      bankcode: params[:bankcode], name_on_card: params[:name_on_card], cardnum: params[:cardnum],
                      payuMoneyId: params[:payuMoneyId], discount: params[:discount], payu_status: params[:status],
                      first_name: params[:first_name], last_name: params[:last_name],
                      email: params[:email], phone: params[:phone], txnid: params[:txnid]
                      )
    order = Order.where(txnid: params[:txnid]).first
    payment.order_id = order.id if order
    payment.save!
    if order && Order.is_valid_transaction?(params[:txnid],params[:amount]) #&& params[:status] == 'success'
      if params[:status] == 'success'
        # if !order.address_id 
        #   order.address_id = (params[:udf1].to_i == 0) ? nil : params[:udf1].to_i
        # end
        # order.save!
        payment.status_id = Payments::Status.where(name: 'SUCCESS').first.id
      else
        payment.status_id = Payments::Status.where(name: 'FAILED').first.id
      end
      payment.save!
      Order.update_status(params[:txnid],params[:status])
      if params[:status] == 'success'
        redirect_to thankyou_path(txnid: order.txnid), notice: 'Payment Successfully done!'
        # OrderJob.perform_later(order.id, 'order_payment_received', current_user)
        # SmsJob.perform_later(current_user, nil, nil, 'order_payment', current_order)
      else  
        redirect_to failure_path(txnid: order.txnid), alert: 'Payment Error. We are resolving It'
      end
      # UserSignupJob.perform_later(order.user.id,'verify_account') if !order.user.profile.profile_verified
    else
      redirect_to failure_path(txnid: order.txnid), alert: 'Payment Error. We are resolving It'
    end
  end

  def payu_mobile
    response = Hash.new
    result = Hash.new
    order = Order.where(txnid: params[:txnid]).first
    if order
      # order. = (params[:udf2].to_i == 0) ? nil : params[:udf2].to_i

      order.address_id = ((params[:udf1].to_i == 0) ? nil : params[:udf1].to_i) if order.address_id.nil?
      order.coupon_id = ((params[:udf3].to_i == 0) ? nil : params[:udf3].to_i) if order.coupon_id.nil?
      order.discount = ((params[:udf4].to_i == 0) ? nil : params[:udf4].to_s) if (order.discount.nil? || (order.discount != params[:udf4].to_s))
      order.save!
    end
    if Order.is_valid_transaction?(params[:txnid],params[:amount]) #&& params[:status] == 'success'
      payment = Payment.new(mihpayid: params[:mihpayid], mode: params[:mode], amount: params[:amount],
                        secret_hash: params[:hash], pg_type: params[:PG_TYPE], bank_ref_num: params[:bank_ref_num],
                        bankcode: params[:bankcode], name_on_card: params[:name_on_card], cardnum: params[:cardnum],
                        payuMoneyId: params[:payuMoneyId], discount: params[:discount], payu_status: params[:status],
                        first_name: params[:first_name], last_name: params[:last_name],
                        email: params[:email], phone: params[:phone], txnid: params[:txnid]
                        )
      # order = Order.where(txnid: params[:txnid]).first
      payment.order_id = order.id
      if params[:status] == 'success'
        # order.save!
        payment.status_id = Payments::Status.where(name: 'SUCCESS').first.id
        UserSignupJob.perform_later(order.user.id,'verify_account') if !order.user.profile.profile_verified
      else
        payment.status_id = Payments::Status.where(name: 'FAILED').first.id
      end
      # OrderJob.perform_later(order.id, 'order_payment_received', current_user)
      # SmsJob.perform_later(current_user, nil, nil, 'order_payment', current_order)
      # OrderJob.perform_later(order.id,'order_placed',current_user)
      # SmsJob.perform_later(current_user, nil, nil, 'order_placed', order.id)
      payment.save!
      Order.update_status(params[:txnid],params[:status])
      if params[:status] == 'success'
        @text = 'Success' #response.merge! Api::ApiStatusList::OK
      else
        @text = 'Failed'
      end
    else
      @text = 'Failed' #response.merge! Api::ApiStatusList::UNKNOWN_ERROR
    end
    render layout: 'payu_money'
  end

  def icici_response
    response = Hash.new
    result = Hash.new
    icici_response = Payments::Icici.new(icici_params)
    icici_response.response_dump = request.params.to_s
    icici_response.save!
    order = Order.where(txnid: params[:txnid]).first
    if order
      payment = Payment.new(mihpayid: params[:ipgTransactionId], mode: params[:paymentMethod], amount: params[:chargetotal],
                  secret_hash: params[:response_hash], pg_type: params[:PG_TYPE], bank_ref_num: params[:endpointTransactionId],
                  bankcode: params[:bankcode], name_on_card: params[:bname], cardnum: params[:cardnumber],
                  payuMoneyId: params[:terminal_id], discount: params[:discount], payu_status: params[:status],
                  first_name: params[:first_name], last_name: params[:last_name],
                  email: params[:email], phone: params[:phone], txnid: params[:txnid], order_id: order.id
                  )
      if params[:processor_response_code] == '00' && params[:status] == 'success'
        payment.status_id = Payments::Status.where(name: 'SUCCESS').first.id
        payment.save!
        Order.update_status(params[:txnid],params[:status]='success')
        redirect_to thankyou_path(txnid: order.txnid), notice: 'Payment Successfully done!'
      else
        payment.status_id = Payments::Status.where(name: 'FAILED').first.id
        payment.save!
        redirect_to failure_path(txnid: order.txnid), alert: 'Payment Error. We are resolving It'
      end
    else
        redirect_to failure_path(txnid: order.txnid), alert: 'Order Not Found'
    end
  end

  def failure
    render 'website/fail_transaction'
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to @payment, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: @payment }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:mihpayid, :mode, :status, :amount, :secret_hash, :pg_type, :bank_ref_num, :bankcode, :name_on_card, :cardnum, :payuMoneyId, :discount, :txnid, :payu_status, :status_id, :order_id, :email, :phone, :first_name, :last_name)
    end

    def paytm_params
      params.permit(:MID, :ORDERID, :TXNAMOUNT, :CURRENCY, :TXNID, :BANKTXNID, :STATUS, 
        :RESPCODE, :RESPMSG, :TXNDATE, :GATEWAYNAME, :BANKNAME, :PAYMENTMODE, :CHECKSUMHASH)
    end
    def icici_params
      params.permit(:endpointTransactionId,:approval_code,:terminal_id,:ipgTransactionId,:currency,
          :chargetotal,:timezone,:cccountry,:oid,:txnid,:ccbin,:tdate,:response_hash,:transactionNotificationURL,
          :txndatetime,:status,:cardnumber,:processor_response_code,:expmonth,:expyear,:ccbrand,:txntype,
          :txndate_processed,:paymentMethod,:response_hash_validation,:responseHash_validationFrom,
          :response_code_3dsecure,:bname)
    end
end
