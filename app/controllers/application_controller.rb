class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  public
  include SessionsHelper
  include Mobvious::Rails::Controller
  protect_from_forgery with: :exception

  def check_permission
    return true
  end

  if Rails.env.production?
    rescue_from ActionController::RoutingError, :with => :route_not_found_error
    rescue_from ActionController::BadRequest, :with => :bad_request_error
    rescue_from StandardError, :with => :render_server_error
  end
  
  protected
  def route_not_found_error
    render "shared/404", :status => 404, layout: false
  end

  def bad_request_error
    render "shared/404", :status => 404, layout: false
  end

  def render_server_error
    render "shared/500", :status => 500, layout: false
  end

  private
  before_filter do |controller|
    if controller.check_permission # && loggedin_employee
      current_employee = loggedin_employee
      if current_employee.nil?
        request_roles = Role.where(:name => "open_user")
      else
        @loggedin_employee = current_employee
        request_roles = User.find(current_employee).roles
      end
      
      if request_roles.include?(Role.where(:name => 'admin').first)
        has_permission = true
      else
        resource_tree = get_resource_tree(controller.controller_name.camelize()+"Controller", controller.action_name)
        has_permission = false
        resource_tree.each do |resource|
          if (resource.roles & request_roles).count > 0
            has_permission = true
            break
          end
        end
      end

      if !has_permission
        flash[:notice] = "You don't have access to this section."
        deny_access
      end
    # else
    #   flash[:notice] = "Login to view"
    #   deny_access
    end
  end
  
  def get_resource_tree(controller, action)
    # OPTIMIZE: Implement using recursive query
    resource_tree = Array.new
    @root = Resource.where(:name => "root").first
    @controller = Resource.where(:name => controller).first
    @action = Resource.where(:name => action, :parent_id => @controller.id).first
    
    resource_tree.push(@action)
    resource_tree.push(@controller)
    resource_tree.push(@root)
    resource_tree
  end
  
  def get_accessible_columns(table_name, roles)
    column_accesses = Array.new
    roles.each do |role|
      column_accesses |= role.role_column_accesses.where(:table_name => table_name).select("column_name")
    end

    selectable_fields = ["id"]
    column_accesses.each do |column_access|
      selectable_fields.push(column_access.column_name)
    end
    return selectable_fields
  end
  
  def get_table_columns(table_name)
    table_name = table_name.capitalize.singularize
    return eval("#{table_name.capitalize.singularize}.column_names")
  end
  
  protected
  def apply_access_control(records)
    return apply_column_control(apply_row_control(records))
  end
  
  
  def apply_column_control(records)
    
    current_employee = loggedin_employee
    if current_employee.nil?
      @request_roles = Role.where(:name => "open_user")
    else
      @request_roles = User.find(current_employee).roles
    end

    
    if records.instance_of?(ActiveRecord::Relation)
      table_name = records.table.name
      selectable_fields = get_accessible_columns(table_name, @request_roles)
      return records.select(selectable_fields)
    elsif records.instance_of?(Array)
      if records.count == 0
        return []
      else
        table_name = records[0].class.table_name
        model_name = records[0].class.model_name
        selectable_fields = get_accessible_columns(table_name, @request_roles)
        current_selected_fields = records[0].attributes.keys() | ["id"]
        
        final_selected_fields = (selectable_fields & current_selected_fields)
        selected_records = records.collect(&:id)
        return  eval("#{model_name}.select(final_selected_fields).find(selected_records)")
      end
    else 
      table_name = records.class.table_name
      model_name = records.class.model_name
      selectable_fields = get_accessible_columns(table_name, @request_roles)
      current_selected_fields = records.attributes.keys() | ["id"]
        
      final_selected_fields = (selectable_fields & current_selected_fields)
        
      return eval("#{model_name}.select(final_selected_fields).find(#{records.id})")
    end
  end
  
  
  def apply_row_control(records)
    
    current_employee = loggedin_employee
    
    if records.instance_of?(ActiveRecord::Relation)
      if current_employee.nil?
        return records.where("id = -1")
      elsif records.count == 0
        return records
      else
        employee = User.find(current_employee)
        model_name = records[0].class.model_name
        selected_records = records.collect(&:id)
        available_records = eval("employee.#{model_name.plural}.collect(&:id)")
        final_records = selected_records & available_records
        selected_fields = records[0].attributes.keys()
        return eval("#{model_name}.select(selected_fields).where('id in (?)', final_records)")
      end
    elsif records.instance_of?(Array)
      if current_employee.nil?
        return []
      elsif records.count == 0
        return []
      else
        employee = User.find(current_employee)
        model_name = records[0].class.model_name
        selected_records = records.collect(&:id)
        available_records = eval("employee.#{model_name.plural}.collect(&:id)")
        final_records = selected_records & available_records
        selected_fields = records[0].attributes.keys()
        return eval("#{model_name}.select(selected_fields).find(final_records)")
      end
    else
      if current_employee.nil?
        raise Exceptions::RowAccessControlViolation
      else
        employee = User.find(current_employee)
        if records.employees.include?(employee)
          return records
        else
          raise Exceptions::RowAccessControlViolation
        end
      end
    end
      
  end
  
  
  def save_after_access_control(record)
    current_employee = loggedin_employee
    if current_employee.nil?
      raise Exceptions::RowAccessControlViolation
    else
      employee = User.find(current_employee)
      if record.employees.include?(employee)
        record.save
      else
        raise Exceptions::RowAccessControlViolation
      end
    end
  end
  
  def update_attributes_after_access_control(record, params)
    current_employee = loggedin_employee
    if current_employee.nil?
      raise Exceptions::RowAccessControlViolation
    else
      employee = User.find(current_employee)
      if record.employees.include?(employee)
        record.update_attributes(params)
      else
        raise Exceptions::RowAccessControlViolation
      end
    end
  end
  
end
