class Services::QuestionsController < ApplicationController
  before_action :set_services_question, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /services/questions
  # GET /services/questions.json
  def index
    @services_questions = Services::Question.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /services/questions/1
  # GET /services/questions/1.json
  def show
  end

  # GET /services/questions/new
  def new
    @services_question = Services::Question.new
  end

  # GET /services/questions/1/edit
  def edit
  end

  # POST /services/questions
  # POST /services/questions.json
  def create
    @services_question = Services::Question.new(services_question_params)

    respond_to do |format|
      if @services_question.save
        format.html { redirect_to @services_question, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @services_question }
      else
        format.html { render :new }
        format.json { render json: @services_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/questions/1
  # PATCH/PUT /services/questions/1.json
  def update
    respond_to do |format|
      if @services_question.update(services_question_params)
        format.html { redirect_to @services_question, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @services_question }
      else
        format.html { render :edit }
        format.json { render json: @services_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/questions/1
  # DELETE /services/questions/1.json
  def destroy
    @services_question.destroy
    respond_to do |format|
      format.html { redirect_to services_questions_url, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_services_question
      @services_question = Services::Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def services_question_params
      params.require(:services_question).permit(:name, :service_id)
    end
end
