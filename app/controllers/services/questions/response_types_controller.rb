class Services::Questions::ResponseTypesController < ApplicationController
  before_action :set_services_questions_response_type, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /services/questions/response_types
  # GET /services/questions/response_types.json
  def index
    @services_questions_response_types = Services::Questions::ResponseType.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /services/questions/response_types/1
  # GET /services/questions/response_types/1.json
  def show
  end

  # GET /services/questions/response_types/new
  def new
    @services_questions_response_type = Services::Questions::ResponseType.new
  end

  # GET /services/questions/response_types/1/edit
  def edit
  end

  # POST /services/questions/response_types
  # POST /services/questions/response_types.json
  def create
    @services_questions_response_type = Services::Questions::ResponseType.new(services_questions_response_type_params)

    respond_to do |format|
      if @services_questions_response_type.save
        format.html { redirect_to @services_questions_response_type, notice: 'Response type was successfully created.' }
        format.json { render :show, status: :created, location: @services_questions_response_type }
      else
        format.html { render :new }
        format.json { render json: @services_questions_response_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/questions/response_types/1
  # PATCH/PUT /services/questions/response_types/1.json
  def update
    respond_to do |format|
      if @services_questions_response_type.update(services_questions_response_type_params)
        format.html { redirect_to @services_questions_response_type, notice: 'Response type was successfully updated.' }
        format.json { render :show, status: :ok, location: @services_questions_response_type }
      else
        format.html { render :edit }
        format.json { render json: @services_questions_response_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/questions/response_types/1
  # DELETE /services/questions/response_types/1.json
  def destroy
    @services_questions_response_type.destroy
    respond_to do |format|
      format.html { redirect_to services_questions_response_types_url, notice: 'Response type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_services_questions_response_type
      @services_questions_response_type = Services::Questions::ResponseType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def services_questions_response_type_params
      params.require(:services_questions_response_type).permit(:name)
    end
end
