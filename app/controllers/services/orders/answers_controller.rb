class Services::Orders::AnswersController < ApplicationController
  before_action :set_services_orders_answer, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /services/orders/answers
  # GET /services/orders/answers.json
  def index
    @services_orders_answers = Services::Orders::Answer.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /services/orders/answers/1
  # GET /services/orders/answers/1.json
  def show
  end

  # GET /services/orders/answers/new
  def new
    @services_orders_answer = Services::Orders::Answer.new
  end

  # GET /services/orders/answers/1/edit
  def edit
  end

  # POST /services/orders/answers
  # POST /services/orders/answers.json
  def create
    @services_orders_answer = Services::Orders::Answer.new(services_orders_answer_params)

    respond_to do |format|
      if @services_orders_answer.save
        format.html { redirect_to @services_orders_answer, notice: 'Answer was successfully created.' }
        format.json { render :show, status: :created, location: @services_orders_answer }
      else
        format.html { render :new }
        format.json { render json: @services_orders_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/orders/answers/1
  # PATCH/PUT /services/orders/answers/1.json
  def update
    respond_to do |format|
      if @services_orders_answer.update(services_orders_answer_params)
        format.html { redirect_to @services_orders_answer, notice: 'Answer was successfully updated.' }
        format.json { render :show, status: :ok, location: @services_orders_answer }
      else
        format.html { render :edit }
        format.json { render json: @services_orders_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/orders/answers/1
  # DELETE /services/orders/answers/1.json
  def destroy
    @services_orders_answer.destroy
    respond_to do |format|
      format.html { redirect_to services_orders_answers_url, notice: 'Answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_services_orders_answer
      @services_orders_answer = Services::Orders::Answer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def services_orders_answer_params
      params.require(:services_orders_answer).permit(:order_id, :question_id, :body)
    end
end
