class ContactRequestsController < ApplicationController
  before_action :set_contact_request, only: [:show, :edit, :update, :destroy]
  before_filter :require_login, only: [:edit, :update, :destroy, :index]
  layout 'employee'
  
  def require_login
    if loggedin_employee.nil?
      @errors = "Please login for this action"
      redirect_to employee_sign_in_path
    end
  end

  def check_permission
    false    
  end

  # GET /contact_requests
  # GET /contact_requests.json
  def index
    @contact_requests = ContactRequest.all.paginate(:page => params[:page], :per_page => 30).order('id DESC')
    respond_to do |format|  
       format.html
       format.xls do
        send_data(
          ContactRequest.all.select('id, name, email, phone,created_at,message,request_type').to_xls,
          content_type: 'application/vnd.ms-excel',
          filename: 'requests.xls' 
        )
      end
   end 
  end

  # GET /contact_requests/1
  # GET /contact_requests/1.json
  def show
  end

  # GET /contact_requests/new
  def new
    @contact_request = ContactRequest.new
  end

  # GET /contact_requests/1/edit
  def edit
  end

  # POST /contact_requests
  # POST /contact_requests.json
  def create
    @contact_request = ContactRequest.new(contact_request_params)

    respond_to do |format|
      if @contact_request.save
        format.html { redirect_to @contact_request, notice: 'Contact request was successfully created.' }
        format.json { render :show, status: :created, location: @contact_request }
      else
        format.html { render :new }
        format.json { render json: @contact_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contact_requests/1
  # PATCH/PUT /contact_requests/1.json
  def update
    respond_to do |format|
      if @contact_request.update(contact_request_params)
        format.html { redirect_to @contact_request, notice: 'Contact request was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact_request }
      else
        format.html { render :edit }
        format.json { render json: @contact_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contact_requests/1
  # DELETE /contact_requests/1.json
  def destroy
    @contact_request.destroy
    respond_to do |format|
      format.html { redirect_to '/admin/contact_requests', notice: 'Contact request was successfully destroyed.' }
      format.json { head :no_content }
    end
    # redirect_to '/admin/contact_requests'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_request
      @contact_request = ContactRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_request_params
      params.require(:contact_request).permit(:name, :email, :phone, :message, :request_type, :note, :status_id)
    end
end
