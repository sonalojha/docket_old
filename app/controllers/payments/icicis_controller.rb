class Payments::IcicisController < ApplicationController
  before_action :set_payments_icici, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  
  # GET /payments/icicis
  # GET /payments/icicis.json
  def index
    @payments_icicis = Payments::Icici.all
  end

  # GET /payments/icicis/1
  # GET /payments/icicis/1.json
  def show
  end

  # GET /payments/icicis/new
  def new
    @payments_icici = Payments::Icici.new
  end

  # GET /payments/icicis/1/edit
  def edit
  end

  # POST /payments/icicis
  # POST /payments/icicis.json
  def create
    @payments_icici = Payments::Icici.new(payments_icici_params)

    respond_to do |format|
      if @payments_icici.save
        format.html { redirect_to @payments_icici, notice: 'Icici was successfully created.' }
        format.json { render :show, status: :created, location: @payments_icici }
      else
        format.html { render :new }
        format.json { render json: @payments_icici.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/icicis/1
  # PATCH/PUT /payments/icicis/1.json
  def update
    respond_to do |format|
      if @payments_icici.update(payments_icici_params)
        format.html { redirect_to @payments_icici, notice: 'Icici was successfully updated.' }
        format.json { render :show, status: :ok, location: @payments_icici }
      else
        format.html { render :edit }
        format.json { render json: @payments_icici.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/icicis/1
  # DELETE /payments/icicis/1.json
  def destroy
    @payments_icici.destroy
    respond_to do |format|
      format.html { redirect_to payments_icicis_url, notice: 'Icici was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payments_icici
      @payments_icici = Payments::Icici.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payments_icici_params
      params.require(:payments_icici).permit(:endpointTransactionId, :approval_code, :terminal_id, :ipgTransactionId, :currency, :chargetotal, :timezone, :cccountry, :oid, :txnid, :ccbin, :tdate, :response_hash, :transactionNotificationURL, :txndatetime, :status, :cardnumber, :processor_response_code, :expmonth, :expyear, :ccbrand, :txntype, :txndate_processed, :paymentMethod, :response_hash_validation, :responseHash_validationFrom, :response_code_3dsecure, :bname, :response_dump)
    end
end
