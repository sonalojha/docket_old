class Payments::PaytmsController < ApplicationController
  before_action :set_payments_paytm, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /payments/paytms
  # GET /payments/paytms.json
  def index
    @payments_paytms = Payments::Paytm.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /payments/paytms/1
  # GET /payments/paytms/1.json
  def show
  end

  # GET /payments/paytms/new
  def new
    @payments_paytm = Payments::Paytm.new
  end

  # GET /payments/paytms/1/edit
  def edit
  end

  # POST /payments/paytms
  # POST /payments/paytms.json
  def create
    @payments_paytm = Payments::Paytm.new(payments_paytm_params)

    respond_to do |format|
      if @payments_paytm.save
        format.html { redirect_to @payments_paytm, notice: 'Paytm was successfully created.' }
        format.json { render :show, status: :created, location: @payments_paytm }
      else
        format.html { render :new }
        format.json { render json: @payments_paytm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/paytms/1
  # PATCH/PUT /payments/paytms/1.json
  def update
    respond_to do |format|
      if @payments_paytm.update(payments_paytm_params)
        format.html { redirect_to @payments_paytm, notice: 'Paytm was successfully updated.' }
        format.json { render :show, status: :ok, location: @payments_paytm }
      else
        format.html { render :edit }
        format.json { render json: @payments_paytm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/paytms/1
  # DELETE /payments/paytms/1.json
  def destroy
    @payments_paytm.destroy
    respond_to do |format|
      format.html { redirect_to payments_paytms_url, notice: 'Paytm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payments_paytm
      @payments_paytm = Payments::Paytm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payments_paytm_params
      params.require(:payments_paytm).permit(:MID, :ORDERID, :TXNAMOUNT, :CURRENCY, :TXNID, :BANKTXNID, :STATUS, :RESPCODE, :RESPMSG, :TXNDATE, :GATEWAYNAME, :BANKNAME, :PAYMENTMODE, :CHECKSUMHASH)
    end
end
