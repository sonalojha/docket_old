class Payments::StatusesController < ApplicationController
  before_action :set_payments_status, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /payments/statuses
  # GET /payments/statuses.json
  def index
    @payments_statuses = Payments::Status.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /payments/statuses/1
  # GET /payments/statuses/1.json
  def show
  end

  # GET /payments/statuses/new
  def new
    @payments_status = Payments::Status.new
  end

  # GET /payments/statuses/1/edit
  def edit
  end

  # POST /payments/statuses
  # POST /payments/statuses.json
  def create
    @payments_status = Payments::Status.new(payments_status_params)

    respond_to do |format|
      if @payments_status.save
        format.html { redirect_to @payments_status, notice: 'Status was successfully created.' }
        format.json { render :show, status: :created, location: @payments_status }
      else
        format.html { render :new }
        format.json { render json: @payments_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/statuses/1
  # PATCH/PUT /payments/statuses/1.json
  def update
    respond_to do |format|
      if @payments_status.update(payments_status_params)
        format.html { redirect_to @payments_status, notice: 'Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @payments_status }
      else
        format.html { render :edit }
        format.json { render json: @payments_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/statuses/1
  # DELETE /payments/statuses/1.json
  def destroy
    @payments_status.destroy
    respond_to do |format|
      format.html { redirect_to payments_statuses_url, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payments_status
      @payments_status = Payments::Status.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payments_status_params
      params.require(:payments_status).permit(:name)
    end
end
