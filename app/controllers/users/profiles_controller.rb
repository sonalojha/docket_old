class Users::ProfilesController < ApplicationController
  before_action :set_users_profile, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /users/profiles
  # GET /users/profiles.json
  def index
    @users_profiles = Users::Profile.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /users/profiles/1
  # GET /users/profiles/1.json
  def show
  end

  # GET /users/profiles/new
  def new
    @users_profile = Users::Profile.new
  end

  # GET /users/profiles/1/edit
  def edit
  end

  # POST /users/profiles
  # POST /users/profiles.json
  def create
    @users_profile = Users::Profile.new(users_profile_params)

    respond_to do |format|
      if @users_profile.save
        format.html { redirect_to @users_profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @users_profile }
      else
        format.html { render :new }
        format.json { render json: @users_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/profiles/1
  # PATCH/PUT /users/profiles/1.json
  def update
    respond_to do |format|
      if @users_profile.update(users_profile_params)
        format.html { redirect_to @users_profile, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_profile }
      else
        format.html { render :edit }
        format.json { render json: @users_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/profiles/1
  # DELETE /users/profiles/1.json
  def destroy
    @users_profile.destroy
    respond_to do |format|
      format.html { redirect_to users_profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_profile
      @users_profile = Users::Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_profile_params
      params.require(:users_profile).permit(:first_name, :last_name, :middle_name, :profile_img, :cover_img,
        :last_login, :mobile_verified, :email_verified, :about, :gender, :sign_in_count, :current_sign_in_at, :last_sign_in_at,
        :current_sign_in_ip, :last_sign_in_ip, :birthday, :profile_verified, :country_id, :address_proof_url, :id_proof_url)
    end
end
