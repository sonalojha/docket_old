class RoleColumnAccessesController < ApplicationController
  before_action :set_role_column_access, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  def check_permission
    return true
  end
  # GET /role_column_accesses
  # GET /role_column_accesses.json
  def index
    @role_column_accesses = RoleColumnAccess.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /role_column_accesses/1
  # GET /role_column_accesses/1.json
  def show
  end

  # GET /role_column_accesses/new
  def new
    @role_column_access = RoleColumnAccess.new
  end

  # GET /role_column_accesses/1/edit
  def edit
  end

  # POST /role_column_accesses
  # POST /role_column_accesses.json
  def create
    @role_column_access = RoleColumnAccess.new(role_column_access_params)

    respond_to do |format|
      if @role_column_access.save
        format.html { redirect_to @role_column_access, notice: 'Role column access was successfully created.' }
        format.json { render :show, status: :created, location: @role_column_access }
      else
        format.html { render :new }
        format.json { render json: @role_column_access.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /role_column_accesses/1
  # PATCH/PUT /role_column_accesses/1.json
  def update
    respond_to do |format|
      if @role_column_access.update(role_column_access_params)
        format.html { redirect_to @role_column_access, notice: 'Role column access was successfully updated.' }
        format.json { render :show, status: :ok, location: @role_column_access }
      else
        format.html { render :edit }
        format.json { render json: @role_column_access.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /role_column_accesses/1
  # DELETE /role_column_accesses/1.json
  def destroy
    @role_column_access.destroy
    respond_to do |format|
      format.html { redirect_to role_column_accesses_url, notice: 'Role column access was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role_column_access
      @role_column_access = RoleColumnAccess.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_column_access_params
      params.require(:role_column_access).permit(:role_id, :column_name, :table_name)
    end
end
