class RolesController < ApplicationController
  layout 'employee'

  before_action :set_role, only: [:show, :edit, :update, :destroy]
  before_filter :require_login

  def require_login
    if loggedin_employee.nil?
      @errors = "Please login for this action"
      render controller: "employees", action: "employee_sign_in"
    end
  end

  
  # GET /roles
  # GET /roles.json
  def index
    @roles = User.find(loggedin_employee).get_children_roles
  end

  # GET /roles/1
  # GET /roles/1.json
  def show
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])
    else
      @errors = "You can't view this role as it is not assigned to you"
    end
    
  end

  # GET /roles/new
  def new
    @role = Role.new
    @possible_parents = Role.all
  end

  # GET /roles/1/edit
  def edit
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])
      @possible_parents = Role.where(["id != ?",@role.id])
    else
      @errors = "You can't edit this role as it is not assigned to you"
    end
  end

  # POST /roles
  # POST /roles.json
  def create
    employee = User.find(loggedin_employee)
    @role = Role.new(role_params)
    if @role.save
      redirect_to @role, notice: 'Role was successfully created.'
    else
      render action: "new"
    end
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])

      if @role.update_attributes(role_params)
        redirect_to @role, notice: 'Role was successfully updated.'
      else
        render action: "edit"
      end
    else
      @errors = "You can't update this roles as it is not assigned to you"
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])
      @role.destroy
      redirect_to roles_url
    else
      @errors = "You can't delete this role as it is not assigned to you"
    end
  end

  def edit_column_access
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])
      @owned_accesses = Hash.new
      @role.role_column_accesses.each do |access|
        if @owned_accesses[access.table_name].nil?
          @owned_accesses[access.table_name] = Array.new
        end
        @owned_accesses[access.table_name].push(access.column_name)
      end
    
      @available_accesses = Hash.new
      employee.roles.each do |role|
        if role.name == 'open_user'
          next
        end
        role.role_column_accesses.each do |access|
          if @available_accesses[access.table_name].nil?
            @available_accesses[access.table_name] = Array.new
          end
          if access.column_name == '*'
            @available_accesses[access.table_name] |=['*']

            if @owned_accesses[access.table_name].nil?
              @available_accesses[access.table_name]|=get_table_columns(access.table_name)
            elsif !@owned_accesses[access.table_name].include?('*')
              @available_accesses[access.table_name]|=get_table_columns(access.table_name)
            end
          else
            if !@available_accesses[access.table_name].include?(access.column_name)
              @available_accesses[access.table_name].push(access.column_name)
            end
          end
        end
      end
    else
      @errors = "You can't edit this role as it is not assigned to you"
    end
  
  end
  
  def toggle_column_access
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])

      table_name = params[:table_name]
      column_name = params[:column_name]
    
    
      if @role.role_column_accesses.where(:table_name => table_name, :column_name => column_name).count == 0
        @role.role_column_accesses.create(:table_name => table_name, :column_name => column_name)
      else
        @role.delete_column_accesses(table_name, column_name, cascade = true)
      end
    
      if @role.save
        redirect_to edit_column_access_path(@role), notice: 'Access was successfully added.'
      else
        render action: "edit_column_access"
      end
    else
      @errors = "You can't edit this role as it is not assigned to you"
      render action: edit_column_access
    end
  end

  def edit_permission
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id]) == false
      @errors = "You can't edit this role as it is not assigned to you"
      return
    end
    @role = Role.find(params[:id])
    @available_resources = []
    @owned_ids = []
    @role.resources.each do |resource|
      @owned_ids<<resource.id        
    end
    resources = []
    employee.roles.each do |role|
      resources |= role.resources unless role.name == 'open_user'
    end
    resources.each do |resource|
      name = resource.get_parents().collect{|r| r.name.camelize}.join("::")
      r = resource.get_children_tree
      r.name = name
      @available_resources.push(r)
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: {:owned_resource_ids => @owned_ids, :available_resources => @available_resources} }
    end
    #render :json => {:owned_resource_ids => @owned_ids, :available_resources => @available_resources}
    
  end
  
  def add_permission
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])
      @resource = Resource.find(params[:resource_id])

      has_permission = false
      @current_resource = @resource
      while !@current_resource.nil?
        if (@current_resource.roles & employee.roles).count != 0
          has_permission = true
          break
        else
          @current_resource = @current_resource.parent
        end
      end
      if has_permission
        @role.resources.push(@resource)
        if @role.save
          response = {"STATUS" => "SS", "MESSAGE" => "Permission successfully added"}
          render :json => response
        else
          response = {"STATUS" => "FF", "MESSAGE" => "Unable to add Permission"}
          render :json => response
        end
      else
        response = {"STATUS" => "FF", "MESSAGE" => "You don't have this permission"}
        render :json => response
      end
    else
      response = {"STATUS" => "FF", "MESSAGE" => "The role you are trying to edit is not assigned to you"}
      render :json => response
    end
  end
  
  def delete_permission
    employee = User.find(loggedin_employee)
    if employee.check_child_role(params[:id])
      @role = Role.find(params[:id])
      resource = Resource.find(params[:resource_id])
    
      if @role.resources.include?(resource)
        @role.delete_resource(resource, cascade = true)
        if @role.save
          response = {"STATUS" => "SS", "MESSAGE" => "Permission was successfully removed."}
          render :json => response
          #redirect_to @role, notice: 'resource was successfully removed.'
        else
          response = {"STATUS" => "FF", "MESSAGE" => "Unable to remove Permission"}
          render :json => response
          #render action: "edit_permission"
        end
      else
        response = {"STATUS" => "FF", "MESSAGE" => "You don't have this permission"}
        render :json => response
        #redirect_to @role, notice: 'you don\'t have it .'
      end
    else
      response = {"STATUS" => "FF", "MESSAGE" => "The role you are trying to edit is not assigned to you"}
      render :json => response
      #@errors = "You can't edit this role as it is not assigned to you"
      #render action: edit_permission
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      employee = User.find(loggedin_employee)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:name, :role_type, :parent_id)
    end
end
