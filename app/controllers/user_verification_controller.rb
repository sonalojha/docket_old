class UserVerificationController < ApplicationController
	layout 'employee'

	def index
  		profile_ids = Users::Profile.where('users_profiles.profile_verified = ?',false).where("users_profiles.id_proof_url IS NOT NULL").pluck(:id)
  		@profile = Users::Profile.includes(:user).find(profile_ids.sample)
	end

	def verify_profile
		if params[:profile]
			profile = Users::Profile.find(params[:profile])
			profile.profile_verified = true
			profile.save!
			redirect_to user_verification_path, notice: 'Profile Verified' 
		else
			redirect_to user_verification_path, :alert => 'Profile Not found'
		end
	end

	def details_incorrect
		if params[:profile]
			profile = Users::Profile.find(params[:profile])
			
			##### SEND email to user to update profile ids
			redirect_to user_verification_path, notice: 'Email Sent to update User' 
		else
			redirect_to user_verification_path, :alert => 'Server error'
		end
	end
end
