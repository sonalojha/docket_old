class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :user_orders]
  layout 'employee'

  def check_permission
    return true
  end
  # GET /users
  # GET /users.json
  def index
    @users = User.all.paginate(:page => params[:page], :per_page => 30).order('id')
    respond_to do |format|  
       format.html
       format.xls do
        send_data(
          User.all.select('id,name, email, phone').to_xls,
          content_type: 'application/vnd.ms-excel',
          filename: 'users.xls'
        )
      end
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end 

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def search
     
  end

  def search_by 
   if params[:type] == 'name'
      @users = User.where("lower(name) like ?", "%#{params[:query].to_s.downcase}%").order('id')
    elsif params[:type] == 'email' 
      @users = User.where(email: params[:query]).order('id')
    elsif params[:type] == 'phone'
      @users = User.where(phone: params[:query]).order('id')
    end
    render 'users', layout: false
  end

def user_orders
    @orders = Order.where(user_id: @user.id).where(status_id: Orders::Status.where(name: ['INPROGRESS', 'COMPLETED']).pluck(:id))
    @order_statuses = Orders::Status.all
    @delivery_statuses = Orders::DeliveryStatus.all
    render 'user_orders'  
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :city_id, :profile_type, :profile_id, :status_id, :parent_id, :phone, :dvault_activated)
    end
end
