class PredictionsController < ApplicationController
	def check_permission
		return false
	end
	def document_search
		if params[:service_id]
			@documents = Document.predictions(params[:q]).where(service_id: params[:service_id]).limit(10).select(:name, :id, :default_price, :tat, :service_id)
								.map{|l| {id: l.id, name: l.name, price: l.default_price, tat: l.tat || '', service: l.service_id}}
		else
			@documents = Document.predictions(params[:q]).limit(10).select(:name, :id, :default_price, :tat, :service_id)
								.map{|l| {id: l.id, name: l.name, price: l.default_price, tat: l.tat || '', service: l.service_id}}
		end
		render json: @documents
	end

	def search_location
		@localities = Locality.includes(city: [state: [:country]]).predictions(params[:q]).limit(10).select(:name, :id).map{|l| {id: l.id, name: l.name}}
		render json: @localities
	end

	def search_city
		@cities = ::City.includes(:state => [:country]).predictions(params[:q]).limit(10).map{|l| {id: l.id, name: l.name}}
		render json: @localities
	end

	def search_user
		@users = ::User.predictions(params[:q]).limit(10).select(:name, :id, :email).map{|l| {"id" => l.id, "name"=> "#{l.name}, #{l.email}"}}
		render json: @users
	end

	def search_files
		@files = ::Dvaults::UserFile.predictions(params[:dvault_id],params[:q]).limit(10).select(:filename, :tag, :mime_type, :sub_category_id, :id)
					.map{|f|{id: f.id, file_name: f.filename, tag: f.tag, mime_type: f.mime_type, sub_category_id: f.sub_category_id}}
		render json: @files
	end
end
