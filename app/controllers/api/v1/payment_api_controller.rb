module Api
module V1
	class PaymentApiController < ApiController
	  
		def check_permission
			return false
		end

		def payu_callback
			response = Hash.new
			result = Hash.new
			if Order.is_valid_transaction?(params[:txnid]) #&& params[:status] == 'success'
				payment = Payment.new(mihpayid: params[:mihpayid],mode: params[:mode], amount: params[:amount],
				                secret_hash: params[:hash], pg_type: params[:PG_TYPE],bank_ref_num: params[:bank_ref_num],
				                bankcode: params[:bankcode], name_on_card: params[:name_on_card], cardnum: params[:cardnum],
				                payuMoneyId: params[:payuMoneyId], discount: params[:discount], payu_status: params[:status],
				                )
				payment.status_id = Payments::Status.where(name: 'PAYMENT RECEIVED').first.id
				order = Order.where(txnid: params[:txnid]).first
				payment.order_id = order.id
				# order.delivery_info.new(firstname: params[:firstname], email: params[:email], phone: params[:phone])
			    if payment.save!
			        Order.update_status(params[:txnid])
					response.merge! ApiStatusList::OK
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def paytm_hash
			response = Hash.new
			result = Hash.new
			# debugger
			if params[:ORDER_ID] && !params[:ORDER_ID].blank?
				order_id = params[:ORDER_ID]
				# paytm_hash = Payment.generate_paytm_hash order_id,'wap'
				paytmHASH = Hash.new
				params.keys.each do |k|
					paytmHASH[k] = params[k]
				end
				paytmHASH.delete('action')
				paytmHASH.delete('controller')
				paytmHASH.delete('key')
				checksumhash = Paytm::ChecksumTool.new.get_checksum_hash(paytmHASH).gsub("\n",'')
				if checksumhash
					response['CHANNEL_ID'] = params['CHANNEL_ID']
				    response['CUST_ID'] = params['CUST_ID']
    				response['EMAIL'] = params['EMAIL']
    				response['INDUSTRY_TYPE_ID'] = params['INDUSTRY_TYPE_ID']
    				response['MID'] = params['MID']
    				response['MOBILE_NO'] = params['MOBILE_NO']
				    response['REQUEST_TYPE'] = params['REQUEST_TYPE']
    				response['TXN_AMOUNT'] = params['TXN_AMOUNT']
    				response['WEBSITE'] = params['WEBSITE']
					response['CHECKSUMHASH'] = checksumhash
					response['ORDER_ID'] = params[:ORDER_ID]
					response['payt_STATUS'] = 1
					# response.merge! ApiStatusList::OK
				else
					response['payt_STATUS'] = 2
				end
				status = 200
			else
				response['payt_STATUS'] = 2
				# response.merge! ApiStatusList::INVALID_REQUEST
				# status = 400			
			end
			puts response
			render :json => response, status: status
		end

		def paytm_verify
			response = Hash.new
			result = Hash.new
			# order_id = params[:order_id]

			paytm_hash = Payment.generate_paytm_hash_verify
			if paytm_hash
				response['result'] = paytm_hash
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end
	end
end
end