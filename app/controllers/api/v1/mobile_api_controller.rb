module Api
module V1
	class MobileApiController < ApiController
	  
		def check_permission
			return false
		end

		######## Register a Device ########
		def register
			response = Hash.new
			result = Hash.new
			if params[:device_id]
				device = DeviceRegistration.find_or_initialize_by(device_id: params[:device_id].to_s.strip)
				device.email = params[:email].to_s.strip
				device.registration_key = params[:registration_key].to_s.strip
				device.user_id = params[:user_id] if params[:user_id]
				device.platform_id = ::Platform.where(name: (params[:platform].capitalize || 'Android') ).first.id
				if device.save!
					result['device'] = {reg_id: device.id}
					result['country_codes'] = Country.all.select([:id,:name,:url_name,:phone_code]).as_json
					result['states'] = State.all.select([:id,:name,:country_id]).as_json
					result['cities'] = City.all.select([:id,:name, :state_id]).as_json
					result['localities'] = Locality.all.select([:id,:name,:city_id]).as_json
					result['ios_version'] = App.where(name: 'ios_version').first.value
					result['android_version'] = App.where(name: 'android_version').first.value
					response['result'] = result
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		######## User Signup ########
		def login
			response = Hash.new
			result = Hash.new
			# if params[:mobile]
				# users = User.where(phone: params[:mobile], profile_type: "Users::Profile")
			# else
			users = User.where(email: params[:email], profile_type: "Users::Profile")
			# end
			if users && (users.length == 1)
				verification = Verification.where(user_id: users.first.id, code: params[:otp], verification_type: 'Login').first
				if verification
					user = users.first
					result = get_user_details user.id
					response['result'] = result
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INCORRECT_OTP
				end
			else
				response.merge! ApiStatusList::USER_NOT_FOUND
			end
			render :json => response
		end

		####### verify mobile #####
		def login_otp
			response = Hash.new
			result = Hash.new
			# if params[:mobile]
				# user = User.where(phone: params[:mobile])
			# else
				user = User.where(email: params[:email])
			# end
			if user && (user.length == 1)
				verify = user.first.verifications.create(verification_type: 'Login')
				verify.save!
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::USER_NOT_FOUND
			end
			result['ios_version'] = App.where(name: 'ios_version').first.value
			result['android_version'] = App.where(name: 'android_version').first.value
			response['result'] = result
			render :json => response
		end

		def get_user_details id
			user = User.includes(:profile).where(id: id, profile_type: 'Users::Profile').first
			result = {id: user.id, first_name: user.profile.first_name, last_name: user.profile.last_name,
				email: user.email, mobile: user.phone || '', city_id: user.city_id || '',
				mobile_verified: (user.profile ? user.profile.mobile_verified : false),
				dvault_active: user.dvault_activated, profile_verified: user.profile.profile_verified,
				country_id: user.profile.country_id, profile_img: user.profile.profile_img || '',
				gender: user.profile.gender || '', birthday: user.profile.birthday ? user.profile.birthday.strftime('%d/%m/%Y') : '',
				id_proof_url: user.profile.id_proof_url || '', address_proof_url: user.profile.address_proof_url || '',
				email_verified: user.profile.email_verified
			}
		end

		def signup
			response = Hash.new
			result = Hash.new
			# if params[:email] 
			# 	user = User.where(email: params[:email], profile_type: 'Users::Profile')
			# else	
			# 	user = User.where(phone: params[:mobile], profile_type: 'Users::Profile')
			# end
			if User.check_valid(params[:email], params[:mobile]) #(user.length == 0)
				user = User.new(name: params[:name], email: params[:email], phone: params[:mobile])
				user.profile_type = 'Users::Profile'
				user.name = params[:name]
				profile = Users::Profile.new(first_name: params[:name].split(' ').first, last_name: params[:name].split(' ').last)
				if params[:phone_code]
					country = Country.where(phone_code: params[:phone_code].strip)
					profile.country_id = country.first.id if country.length > 0
				end
				profile.save!
				user.profile_id = profile.id
				user.save!
			    if params[:device_id] && !params[:device_id].empty?
					device = DeviceRegistration.where(device_id: params[:device_id]).first
				    if device
				    	device.user_id = user.id
				    	device.save!
				    end
				end
				response['result'] = get_user_details user.id
				response['result']['ios_version'] = App.where(name: 'ios_version').first.value
				response['result']['android_version'] = App.where(name: 'android_version').first.value
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::USER_ALREADY_EXIST	
			end	
			render :json => response
			# if !params[:user].nil?
			# 	user_data = JSON.parse(params[:user])
			# 	user_data['email'] = user_data['email']
			# 	users = User.where(phone: user_data[:mobile], profile_type: 'Users::Profile')
			# 	if user_data && !user_data['email'].empty? 
			# 		if User.where(email: user_data['email']).empty? && users && (users.length == 0)
			# 			user = User.new(email: user_data['email'])
			# 			user.name =  user_data['name'] || (user_data['first_name'] + ' ' + user_data['last_name'])
			# 			user.password = user_data['password'] if user_data['password']
			# 			user.phone = user_data['mobile'] if user_data['mobile']
			# 			user.profile_type = "Users::Profile"
			# 			user.save!
			# 			profile = Users::Profile.find(user.profile_id) if user.profile_id
			# 		    profile = profile || Users::Profile.new      
			# 		    if profile
			# 		        profile.first_name = user_data['first_name'] if user_data['first_name']
			# 		        profile.middle_name = user_data['middle_name'] if user_data['middle_name']
			# 		        profile.last_name = user_data['last_name'] if user_data['last_name']
			# 		        profile.gender = user_data['gender'] if user_data['gender']
			# 		        profile.birthday = Time.parse(user_data['birthday']) if !user_data['birthday'].nil?
			# 		        profile.profile_img = user_data['image'] if user_data['image']
			# 		        profile.cover_img = user_data['link'] if user_data['link']
			# 		        profile.country_id = user_data['country_id'] if user_data['country_id']
			# 				profile.current_sign_in_at = Time.zone.now
			# 				profile.current_sign_in_ip = request.ip
			# 		        profile.sign_in_count += 1
			# 		        profile.save!
			# 		    end
			# 		    user.profile_id = profile.id
			# 			user.save!
			# 		    if params[:device_id] && !params[:device_id].empty?
			# 				device = DeviceRegistration.where(device_id: params[:device_id]).first
			# 			    if device
			# 			    	device.user_id = user.id
			# 			    	device.save!
			# 			    end
			# 		    end
			# 			login_otp user.id
			# 			response['result'] = get_user_details user.id
			# 			response.merge! ApiStatusList::OK
			# 		else
			# 			response.merge! ApiStatusList::USER_ALREADY_EXIST	
			# 		end
			# 	else
			# 		response.merge! ApiStatusList::INVALID_REQUEST
			# 	end
			# else
			# 	response.merge! ApiStatusList::INVALID_REQUEST
			# end
			# render :json => response
		end

		##### Send Notifications ####
		def send_notifications
			response = Hash.new
			fcm = ::FCM.new(ENV['fcm_key'])
			if params[:registration_id] && params[:message]
				registration_id = [params[:registration_id]]
				options = {
					'notification' => {
						# 'message' => params[:message]
						'title' => params[:title],
						'text' => params[:text] #,
						# 'image' => params[:image_url] || ''
				  	},
					'collapse_key' => 'updated_state'
				}
				send = fcm.send_notification(registration_id, options)
				if send
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render json: response
		end

		def profile_data
			response = Hash.new
			if !params[:user_id].nil? && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				if user && user.profile
					response['result'] = get_user_details params[:user_id]
					response.merge! ApiStatusList::OK	
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def update_user
			response = Hash.new
			result = Hash.new
			if !params[:user_id].nil? && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				if user && user.profile
					user.profile.first_name = params[:first_name] if (params[:first_name] && !params[:first_name].empty?)
					user.profile.last_name = params[:last_name] if (params[:last_name] && !params[:last_name].empty?)
					# user.profile.middle_name = params[:middle_name] if !params[:middle_name].empty?
					user.profile.profile_img = params[:profile_img] if (params[:profile_img] && !params[:profile_img].empty?)
					user.profile.gender = params[:gender] if (params[:gender] && !params[:gender].empty?)
					user.profile.birthday = Time.parse(params[:birthday]) if (params[:birthday] && !params[:birthday].empty?)
					if params[:mobile] && !params[:birthday].empty? && (User.where(phone: params[:mobile]).count == 1)
						user.phone = params[:mobile]
					end
					user.profile.save!
					user.save!

					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		######## Service Selection ########

		def search
			response = Hash.new
			result = Hash.new
			if params[:service_id]
				result = Document.predictions(params[:query].strip).where(service_id: params[:service_id]).map{|c|{name: c.name, id: c.id, service_id: c.service_id, default_price: c.default_price || '99'}}
				response.merge! ApiStatusList::OK
			else
				result = Document.predictions(params[:query].strip).map{|c|{name: c.name, id: c.id, service_id: c.service_id, default_price: c.default_price || '99'}}
				response.merge! ApiStatusList::OK
			end
			response['result'] = result
			render :json => response
		end

		def service_all
			response = Hash.new
			result = Hash.new
			ondemand = ['Notary', 'E-stamp', 'Attestation', 'Franking']
			result['services'] = Service.all.where('description is not null').select([:id,:name,:priority, :min_stamp, :description, :delivery_charges,
											 :print_price, :notary_price, :registration_price]).as_json
			result["ondemand"] = Document.where(name: ondemand).select([:name, :id, :default_price, :tat, :service_id]).as_json
			result['estamp_service_charge'] = {
				service_charge: 25,
				service_charge_percent: 2.5
			}
			result['ios_version'] = App.where(name: 'ios_version').first.value
			result['android_version'] = App.where(name: 'android_version').first.value
			response['result'] = result
			response.merge! ApiStatusList::OK
			render :json => response
		end
		
		######## User Address ########
		def add_address
			response = Hash.new
			result = Hash.new
			if !params[:tag].empty? && !params[:user_id].empty?
				if !params[:country].empty?
					country = Country.find_or_initialize_by(name: params[:country].capitalize)
					country.save!
			  	end
			  	if !params[:state].empty?
					state = State.find_or_initialize_by(name: params[:state].capitalize)
					state.country_id = country.id
					state.save!
			  	end
			  	if !params[:city].empty?
					city = City.find_or_initialize_by(name: params[:city].capitalize)
					city.state_id = state.id
					city.save!
			  	end
				if !params[:locality].empty?
					locality = Locality.find_or_initialize_by(name: params[:locality].capitalize, city_id: city.id)
					locality.save!
				end
				address = Address.find_or_initialize_by(tag: params[:tag], user_id: params[:user_id])
				address.pincode = params[:pincode] if params[:pincode]
				address.address1 = params[:address1] if params[:address1]
				address.address2 = params[:address2] if params[:address2]
				address.city_id = city.id if !city.nil?
				address.locality_id = locality.id if !locality.nil?
				address.landmark = params[:landmark] if params[:landmark]
				address.save!
				result = {id: address.id, tag: address.tag, address1: address.address1, address2: address.address2, 
						locality: (address.locality ? address.locality.name : ''), pincode: address.pincode, landmark: address.landmark, city: (address.city ? address.city.name : ''),
						state: (address.city.state_id ? address.city.state.name : ''), country: (address.city.state_id ? address.city.state.country.name : '')
						}
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::NOT_FOUND
			end
			response['result'] = result
			render :json => response
		end

		def update_address
			response = Hash.new
			result = Hash.new
			if !params[:tag].empty? && !params[:user_id].empty? && params[:id]
				if !params[:country].empty?
					country = Country.find_or_initialize_by(name: params[:country].capitalize)
					country.save!
			  	end
			  	if !params[:state].empty?
					state = State.find_or_initialize_by(name: params[:state].capitalize)
					state.country_id = country.id
					state.save!
			  	end
			  	if params[:city] && !params[:city].empty?
					city = City.find_or_initialize_by(name: params[:city].capitalize)
					city.save!
				end
				if !params[:locality].empty?
					locality = Locality.find_or_initialize_by(name: params[:locality].capitalize, city_id: city.id)
					locality.save!
				end
				address = Address.where(id: params[:id],user_id: params[:user_id]).first
				if address
					address.tag = params[:tag] if params[:tag]
					address.pincode = params[:pincode] if params[:pincode]
					address.address1 = params[:address1] if params[:address1]
					address.address2 = params[:address2] if params[:address2]
					address.city_id = city.id if !city.nil?
					address.locality_id = locality.id if !locality.nil?
					address.landmark = params[:landmark] if params[:landmark]
					address.save!
					result = {id: address.id, tag: address.tag, address1: address.address1, address2: address.address2, 
						locality: (address.locality ? address.locality.name : ''), pincode: address.pincode, landmark: address.landmark, city: (address.city ? address.city.name : ''),
						state: (address.city.state_id ? address.city.state.name : ''), country: (address.city.state_id ? address.city.state.country.name : '')
						}
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::NOT_FOUND
			end
			response['result'] = result
			render :json => response
		end

		def delete_address
			response = Hash.new
			if params[:user_id] && params[:address_id]
			  	@address = Address.where(id: params[:address_id], user_id: params[:user_id])
				if @address && (@address.length > 0)
					@address = @address.destroy_all
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::UNKNOWN_ERROR
			end
			render :json => response
		end

		def get_address
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id])
				result = Address.includes(:city, :locality).where(user_id: params[:user_id]).map{|ad| {id: ad.id, tag: ad.tag, address1: ad.address1, address2: ad.address2, 
					locality: (ad.locality ? ad.locality.name : ''), pincode: ad.pincode, landmark: ad.landmark, city: (ad.city ? ad.city.name : ''),
					state: (ad.city.state_id ? ad.city.state.name : ''), country: (ad.city.state_id ? ad.city.state.country.name : '')
					}}
				response.merge! ApiStatusList::OK
			elsif params[:email] && (User.where(email: params[:email]).length > 0)
				result = Address.includes(:city, :locality).where(user_id: User.where(email: params[:email]).first.id).map{|ad| {id: ad.id, tag: ad.tag, address1: ad.address1, address2: ad.address2, 
					locality: (ad.locality ? ad.locality.name : ''), pincode: ad.pincode, landmark: ad.landmark, city: (ad.city ? ad.city.name : ''),
					state: (ad.city.state_id ? ad.city.state.name : ''), country: (ad.city.state_id ? ad.city.state.country.name : '')
					}}
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::NOT_FOUND
			end
			response['result'] = result
			render :json => response
		end
		
		######## Mobile Register ########
		def mobile_exists
			response = Hash.new
			result = Hash.new
			if params[:user_id] && params[:mobile] && User.exists?(params[:user_id])
				users = User.where(phone: params[:mobile], profile_type: 'Users::Profile')
				if users && (users.length == 0)
					response.merge! ApiStatusList::OK
				elsif (users.pluck(:id) == params[:user_id])
					response.merge! ApiStatusList::ALREADY_REGISTERED
				else
					response.merge! ApiStatusList::ALREADY_EXIST
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def add_mobile
			response = Hash.new
			result = Hash.new
			if params[:user_id] && params[:mobile] && User.exists?(params[:user_id]) 
				users = User.where(phone: params[:mobile], profile_type: 'Users::Profile')
				if (users.length == 0)
					user = User.find(params[:user_id])
					user.phone = params[:mobile]
					user.profile.mobile_verified = true
					# verify = user.verifications.new
					# verify.save!
					# SmsJob.perform_later(user.id,params[:mobile],verify.otp,'sms_verify',nil)
					user.save!
					user.profile.save!
					response.merge! ApiStatusList::OK
					# if send_msg(user.id, mobile)
					# else
						# response.merge! ApiStatusList::UNKNOWN_ERROR
					# end
				else
					response.merge! ApiStatusList::ALREADY_EXIST
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def send_msg id,mobile
			verify = User.find(id).verifications.new
			verify.save!
			msg = "OTP for Notary Mama is #{verify.otp}. Please do not share it with anyone."
			send_sms = User.send_msg(id,'send_otp', verify.otp, msg, mobile)
		end

		def verify_mobile
			response = Hash.new
			if params[:user_id] && User.exists?(params[:user_id])
				verify = User.find(params[:user_id]).verifications.create(verification_type: 'Otp')
				verify.save!
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INCORRECT_PASSWORD
			end
			render json: response
		end

		def verify_otp
			response = Hash.new
			if params[:user_id] && User.exists?(params[:user_id]) && params[:otp]
				user = User.where(id: params[:user_id]).first
				if user
					verify = Verification.where(code: params[:otp], verification_type: 'Otp', user_id: params[:user_id], status_id: Status.where(name: 'ACTIVE').first.id)
					if verify
						user.profile.mobile_verified = true
						user.profile.save!
						verify.first.status_id = Status.where(name: 'INACTIVE').first.id
						verify.first.save!
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::NOT_FOUND
					end
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render json: response
		end

		def forgot_password
			response = Hash.new
			result = Hash.new
			if params[:email]
				user = User.where(email: params[:email],profile_type: 'Users::Profile')
				if user.length == 1
					verification = user.first.verifications.create(verification_type: 'Reset')
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::USER_NOT_FOUND
				end
			elsif params[:mobile]
				user = User.where(phone: params[:mobile],profile_type: 'Users::Profile')
				if user.length == 1
					verification = user.first.verifications.create(verification_type: 'Reset')
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::USER_NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def change_password
			response = Hash.new
			result = Hash.new
			if params[:user_id] && params[:old_password] && params[:new_password] && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				if (User.authenticate(user.email,params[:old_password])) && (params[:new_password] != params[:old_password]) 
					user.password = params[:new_password] 
					user.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INCORRECT_PASSWORD
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def change_phone
			response = Hash.new
			result = Hash.new
			if params[:user_id] && params[:old_mobile] && params[:new_mobile] && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				users = User.where(phone: params[:new_mobile], profile_type: 'Users::Profile')
				if users && (users.length == 0)
					if (params[:new_mobile] != params[:old_mobile]) 
						user.phone = params[:new_mobile]
						user.profile.country_id = params[:country_id].to_i
						user.profile.save!
						user.save!
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::INVALID_REQUEST
					end
				else
					response.merge! ApiStatusList::ALREADY_EXIST
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def request_call
			response = Hash.new
			result = Hash.new
			if params[:email] && params[:mobile] 
				contact = ContactRequest.new(
						name: params[:name], phone: params[:mobile], message: params[:message], request_type: params[:type], 
						status_id: Status.where(name: 'NEW').first.id, email: params[:email])
				contact.save!
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def country_code
			response = Hash.new
			result = Hash.new
			result['country_codes'] = Country.all.select([:id,:name,:url_name,:phone_code]).as_json
			result['states'] = State.all.select([:id,:name,:country_id]).as_json
			result['cities'] = City.all.select([:id,:name, :state_id]).as_json
			result['localities'] = Locality.all.select([:id,:name,:city_id]).as_json
			response.merge! ApiStatusList::OK
			response['result'] = result
			render :json => response
		end

		def upload_id_proof
			response = Hash.new
			result = Hash.new
			if params[:user_id] && (User.exists? params[:user_id]) && (params[:address_proof] || params[:id_proof])
				user = User.find(params[:user_id])
				if user 
					user.profile.address_proof_url = params[:address_proof] if params[:address_proof]
					user.profile.id_proof_url = params[:id_proof] if params[:id_proof]
					user.profile.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::USER_NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def confirm_draft
			response = Hash.new
			result = Hash.new
			if params[:order_id] && params[:user_id] && params[:confirm]
				order = Order.find(params[:order_id])
				if order 
					if params[:confirm].downcase == 'yes'
						order.draft_confirmed = true
					elsif params[:confirm].downcase == 'no'
						order.correction = params[:corrections]
					end
					order.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def verify_email
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				if user
					user.verifications.create(verification_type: 'Activate')
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INVALID_REQUEST
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def verification_code
			response = Hash.new
			result = Hash.new
			if params[:hash]
				verification = Verification.where(code: params[:hash], verification_type: 'Reset',status_id: Status.where(name: 'ACTIVE').first.id)
				if verification.length > 0
					@verification = verification.first
					@user = verification.first.user
				end
			else
				if params[:user_id] && params[:reset_code]
					user = User.find(params[:user_id])
					verification = Verification.where(user_id: params[:user_id], code: params[:reset_code],status_id: Status.where(name: 'ACTIVE').first.id).first
					if verification && user && (params[:password] == params[:confirm_password]) 
						user.password = params[:password] 
						user.save!
						verification.status_id = Status.where(name: 'INACTIVE').first.id
						verification.save!
					end
				else
					@verification = Verification.where(code: params[:hash], verification_type: 'Activation',status_id: Status.where(name: 'ACTIVE').first.id).first
					if @verification
						@verification.user.profile.email_verified = true
						@verification.user.profile.save!
						@verification.status_id = Status.where(name: 'INACTIVE').first.id
						@verification.save!
						UserSignupJob.perform_later(@verification.user_id,'activated',nil)
					end
				end
			end
		end

		# def login_otp user_id
		# 	user = User.find(params[:user_id])
		# 	if user
		# 		user.verifications.create(verification_type: 'Login')
		# 	end
		# end
	end
	private
	def user_params
		params.permit(:email, :password, :mobile)
	end
end
end