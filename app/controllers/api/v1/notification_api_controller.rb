module Api
module V1
	class NotificationApiController < ApiController
		def check_permission
			return false
		end

		def notifications
			response = Hash.new
			result = Hash.new
			if params[:user_id]	
				user = User.find params[:user_id]	
				if user
					result['notification'] = Notification.get_user_notifications(params[:user_id]).as_json
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def read_notification
			response = Hash.new
			result = Hash.new
			if params[:user_id]	&& params[:notification_id]
				user = User.find params[:user_id]	
				notification = Notification.where(id: params[:notification_id], user_id: params[:user_id]).first
				if user && notification
					notification.read = true
					notification.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end
	end	
end
end