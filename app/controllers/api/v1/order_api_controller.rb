module Api
module V1
	class OrderApiController < ApiController
	  
		def check_permission
			return false
		end

		def create_order
			response = Hash.new
			result = Hash.new
			if params[:service_id] && params[:answers] && !params[:total_amount].nil?
				order = Order.new(service_id: params[:service_id], document_id: params[:document_id])
				order.total_amount = params[:total_amount] if params[:total_amount]
				order.document_id = params[:document_id] if params[:document_id]
				order.stamp_amount = params[:stamp_amount] if params[:stamp_amount]
				order.delivery_amount = params[:delivery_amount] if params[:delivery_amount]
				order.pickup_amount = params[:pickup_amount] if params[:pickup_amount]
				order.user_id = params[:user_id]
				order.attachments.new(name: params[:attachment_name], s3_url: params[:attachment_s3_url], tag: 'User Format', mime_type: params[:mime_type]) if params[:attachment_name]
				if params[:platform]
					order.platform_id = ::Platform.where(name: params[:platform].capitalize).first.id
				end
				order.product = params[:product]
				order.summary = params[:summary] if params[:summary]

				if params[:coupon] && !params[:coupon].blank?
					coupon = Coupon.validate_coupon(params[:coupon])
					if coupon
						discount = Coupon.get_discount_amount(coupon,nil,params[:net_amount])
						if discount && !(params[:net_amount].to_i > params[:total_amount].to_i) 
							order.total_amount = params[:net_amount].to_i
							order.discount = nil
							order.coupon_id = nil
							# order.discount = params[:discount] #for this to work alter in invoice also
						else
							order.discount = nil
							order.coupon_id = coupon
						end
					else
						order.discount = nil
						order.coupon_id = nil
					end
				end
				order.gst = ENV['gst']
				order.gst_amount = (ENV['gst'].to_f/100) * order.total_amount.to_f
				order.final_amount = order.total_amount.to_f + order.gst_amount.to_f

				if order.save!
					JSON.parse(params[:answers]).each do |ques,ans|
						answer = order.answers.create(body: ans, question_id: ques)
						answer.save!
					end
					detail = Orders::Detail.find_or_initialize_by(order_id: order.id)
					detail.name = params[:name]
					detail.email = params[:email]
					detail.mobile = params[:mobile]
					detail.save!
					if !order.user
						user = User.check_user_status detail
						order.user_id = user.id
						order.save!
					end
					# UserSignupJob.perform_later(params[:user_id],'verify_account') if !User.find(params[:user_id]).profile.profile_verified
					# OrderJob.perform_later(order.id,'order_placed',params[:user_id])
        			# SmsJob.perform_later(params[:user_id], nil, nil, 'order_placed', order.id)
				end
				if params[:payment_type] && (params[:payment_type] == 'Paytm')
	            	result['paytm_hash'] = paytm_has_generate(order.id)
	            end
	            device = DeviceRegistration.where(device_id: params[:device_id].to_s.strip)
	            if device.length > 0
	            	device.first.user_id = order.user_id if device.first.user_id.nil?
	            	device.first.save!
	            end
				result['order_id'] = order.id
				result['order_txnid'] = order.txnid
				result['user_id'] = order.user_id
				result['gst'] = order.gst
				result['gst_amount'] = order.gst_amount.to_f.round(2)
				result['discount'] = order.discount
				result['final_amount'] = order.final_amount.to_f.round(2)
				response['result'] = result
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			puts response
			render :json => response
		end

		def paytm_has_generate order_id
			options = Payment.generate_paytm_hash_new(order_id)
			return options
		end

		def update_order
			response = Hash.new
			result = Hash.new
			if params[:service_id] && params[:answers] && !params[:total_amount].nil?
				order = Order.where(service_id: params[:service_id], document_id: params[:document_id], txnid: params[:txnid]).first
				order.total_amount = params[:total_amount] if params[:total_amount]
				order.document_id = params[:document_id] if params[:document_id]
				order.stamp_amount = params[:stamp_amount] if params[:stamp_amount]
				order.delivery_amount = params[:delivery_amount] if params[:delivery_amount]
				order.pickup_amount = params[:pickup_amount] if params[:pickup_amount]
				order.user_id = params[:user_id] if params[:user_id]
				order.attachments.new(name: params[:attachment_name], s3_url: params[:attachment_s3_url], tag: 'User Format', mime_type: params[:mime_type]) if params[:attachment_name]
				if params[:platform]
					order.platform_id = ::Platform.where(name: params[:platform].capitalize).first.id
				end
				order.product = params[:product]
				order.summary = params[:summary] if params[:summary]

				if params[:coupon] && !params[:coupon].blank?
					coupon = Coupon.validate_coupon(params[:coupon])
					if coupon
						discount = Coupon.get_discount_amount(coupon,nil,params[:net_amount])
						if discount && !(params[:net_amount].to_i > params[:total_amount].to_i) 
							order.total_amount = params[:net_amount].to_i
							order.discount = nil
							order.coupon_id = nil
							# order.discount = params[:discount] #for this to work alter in invoice also
						else
							order.discount = nil
							order.coupon_id = coupon
						end
					else
						order.discount = nil
						order.coupon_id = nil
					end
				end
				order.gst = ENV['gst']
				order.gst_amount = (ENV['gst'].to_f/100) * order.total_amount.to_f
				order.final_amount = order.total_amount.to_f + order.gst_amount.to_f

				if order.save!
					JSON.parse(params[:answers]).each do |ques,ans|
						answer = order.answers.find_or_initialize_by(question_id: ques)
						answer.body = ans
						answer.save!
					end
					detail = Orders::Detail.find_or_initialize_by(order_id: order.id)
					detail.name = params[:name]
					detail.email = params[:email]
					detail.mobile = params[:mobile]
					detail.save!
					# UserSignupJob.perform_later(params[:user_id],'verify_account') if !User.find(params[:user_id]).profile.profile_verified
					# OrderJob.perform_later(order.id,'order_placed',params[:user_id])
        			# SmsJob.perform_later(params[:user_id], nil, nil, 'order_placed', order.id)
				end
				if params[:payment_type] && (params[:payment_type] == 'Paytm')
	            	result['paytm_hash'] = paytm_has_generate(order.id)
	            end
				result['order_id'] = order.id
				result['order_txnid'] = order.txnid
				result['user_id'] = order.user_id
				result['gst'] = order.gst
				result['gst_amount'] = order.gst_amount.to_f.round(2)
				result['discount'] = order.discount
				result['final_amount'] = order.final_amount.to_f.round(2)
				response['result'] = result
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def update_order_address
			response = Hash.new
			result = Hash.new
			@order = Order.where(txnid: params[:txnid]).first
			# if !@order
			# 	@order = Order.where(id: params[:order_id]).first
			# end
			if @order
				if params[:address_id] && !params[:address_id].empty?
					@order.address_id = params[:address_id]
					response.merge! ApiStatusList::OK
				else	
					if !params[:country].empty?
						country = Country.find_or_initialize_by(name: params[:country].capitalize)
						country.save!
				  	end
				  	if !params[:state].empty?
						state = State.find_or_initialize_by(name: params[:state].capitalize)
						state.country_id = country.id
						state.save!
				  	end
				  	if !params[:city].empty?
						city = City.find_or_initialize_by(name: params[:city].capitalize)
						city.state_id = state.id
						city.save!
				  	end
				  	if !params[:locality].empty?
						locality = Locality.find_or_initialize_by(name: params[:locality].capitalize, city_id: city.id)
						locality.save!
				  	end
				  	address = Address.find_or_initialize_by(tag: params[:tag], order_id: @order.id)
				  	address.user_id = @order.user_id || params[:user_id] 
				  	address.pincode = params[:pincode] if params[:pincode]
				  	address.address1 = params[:address1] if params[:address1]
				  	address.address2 = params[:address2] if params[:address2]
				  	address.city_id = city.id if !city.nil?
				  	address.locality_id = locality.id if !locality.nil?
				  	address.landmark = params[:landmark] if params[:landmark]
				  	address.save!
			  		@order.address_id = address.id
				end
		  		@order.save!
		  		OrderJob.perform_later(@order.id,'order_thankyou')
			  	OrderJob.perform_later(@order.id,'order_invoice')
        		SmsJob.perform_later(@order.user_id,nil,'order_invoice',@order.id)
			  	response.merge! ApiStatusList::OK
			  	if (['Agreement', 'Affidavit', 'Deed', 'Bond'].include? @order.service.name) && (@order.document.automated == true)
					result['automation'] = true
				else
					result['automation'] = false
				end
				result['user_id'] = @order.user_id
				response['result'] = result
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def delete_order
			response = Hash.new
			result = Hash.new
			order = Order.where(txnid: params[:txnid], user_id: params[:user_id]).first
			if order
			    order.status_id = Orders::Status.where(name: 'DELETED').first.id
			    if order.save!
			    	response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def order_list
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id])
				orders = Order.includes(:service, :address, :delivery_status, :delivery_partner, :status, :automated_answers, payment: [:status]).where(user_id: params[:user_id]).order('id DESC')
				result['Incomplete'] = orders.where(status_id: 1).map{|order| {
						id: order.id, 
						date: order.created_at.strftime('%d/%m/%Y'), 
						service_name: order.service.name || '', 
						document_id: order.document_id || '', 
						order_status: order.status.name || 'INCOMPLETE',
						txnid: order.txnid, 
						payment_status: order.payment ? order.payment.status.name : 'INCOMPLETE', 
						deletable: true,
						product: order.product || '', 
						mode: order.payment ? order.payment.mode : 'N/A', 
						amount: order.total_amount,
						txn_date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : 'N/A', 
						address: order.address ? {address1: order.address.address1, address2: order.address.address2,tag: order.address.tag} : '' ,
						pickup_amount: order.pickup_amount || '', 
						soft_copy_url: order.soft_copy_url || '',
						delivery_status: order.delivery_status ? order.delivery_status.name : '',
						delivery_partner: order.delivery_partner ? order.delivery_partner.name : '' ,
						delivery_tracking_no: order.delivery_tracking_no || '',
						receipt_url: order.invoice_url || '',
						draft_confirmed: order.draft_confirmed,
						draft_required: order.draft_required,
						draft_url: order.draft_url || '',
						discount: order.discount || '',
						coupon: order.coupon ? order.coupon.code : '',
						stamp_amount: order.stamp_amount || '',
						delivery_amount: order.delivery_amount || '',
						answers: order.answers.map{|answer| {id: answer.id, body: answer.body, question_id: answer.question_id, question: answer.question.name}},
						automation_done: (order.automated_answers.length > 0) ? true : false,
						document_price: order.document_id ? order.document.default_price : '',
						summary: order.summary || ''
					}
				}
				result['Inprogress'] = orders.where(status_id: 2).map{|order| {
						id: order.id, 
						date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : '', 
						service_name: order.service.name || '', 
						document_id: order.document_id || '',
						order_status: order.status.name || 'INCOMPLETE',
						txnid: order.txnid, 
						payment_status: order.payment ? order.payment.status.name : 'INCOMPLETE', 
						deletable: order.payment ? false : true,
						product: order.product || '', 
						mode: order.payment ? order.payment.mode : 'N/A', 
						amount: order.total_amount,
						txn_date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : 'N/A',
						address: order.address ? {address1: order.address.address1, address2: order.address.address2,tag: order.address.tag} : '' ,
						pickup_amount: order.pickup_amount || '' , 
						soft_copy_url: order.soft_copy_url || '',
						delivery_status: order.delivery_status ? order.delivery_status.name : '',
						delivery_partner: order.delivery_partner ? order.delivery_partner.name : '' ,
						delivery_tracking_no: order.delivery_tracking_no || '',
						receipt_url: order.invoice_url || '',
						draft_confirmed: order.draft_confirmed,
						draft_required: order.draft_required,
						draft_url: order.draft_url || '',
						discount: order.discount || '',
						coupon: order.coupon ? order.coupon.code : '',
						stamp_amount: order.stamp_amount || '',
						delivery_amount: order.delivery_amount || '',
						answers: order.answers.map{|answer| {id: answer.id, body: answer.body, question_id: answer.question_id, question: answer.question.name}},
						automation_done: (order.automated_answers.length > 0) ? true : false,
						document_price: order.document_id ? order.document.default_price : '',
						summary: order.summary || ''
					}
				}
				result['Completed'] = orders.where(status_id: 3).map{|order| {
						id: order.id, 
						date: order.payment.created_at.strftime('%d/%m/%Y'), 
						service_name: order.service.name || '', 
						document_id: order.document_id || '',
						order_status: order.status.name || 'INCOMPLETE',
						txnid: order.txnid, 
						payment_status: order.payment ? order.payment.status.name : 'INCOMPLETE', 
						deletable: order.payment ? false : true,
						product: order.product || '', 
						mode: order.payment ? order.payment.mode : 'N/A', 
						amount: order.total_amount,
						txn_date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : 'N/A',
						address: order.address ? {address1: order.address.address1, address2: order.address.address2,tag: order.address.tag} : '' ,
						pickup_amount: order.pickup_amount || '', 
						soft_copy_url: order.soft_copy_url || '',
						delivery_status: order.delivery_status ? order.delivery_status.name : '',
						delivery_partner: order.delivery_partner ? order.delivery_partner.name : '' ,
						delivery_tracking_no: order.delivery_tracking_no || '',
						receipt_url: order.invoice_url || '',
						draft_confirmed: order.draft_confirmed,
						draft_required: order.draft_required,
						draft_url: order.draft_url || '',
						discount: order.discount || '',
						coupon: order.coupon ? order.coupon.code : '',
						stamp_amount: order.stamp_amount || '',
						delivery_amount: order.delivery_amount || '',
						answers: order.answers.map{|answer| {id: answer.id, body: answer.body, question_id: answer.question_id, question: answer.question.name}},
						automation_done: (order.automated_answers.length > 0) ? true : false,
						document_price: order.document_id ? order.document.default_price : '',
						summary: order.summary || ''
					}
				}
				response.merge! ApiStatusList::OK
				response['result'] = result
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		# def get_delivery_info
			
		# end

		def delivery_info
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id]) && params[:order_id]
				delivery_info = Orders::DeliveryInfo.new(name: params[:name], email: params[:email], phone: params[:mobile], address1: params[:address1], 
					address2: params[:address2], landmark: params[:landmark], pincode: params[:pincode], message: params[:message], order_id: params[:order_id])
    			if delivery_info.save!
    				response.merge! ApiStatusList::OK
	    			result['order'] = {
	    				order_id: params[:order_id],
	    				# invoice_id: "NMDJK#{Random.rand(100)}"
	    			}
	    			response['result'] = result
	    		else
					response.merge! ApiStatusList::INVALID_REQUEST
    			end
    		else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def order_tracking
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id]) && params[:order_id]
				result = Orders::Track.where(order_id: params[:order_id]).map{|track| {
					product: track.order.product,
					verified: track.verified, 
					printed: track.printed, 
					drafted: track.drafted, 
					delivered: track.delivered,
					signed: track.signed, 
					notary: track.notary,
					user_status: track.user_status,
					verified_at: track.verified_at ? track.verified_at.strftime('%d/%m/%Y') : '' ,
					printed_at: track.printed_at ? track.printed_at.strftime('%d/%m/%Y') : '' ,
					drafted_at: track.drafted_at ? track.drafted_at.strftime('%d/%m/%Y') : '' ,
					delivered_at: track.delivered_at ? track.delivered_at.strftime('%d/%m/%Y') : '' ,
					signed_at: track.signed_at ? track.signed_at.strftime('%d/%m/%Y') : '' ,
					notary_at: track.notary_at ? track.notary_at.strftime('%d/%m/%Y') : ''
					}
				}
				response.merge! ApiStatusList::OK
    		else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result.first
			render :json => response
		end

		def tracking_list
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id])
				result = Order.includes(:service, :status, payment: [:status]).where(user_id: params[:user_id], status_id: [2,3]).order('updated_at DESC').map{|order| {
						id: order.id, date: order.created_at.strftime('%d/%m/%Y'), service_name: order.service.name || '', order_status: order.status.name || 'INCOMPLETE',
						txnid: order.txnid, payment_status: order.payment ? order.payment.status.name : 'INCOMPLETE', deletable: order.payment ? false : true,
						product: order.product || '', mode: order.payment ? order.payment.mode : 'NA', amount: order.total_amount,
						discount: order.discount || '',
						coupon: order.coupon ? order.coupon.code : '',
						txn_date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : 'NA',
						address: order.address ? {address1: order.address.address1, address2: order.address.address2,tag: order.address.tag} : '',
						pickup_amount: order.pickup_amount || ''  
				}}
				response.merge! ApiStatusList::OK
    		else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def get_soft_copy
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id]) && params[:order_id] && Order.exists?(params[:order_id])
				order = Order.where(user_id: params[:user_id], id: params[:order_id]).first
				if order
					result = order.soft_copy_url || ''
					response.merge! ApiStatusList::Ok
				else
					response.merge! ApiStatusList::ZERO_RESULTS
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def make_payment
			response = Hash.new
			result = Hash.new
			if params[:user_id] && User.exists?(params[:user_id]) && params[:order_id] && Order.exists?(params[:order_id])
				order_status = Orders::Status.where(name: params[:order_status].capitalize).first
				order = Order.includes(:service, :status, payment: [:status]).where(user_id: params[:user_id], status_id: order_status.id, id: params[:order_id])
				if order
					result = order.map{|order| {
						id: order.id, date: order.created_at.strftime('%d/%m/%Y'), 
						service_name: order.service.name || '', 
						order_status: order.status.name || 'INCOMPLETE',
						txnid: order.txnid, 
						payment_status: order.payment ? order.payment.status.name : 'INCOMPLETE', 
						deletable: order.payment ? false : true,
						product: order.product || '', 
						mode: order.payment ? order.payment.mode : 'NA', 
						amount: order.total_amount,
						txn_date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : 'NA', 
						address: order.address ? {address1: order.address.address1, address2: order.address.address2,tag: order.address.tag} : '' ,
						pickup_amount: order.pickup_amount || '',
						discount: order.discount || '',
						coupon: order.coupon ? order.coupon.code : ''
						}
					}
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::ZERO_RESULTS
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		# VALIDATE AND APPLY COUPONS ON ORDER
		def validate_coupon
			response = Hash.new
			result = Hash.new
			if params[:coupon] && params[:total_amount]#&& params[:user_id] && params[:order_id]
				# order = Order.where(id: params[:order_id], user_id: params[:user_id]).first
				coupon = Coupon.where(code: params[:coupon].upcase, active: true).first
				# if coupon #order && !order.coupon
				if coupon && (Coupon.validate_coupon params[:coupon]) #&& order 
					discount = Coupon.get_discount_amount(coupon.id,nil, params[:total_amount])
					# discount = params[:total_amount].to_f / coupon.value.to_f
					# order.discount = discount
					# order.coupon_id = coupon.id
					# order.save!
					response['discount'] = discount.to_s
					response['coupon_id'] = coupon.id
					response['coupon_code'] = coupon.code
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INVALID_COUPON
				end
				# else
				# 	response.merge! ApiStatusList::COUPON_APPLIED
				# end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end
		
		# Remove coupon from and order
		def remove_coupon
			response = Hash.new
			result = Hash.new
			if params[:order_id]
				order = Order.where(id: params[:order_id], user_id: params[:user_id]).first
				if order && order.coupon
					order.discount = nil
					order.coupon_id = nil
					order.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::COUPON_NOT_APPLIED
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		# LAST 5 ORDERS
		def recent_orders
			response = Hash.new
			result = Hash.new
			if params[:user_id]	
				user = User.find params[:user_id]	
				status_ids = Orders::Status.where("name != ?","DELETED").pluck(:id)
				if user
					orders = Order.includes(:service, :address, :delivery_status, :delivery_partner, :status, payment: [:status]).where(user_id: params[:user_id]).limit(5).order('id desc')
					result['orders'] = orders.where(status_id: status_ids).map{|order| {
							id: order.id, 
							date: order.payment ?  order.payment.created_at.strftime('%d/%m/%Y') : order.created_at.strftime('%d/%m/%Y'),
							service_name: order.service.name || '', 
							document_id: order.document_id || '',
							order_status: order.status.name || 'INCOMPLETE',
							txnid: order.txnid, 
							payment_status: order.payment ? order.payment.status.name : 'INCOMPLETE', 
							deletable: order.payment ? false : true,
							product: order.product || '', 
							mode: order.payment ? order.payment.mode : 'N/A', 
							amount: order.total_amount,
							txn_date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : 'N/A',
							address: order.address ? {address1: order.address.address1, address2: order.address.address2,tag: order.address.tag} : '' ,
							pickup_amount: order.pickup_amount || '' , 
							soft_copy_url: order.soft_copy_url || '',
							delivery_status: order.delivery_status ? order.delivery_status.name : '',
							delivery_partner: order.delivery_partner ? order.delivery_partner.name : '' ,
							delivery_tracking_no: order.delivery_tracking_no || '',
							receipt_url: order.invoice_url || '',
							draft_confirmed: order.draft_confirmed,
							draft_required: order.draft_required,
							draft_url: order.draft_url || '',
							discount: order.discount || '',
							coupon: order.coupon ? order.coupon.code : '',
							stamp_amount: order.stamp_amount || '',
							delivery_amount: order.delivery_amount || '',
							answers: order.automated_answers.map{|answer| {id: answer.id, section_id: answer.section_id,body: answer.body,question_id: answer.question_id}}
							}}
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		# LAST PENDING ORDER
		def current_pending_order
			response = Hash.new
			result = Hash.new
			status_ids = Orders::Status.where(name: ['INPROGRESS', 'INCOMPLETE']).pluck(:id)
			# order = Order.where(user_id: params[:user_id], status_id: status_ids)
			if params[:user_id]
				user = User.find params[:user_id]	
				if user #&& order
					orders = Order.includes(:service, :address, :delivery_status, :delivery_partner, :status, payment: [:status]).where(user_id: params[:user_id]).limit(5).order('id desc')
					result['orders'] = orders.where(status_id: status_ids).limit(1).map{|order| {
							id: order.id, 
							date: order.payment ?  order.payment.created_at.strftime('%d/%m/%Y') : order.created_at.strftime('%d/%m/%Y'),
							service_name: order.service.name || '', 
							document_id: order.document_id || '',
							order_status: order.status.name || 'INCOMPLETE',
							txnid: order.txnid, 
							payment_status: order.payment ? order.payment.status.name : 'INCOMPLETE', 
							deletable: order.payment ? false : true,
							product: order.product || '', 
							mode: order.payment ? order.payment.mode : 'N/A', 
							amount: order.total_amount,
							txn_date: order.payment ? order.payment.created_at.strftime('%d/%m/%Y') : 'N/A',
							address: order.address ? {address1: order.address.address1, address2: order.address.address2,tag: order.address.tag} : '' ,
							pickup_amount: order.pickup_amount || '' , 
							soft_copy_url: order.soft_copy_url || '',
							delivery_status: order.delivery_status ? order.delivery_status.name : '',
							delivery_partner: order.delivery_partner ? order.delivery_partner.name : '' ,
							delivery_tracking_no: order.delivery_tracking_no || '',
							receipt_url: order.invoice_url || '',
							draft_confirmed: order.draft_confirmed,
							draft_required: order.draft_required,
							draft_url: order.draft_url || '',
							discount: order.discount || '',
							coupon: order.coupon ? order.coupon.code : '',
							stamp_amount: order.stamp_amount || '',
							delivery_amount: order.delivery_amount || '',
							answers: order.automated_answers.map{|answer| {id: answer.id, section_id: answer.section_id,body: answer.body,question_id: answer.question_id}}
							}}
					response.merge! ApiStatusList::OK
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		# LAST ORDER TRACKING
		def current_order_tracking
			response = Hash.new
			result = Hash.new
			if params[:user_id]	
				user = User.find params[:user_id]	
				if user
					status_ids = Orders::Status.where(name: ['INPROGRESS']).pluck(:id)
					order = Order.where(user_id: params[:user_id], status_id: status_ids)
					if order && (order.length > 0)
						result = Orders::Track.where(order_id: order.first.id).map{|track| {
							product: track.order.product,
							verified: track.verified, 
							printed: track.printed, 
							drafted: track.drafted, 
							delivered: track.delivered,
							signed: track.signed, 
							notary: track.notary,
							user_status: track.user_status
							# ,
							# verified_at: track.verified_at ? track.verified_at.strftime('%d/%m/%Y') : '' ,
							# printed_at: track.printed_at ? track.printed_at.strftime('%d/%m/%Y') : '' ,
							# drafted_at: track.drafted_at ? track.drafted_at.strftime('%d/%m/%Y') : '' ,
							# delivered_at: track.delivered_at ? track.delivered_at.strftime('%d/%m/%Y') : '' ,
							# signed_at: track.signed_at ? track.signed_at.strftime('%d/%m/%Y') : '' ,
							# notary_at: track.notary_at ? track.notary_at.strftime('%d/%m/%Y') : ''
							}
						}
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::NOT_FOUND
					end
				else
					response.merge! ApiStatusList::USER_NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		# Generate Hash and Payment params

		def generate_payu_hash
			response = Hash.new
			if params[:txnid] && params[:user_id]
				order = Order.where(txnid: params[:txnid].upcase, user_id: params[:user_id])
				if order.length == 1
					response['result'] = Order.generate_payu_url(order.first.id,params[:address_id])
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def presigned_s3_url
			response = Hash.new
			result = Hash.new
			if params[:user_id]
				bucket = AWS::S3::Bucket.new(ENV['S3_BUCKET'])
									.presigned_post( 
										policy: 'public_read_write',
										secure: true,
										expires: (Time.now + 15.minutes), 
										key: params[:object_key],
										content_length: params[:content_length]
										)
				if bucket.url
					response['result'] = {fields: bucket.fields.map{|key,value| { "#{key.downcase}" => value } },
								url: bucket.url.host, port: bucket.url.port, scheme: bucket.url.scheme
								 }
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def paytm_response_status
			response = Hash.new
			result = Hash.new
			if params[:txnid]  
				txnid = params[:txnid]
				status = Payments::Paytm.check_paytm_status txnid
				result['paytm_success'] = status
				response.merge! ApiStatusList::OK	
			else
				response.merge! ApiStatusList::INVALID_REQUEST	
			end
			response['result'] = result
			render :json => response
		end
	end
end
end
