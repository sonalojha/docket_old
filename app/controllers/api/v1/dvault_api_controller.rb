module Api
module V1
	class DvaultApiController < ApiController
	  
		def check_permission
			return false
		end

		def check_dvault_active
			response = Hash.new
			result = Hash.new
			if params[:user_id]	
				user = User.find params[:user_id]	
				if user && user.dvault_activated && user.dvault
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_ACTIVE
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			# response['result'] = result
			render :json => response
		end

		def create_pin
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]
			if params[:user_id] && user 
				if !user.dvault
					dvault = Dvault.new(pin: params[:pin])
					dvault.user_id = params[:user_id]
					if dvault.save!
						user.dvault_activated = true
						user.save!
						result = get_categories result
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::UNKNOWN_ERROR
					end
				else
					response.merge! ApiStatusList::DVAULT_ACTIVE
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end
		
		def get_categories result
			result = ::Dvaults::Category.all.map{|cat| {id: cat.id, name: cat.name}}
			# result['sub_categories'] = ::Dvaults::SubCategory.all.map{|cat| {id: cat.id, name: cat.name, category_id: cat.category_id}}
			return result
		end
		
		def login
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated && user.dvault
				id = user.dvault.id
				authenticate = Dvault.authenticate(id, params[:pin])
				if authenticate
					# result['dvault'] = {dvault_id: authenticate.id}
					result = get_categories result
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INVALID_PIN
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def forgot_pin
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated
				
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def categories
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated
				result = get_categories result
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end


		# get all files for this category
		def get_files
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated
				sub_categories = ::Dvaults::SubCategory.where(category_id: params[:category_id])
				result['files'] = ::Dvaults::UserFile.joins(:sub_category)
					.joins('INNER JOIN dvaults_categories on dvaults_categories.id=dvaults_sub_categories.category_id')
					.where("dvaults_user_files.dvault_id=#{user.dvault.id}")
					.where("dvaults_categories.id=#{params[:category_id]}")
					.select('dvaults_user_files.*').map{|file| { id: file.id,
						filename: file.filename, tag: file.tag || '', mime_type: file.mime_type,
						vault_uid: file.vault_uid,
						size: file.size, profile_id: file.profile_id || '', 
						thumbnail_image: file.thumbnail_image || '',
						sub_category_id: file.sub_category_id, 
						created_at: file.created_at.strftime("%d/%m/%Y %H:%M:%S"),
						updated_at: file.updated_at.strftime("%d/%m/%Y %H:%M:%S")
					}}
				result['sub_categories'] = sub_categories.map{|cat| {id: cat.id, name: cat.name, category_id: cat.category_id}}
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		# upload file to a particular category
		def upload_file
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated
				#upload code here
				category = Dvaults::SubCategory.where(id: params[:sub_category_id]).first.category
				file = ::Dvaults::UserFile.new(
					filename: params[:filename],
					tag: params[:tag],
					mime_type: params[:mime_type],
					size: params[:size],
					vault_uid: params[:s3_url],
					dvault_id: user.dvault.id,
					thumbnail_image: params[:thumbnail_image],
					sub_category_id: params[:sub_category_id]
				)
				file.save!
				result['files'] = ::Dvaults::UserFile.joins(:sub_category)
					.joins('INNER JOIN dvaults_categories on dvaults_categories.id=dvaults_sub_categories.category_id')
					.where("dvaults_user_files.dvault_id=#{user.dvault.id}")
					.where("dvaults_categories.id=#{category.id}")
					.select('dvaults_user_files.*').map{|file| { id: file.id,
						filename: file.filename, tag: file.tag || '', mime_type: file.mime_type,
						size: file.size, 
						profile_id: file.profile_id || '', 
						vault_uid: file.vault_uid,
						thumbnail_image: file.thumbnail_image || '',
						sub_category_id: file.sub_category_id, 
						created_at: file.created_at.strftime("%d/%m/%Y %H:%M:%S"),
						updated_at: file.updated_at.strftime("%d/%m/%Y %H:%M:%S")
					}}
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def edit_file
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated && params[:file_id]
				file = ::Dvaults::UserFile.where(dvault_id: user.dvault.id, id: params[:file_id]).first
				file.filename 	= params[:filename] if params[:filename]
				file.tag 		= params[:tag] if params[:tag]
				file.mime_type 	= params[:mime_type] if params[:mime_type]
				file.vault_uid 	= params[:vault_uid] if params[:vault_uid]
				file.size 		= params[:size] if params[:size]
				file.profile_id = params[:profile_id] if params[:profile_id]
				file.thumbnail_image = params[:thumbnail_image] if params[:thumbnail_image]
				file.sub_category_id = params[:sub_category_id] if params[:sub_category_id]
				file.save!
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def send_file
			response = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault && params[:file_ids] && params[:email]
				DvaultJob.perform_later(params[:user_id],params[:email],params[:file_ids])
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		def delete_file
			response = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault && params[:file_ids]
				if 	::Dvaults::UserFile.where(dvault_id: user.dvault.id, id: params[:file_ids]).destroy_all
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::UNKNOWN_ERROR
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end

		# email / zip of files  
		def email_zip
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated && params[:file_ids]
				path = ::Dvaults::UserFile.create_zip(params[:file_ids])
				# attach in Email attachment and send
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		# search for uploaded doc
		def search_doc
			response = Hash.new
			result = Hash.new
			user = User.find params[:user_id]	
			if user && user.dvault_activated && params[:query]
				result['files'] = Dvaults::UserFile.predictions(dvault_id,params[:query])
										.map{|f|{id: f.id, file_name: f.filename, tag: f.tag, mime_type: f.mime_type, sub_category_id: f.sub_category_id}}
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end
		# generate otp
		def reset_pin_request 
			response = Hash.new
			if params[:user_id] && User.exists?(params[:user_id])
				user = User.find(params[:user_id])
				if user.dvault_activated
					verify = user.verifications.create(verification_type: 'Pin::Reset')
					verify.save!
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::INVALID_REQUEST
				end
			else
				response.merge! ApiStatusList::USER_NOT_FOUND
			end
			render json: response
		end
		##### VALIDATE OTP #######
		def pin_reset_otp
			response = Hash.new
			if params[:user_id] && params[:otp]
				user = User.find(params[:user_id])
				if user
					verify = Verification.where(code: params[:otp], verification_type: 'Pin::Reset', user_id: params[:user_id], status_id: ::Status.where(name: 'ACTIVE').first.id)
					if verify.length > 0
						verify.first.status_id = ::Status.where(name: 'INACTIVE').first.id
						verify.first.save!
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::NOT_FOUND
					end
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end
		##### RESET PIN ######
		def pin_reset
			response = Hash.new
			result = Hash.new
			if params[:user_id]
				user = User.find(params[:user_id])
				if user && user.dvault_activated && params[:password]
					dvault = user.dvault
					dvault.pin = params[:password]
					if dvault.save!
						response['result'] = get_categories result
						response.merge! ApiStatusList::OK
					else
						response.merge! ApiStatusList::UNKNOWN_ERROR
					end
				else
					response.merge! ApiStatusList::INVALID_REQUEST
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end
	end
end
end		