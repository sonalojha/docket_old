module Api
module V0
	class PaymentApiController < ApiController
	  
		def check_permission
			return false
		end

		def payu_callback
			response = Hash.new
			result = Hash.new
			if Order.is_valid_transaction?(params[:txnid]) #&& params[:status] == 'success'
				payment = Payment.new(mihpayid: params[:mihpayid],mode: params[:mode], amount: params[:amount],
				                secret_hash: params[:hash], pg_type: params[:PG_TYPE],bank_ref_num: params[:bank_ref_num],
				                bankcode: params[:bankcode], name_on_card: params[:name_on_card], cardnum: params[:cardnum],
				                payuMoneyId: params[:payuMoneyId], discount: params[:discount], payu_status: params[:status],
				                )
				payment.status_id = Payments::Status.where(name: 'PAYMENT RECEIVED').first.id
				order = Order.where(txnid: params[:txnid]).first
				payment.order_id = order.id
				# order.delivery_info.new(firstname: params[:firstname], email: params[:email], phone: params[:phone])
			    if payment.save!
			        Order.update_status(params[:txnid])
					response.merge! ApiStatusList::OK
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end
	end
end
end