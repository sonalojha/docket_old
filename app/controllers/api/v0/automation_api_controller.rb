module Api
module V0
	class AutomationApiController < ApiController
		def check_permission
			return false
		end

		def answer_capture
			response = Hash.new
			result = Hash.new
			order = Order.where(txnid: params[:txnid], user_id: params[:user_id]).first
			if order
				answers = JSON.parse(params[:answers])
			    answers.each do |answer|
			    	document_answer = order.automated_answers.find_or_initialize_by(question_id: answer['question_id'],
			    	order_id: order.id, document_id: params['document_id'], section_id: answer['section_id'])
					document_answer.body = answer['body']
				    document_answer.save!
				end
				TemplateSendJob.perform_later(order.id)
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::UNKNOWN_ERROR
			end
			render :json => response
		end

		def get_questions
			response = Hash.new
			result = Hash.new
			if params[:document_id] && params[:user_id]
				result = Document.includes(sections:[questions: [:options, :response_type]]).where(id: params[:document_id]).map{|document|{
							id: document.id, name: document.name, 
							sections: document.sections.map{|section| {
								id:section.id,
								name: section.name,
								section_no: section.section_no || '',
								description: section.description || '',
								questions: section.questions.map{|quest|{
									id: quest.id,
									response_type_id: quest.response_type_id,
									is_mandatory: quest.is_mandatory,
									body: quest.body,
									associated_text: quest.associated_text,
									placeholder: quest.placeholder,
									options: quest.options.map{|opt|{
										id: opt.id,
										name: opt.name,
										priority: opt.priority || ''
										}
									}
									}
								}
							}
						} 
					}
				}
				result.first['response_types'] = Documents::Questions::ResponseType.all.map{|r| {id:r.id, name: r.name, regex: r.regex || '' }}
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result.first
			render :json => response
		end

		# def create_document
		# 	doc = DocxReplace::Doc.new("#{File.dirname(__FILE__)}/template/template2.docx", "#{File.dirname(__FILE__)}/tmp")
		# 	answers.each do |key,value|
		# 		doc.replace("$#{key}$", value.upcase, true)
		# 	end
		# 	tmp_file = Tempfile.new('word_tempate', "#{File.dirname(__FILE__)}/tmp")
		# 	doc.commit("./tmp/word_new.docx")
		# 	puts doc, tmp_file.path
		# end
	end
end
end