module Api
module V0
	class PromotionApiController < ApiController
		def check_permission
			return false
		end

		def promotions
			response = Hash.new
			result = Hash.new
			if params[:user_id]	
				user = User.find params[:user_id]	
				if user
					result['promotions'] = Promotion.valid_offers.select([:id,:name,:category,:valid_by,:offer,:offer_type]).as_json
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end

		def check_promotion
			response = Hash.new
			result = Hash.new
			if params[:user_id]	&& params[:promotion_id]
				user = User.find params[:user_id]	
				if user
					result = Promotion.all.check_validity(params[:promotion_id])
					response.merge! ApiStatusList::OK
				else
					response.merge! ApiStatusList::NOT_FOUND
				end
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			response['result'] = result
			render :json => response
		end
	end	
end
end