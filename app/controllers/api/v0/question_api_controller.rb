module Api
module V0
	class QuestionApiController < ApiController
	  
		def check_permission
			return false
		end

		def service_questions
			response = Hash.new
			result = Hash.new
			if params[:service_id]
				result['questions'] = Services::Question.includes(:service, :options, :response_type).where(service_id: params[:service_id].to_i)
												.where('services_questions.priority is not null').map{|question|{
									name: question.name || '', data_type: question.data_type || '', api_url: question.api_url || '', 
									price: question.price || '',priority: question.priority || '', hint: question.hint || '', 
									error_msg: question.error_msg || '', placeholder: question.placeholder || '', response_type: question.response_type.name || ''.name,
									options: question.options.map{|option| {
										name: option.name || '', condition: option.condition || '', other_questions: option.other_questions || ''}
										}
									}}
				result['optional_questions'] = Services::Question.includes(:service, :options, :response_type)
												.where(service_id: params[:service_id])
												.where('services_questions.priority is null').map{|question|{
									name: question.name || '', data_type: question.data_type || '', api_url: question.api_url || '', 
									price: question.price || '',priority: question.priority || '', hint: question.hint || '', 
									error_msg: question.error_msg || '', placeholder: question.placeholder || '', response_type: question.response_type.name || ''.name,
									options: question.options.map{|option| {
										name: option.name || '', condition: option.condition || '', other_questions: option.other_questions || ''}
										}
									}}
				response['result'] = result
				response.merge! ApiStatusList::OK
			else
				response.merge! ApiStatusList::INVALID_REQUEST
			end
			render :json => response
		end
	end
end
end