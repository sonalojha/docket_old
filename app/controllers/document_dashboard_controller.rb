class DocumentDashboardController < ApplicationController
	layout 'dvault'
	def check_permission
		return false
	end
	def index
		document_id = params[:id] || 5
		if params[:order_id]
			@order = Order.find(params[:order_id])
		end
	  	# document_id  = Document.find() #Random.rand([1359, 1360, , 1317])
	  	@document = Document.includes(sections:[questions: [:options, :response_type]]).where(id: document_id).first
	  	@site_state = 'flip_dashboard_bg'
	end

	def generate_docx
	  	document = Document.find(params[:document_id])
	  	params[:answer].each do |answer, value|
	  		answer = Documents::Answer.find_or_initialize_by(order_id: params[:order_id], document_id:params[:document_id],
	  							question_id: answer.to_i, section_id: value[:section_id])
			answer.body=value[:body]
			answer.save!
	  	end
	  	array = Documents::Answer.all.joins(:question).where(order_id: params[:order_id]).pluck('documents_questions.placeholder,documents_answers.body')
	  	answers = Hash[array.map {|key, value| [key, value]}]
	  	url = Document.create_template(current_user,answers,document.template_url,document.name)
	  	@soft_copy_url = url
	end
end