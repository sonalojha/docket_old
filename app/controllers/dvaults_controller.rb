class DvaultsController < ApplicationController
  before_action :set_dvault, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  # GET /dvaults
  # GET /dvaults.json
  def index
    @dvaults = Dvault.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /dvaults/1
  # GET /dvaults/1.json
  def show
  end

  # GET /dvaults/new
  def new
    @dvault = Dvault.new
  end

  # GET /dvaults/1/edit
  def edit
  end

  # POST /dvaults
  # POST /dvaults.json
  def create
    @dvault = Dvault.new(dvault_params)

    respond_to do |format|
      if @dvault.save
        format.html { redirect_to @dvault, notice: 'Dvault was successfully created.' }
        format.json { render :show, status: :created, location: @dvault }
      else
        format.html { render :new }
        format.json { render json: @dvault.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dvaults/1
  # PATCH/PUT /dvaults/1.json
  def update
    respond_to do |format|
      if @dvault.update(dvault_params)
        format.html { redirect_to @dvault, notice: 'Dvault was successfully updated.' }
        format.json { render :show, status: :ok, location: @dvault }
      else
        format.html { render :edit }
        format.json { render json: @dvault.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dvaults/1
  # DELETE /dvaults/1.json
  def destroy
    @dvault.destroy
    respond_to do |format|
      format.html { redirect_to dvaults_url, notice: 'Dvault was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dvault
      @dvault = Dvault.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dvault_params
      params.require(:dvault).permit(:user_id, :pin)
    end
end
