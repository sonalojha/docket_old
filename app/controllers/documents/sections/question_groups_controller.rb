class Documents::Sections::QuestionGroupsController < ApplicationController
  before_action :set_documents_sections_question_group, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  # GET /documents/sections/question_groups
  # GET /documents/sections/question_groups.json
  def index
    @documents_sections_question_groups = Documents::Sections::QuestionGroup.all
  end

  # GET /documents/sections/question_groups/1
  # GET /documents/sections/question_groups/1.json
  def show
  end

  # GET /documents/sections/question_groups/new
  def new
    @documents_sections_question_group = Documents::Sections::QuestionGroup.new
  end

  # GET /documents/sections/question_groups/1/edit
  def edit
  end

  # POST /documents/sections/question_groups
  # POST /documents/sections/question_groups.json
  def create
    @documents_sections_question_group = Documents::Sections::QuestionGroup.new(documents_sections_question_group_params)

    respond_to do |format|
      if @documents_sections_question_group.save
        format.html { redirect_to @documents_sections_question_group, notice: 'Question group was successfully created.' }
        format.json { render :show, status: :created, location: @documents_sections_question_group }
      else
        format.html { render :new }
        format.json { render json: @documents_sections_question_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/sections/question_groups/1
  # PATCH/PUT /documents/sections/question_groups/1.json
  def update
    respond_to do |format|
      if @documents_sections_question_group.update(documents_sections_question_group_params)
        format.html { redirect_to @documents_sections_question_group, notice: 'Question group was successfully updated.' }
        format.json { render :show, status: :ok, location: @documents_sections_question_group }
      else
        format.html { render :edit }
        format.json { render json: @documents_sections_question_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/sections/question_groups/1
  # DELETE /documents/sections/question_groups/1.json
  def destroy
    @documents_sections_question_group.destroy
    respond_to do |format|
      format.html { redirect_to documents_sections_question_groups_url, notice: 'Question group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_documents_sections_question_group
      @documents_sections_question_group = Documents::Sections::QuestionGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def documents_sections_question_group_params
      params.require(:documents_sections_question_group).permit(:section_id, :question_id, :priority)
    end
end
