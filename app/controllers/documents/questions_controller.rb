class Documents::QuestionsController < ApplicationController
  before_action :set_documents_question, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /documents/questions
  # GET /documents/questions.json
  def index
    @documents_questions = Documents::Question.all.order('document_id').paginate(:page => params[:page], :per_page => 30)
  end

  # GET /documents/questions/1
  # GET /documents/questions/1.json
  def show
  end

  # GET /documents/questions/new
  def new
    @documents_question = Documents::Question.new
  end

  # GET /documents/questions/1/edit
  def edit
  end

  # POST /documents/questions
  # POST /documents/questions.json
  def create
    @documents_question = Documents::Question.new(documents_question_params)

    respond_to do |format|
      if @documents_question.save
        format.html { redirect_to @documents_question, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @documents_question }
      else
        format.html { render :new }
        format.json { render json: @documents_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/questions/1
  # PATCH/PUT /documents/questions/1.json
  def update
    respond_to do |format|
      if @documents_question.update(documents_question_params)
        format.html { redirect_to @documents_question, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @documents_question }
      else
        format.html { render :edit }
        format.json { render json: @documents_question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/questions/1
  # DELETE /documents/questions/1.json
  def destroy
    @documents_question.destroy
    respond_to do |format|
      format.html { redirect_to documents_questions_url, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_documents_question
      @documents_question = Documents::Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def documents_question_params
      params.require(:documents_question).permit(:placeholder, :response_type_id, :is_mandatory, :body, :associated_text, :document_id)
    end
end
