class Documents::AnswersController < ApplicationController
  before_action :set_documents_answer, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  # GET /documents/answers
  # GET /documents/answers.json
  def index
    @documents_answers = Documents::Answer.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /documents/answers/1
  # GET /documents/answers/1.json
  def show
  end

  # GET /documents/answers/new
  def new
    @documents_answer = Documents::Answer.new
  end

  # GET /documents/answers/1/edit
  def edit
  end

  # POST /documents/answers
  # POST /documents/answers.json
  def create
    @documents_answer = Documents::Answer.new(documents_answer_params)

    respond_to do |format|
      if @documents_answer.save
        format.html { redirect_to @documents_answer, notice: 'Answer was successfully created.' }
        format.json { render :show, status: :created, location: @documents_answer }
      else
        format.html { render :new }
        format.json { render json: @documents_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/answers/1
  # PATCH/PUT /documents/answers/1.json
  def update
    respond_to do |format|
      if @documents_answer.update(documents_answer_params)
        format.html { redirect_to @documents_answer, notice: 'Answer was successfully updated.' }
        format.json { render :show, status: :ok, location: @documents_answer }
      else
        format.html { render :edit }
        format.json { render json: @documents_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/answers/1
  # DELETE /documents/answers/1.json
  def destroy
    @documents_answer.destroy
    respond_to do |format|
      format.html { redirect_to documents_answers_url, notice: 'Answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_documents_answer
      @documents_answer = Documents::Answer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def documents_answer_params
      params.require(:documents_answer).permit(:document_id, :section_id, :question_id, :body, :order_id)
    end
end
