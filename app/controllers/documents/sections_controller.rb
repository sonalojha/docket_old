class Documents::SectionsController < ApplicationController
  before_action :set_documents_section, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /documents/sections
  # GET /documents/sections.json
  def index
    @documents_sections = Documents::Section.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /documents/sections/1
  # GET /documents/sections/1.json
  def show
  end

  # GET /documents/sections/new
  def new
    @documents_section = Documents::Section.new
  end

  # GET /documents/sections/1/edit
  def edit
  end

  # POST /documents/sections
  # POST /documents/sections.json
  def create
    @documents_section = Documents::Section.new(documents_section_params)

    respond_to do |format|
      if @documents_section.save
        format.html { redirect_to @documents_section, notice: 'Section was successfully created.' }
        format.json { render :show, status: :created, location: @documents_section }
      else
        format.html { render :new }
        format.json { render json: @documents_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/sections/1
  # PATCH/PUT /documents/sections/1.json
  def update
    respond_to do |format|
      if @documents_section.update(documents_section_params)
        format.html { redirect_to @documents_section, notice: 'Section was successfully updated.' }
        format.json { render :show, status: :ok, location: @documents_section }
      else
        format.html { render :edit }
        format.json { render json: @documents_section.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/sections/1
  # DELETE /documents/sections/1.json
  def destroy
    @documents_section.destroy
    respond_to do |format|
      format.html { redirect_to documents_sections_url, notice: 'Section was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_documents_section
      @documents_section = Documents::Section.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def documents_section_params
      params.require(:documents_section).permit(:name, :section_no, :description, :document_id)
    end
end
