class Documents::Questions::OptionsController < ApplicationController
  before_action :set_documents_questions_option, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /documents/questions/options
  # GET /documents/questions/options.json
  def index
    @documents_questions_options = Documents::Questions::Option.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /documents/questions/options/1
  # GET /documents/questions/options/1.json
  def show
  end

  # GET /documents/questions/options/new
  def new
    @documents_questions_option = Documents::Questions::Option.new
  end

  # GET /documents/questions/options/1/edit
  def edit
  end

  # POST /documents/questions/options
  # POST /documents/questions/options.json
  def create
    @documents_questions_option = Documents::Questions::Option.new(documents_questions_option_params)

    respond_to do |format|
      if @documents_questions_option.save
        format.html { redirect_to @documents_questions_option, notice: 'Option was successfully created.' }
        format.json { render :show, status: :created, location: @documents_questions_option }
      else
        format.html { render :new }
        format.json { render json: @documents_questions_option.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/questions/options/1
  # PATCH/PUT /documents/questions/options/1.json
  def update
    respond_to do |format|
      if @documents_questions_option.update(documents_questions_option_params)
        format.html { redirect_to @documents_questions_option, notice: 'Option was successfully updated.' }
        format.json { render :show, status: :ok, location: @documents_questions_option }
      else
        format.html { render :edit }
        format.json { render json: @documents_questions_option.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/questions/options/1
  # DELETE /documents/questions/options/1.json
  def destroy
    @documents_questions_option.destroy
    respond_to do |format|
      format.html { redirect_to documents_questions_options_url, notice: 'Option was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_documents_questions_option
      @documents_questions_option = Documents::Questions::Option.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def documents_questions_option_params
      params.require(:documents_questions_option).permit(:name, :question_id, :priority)
    end
end
