class Documents::Questions::ResponseTypesController < ApplicationController
  before_action :set_documents_questions_response_type, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /documents/questions/response_types
  # GET /documents/questions/response_types.json
  def index
    @documents_questions_response_types = Documents::Questions::ResponseType.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /documents/questions/response_types/1
  # GET /documents/questions/response_types/1.json
  def show
  end

  # GET /documents/questions/response_types/new
  def new
    @documents_questions_response_type = Documents::Questions::ResponseType.new
  end

  # GET /documents/questions/response_types/1/edit
  def edit
  end

  # POST /documents/questions/response_types
  # POST /documents/questions/response_types.json
  def create
    @documents_questions_response_type = Documents::Questions::ResponseType.new(documents_questions_response_type_params)

    respond_to do |format|
      if @documents_questions_response_type.save
        format.html { redirect_to @documents_questions_response_type, notice: 'Response type was successfully created.' }
        format.json { render :show, status: :created, location: @documents_questions_response_type }
      else
        format.html { render :new }
        format.json { render json: @documents_questions_response_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/questions/response_types/1
  # PATCH/PUT /documents/questions/response_types/1.json
  def update
    respond_to do |format|
      if @documents_questions_response_type.update(documents_questions_response_type_params)
        format.html { redirect_to @documents_questions_response_type, notice: 'Response type was successfully updated.' }
        format.json { render :show, status: :ok, location: @documents_questions_response_type }
      else
        format.html { render :edit }
        format.json { render json: @documents_questions_response_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/questions/response_types/1
  # DELETE /documents/questions/response_types/1.json
  def destroy
    @documents_questions_response_type.destroy
    respond_to do |format|
      format.html { redirect_to documents_questions_response_types_url, notice: 'Response type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_documents_questions_response_type
      @documents_questions_response_type = Documents::Questions::ResponseType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def documents_questions_response_type_params
      params.require(:documents_questions_response_type).permit(:name, :regex)
    end
end
