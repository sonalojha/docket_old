class Orders::DeliveryStatusesController < ApplicationController
  before_action :set_orders_delivery_status, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /orders/delivery_statuses
  # GET /orders/delivery_statuses.json
  def index
    @orders_delivery_statuses = Orders::DeliveryStatus.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /orders/delivery_statuses/1
  # GET /orders/delivery_statuses/1.json
  def show
  end

  # GET /orders/delivery_statuses/new
  def new
    @orders_delivery_status = Orders::DeliveryStatus.new
  end

  # GET /orders/delivery_statuses/1/edit
  def edit
  end

  # POST /orders/delivery_statuses
  # POST /orders/delivery_statuses.json
  def create
    @orders_delivery_status = Orders::DeliveryStatus.new(orders_delivery_status_params)

    respond_to do |format|
      if @orders_delivery_status.save
        format.html { redirect_to @orders_delivery_status, notice: 'Delivery status was successfully created.' }
        format.json { render :show, status: :created, location: @orders_delivery_status }
      else
        format.html { render :new }
        format.json { render json: @orders_delivery_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/delivery_statuses/1
  # PATCH/PUT /orders/delivery_statuses/1.json
  def update
    respond_to do |format|
      if @orders_delivery_status.update(orders_delivery_status_params)
        format.html { redirect_to @orders_delivery_status, notice: 'Delivery status was successfully updated.' }
        format.json { render :show, status: :ok, location: @orders_delivery_status }
      else
        format.html { render :edit }
        format.json { render json: @orders_delivery_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/delivery_statuses/1
  # DELETE /orders/delivery_statuses/1.json
  def destroy
    @orders_delivery_status.destroy
    respond_to do |format|
      format.html { redirect_to orders_delivery_statuses_url, notice: 'Delivery status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orders_delivery_status
      @orders_delivery_status = Orders::DeliveryStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orders_delivery_status_params
      params.require(:orders_delivery_status).permit(:name)
    end
end
