class Orders::DetailsController < ApplicationController
  before_action :set_orders_detail, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  # GET /orders/details
  # GET /orders/details.json
  def index
    @orders_details = Orders::Detail.all
  end

  # GET /orders/details/1
  # GET /orders/details/1.json
  def show
  end

  # GET /orders/details/new
  def new
    @orders_detail = Orders::Detail.new
  end

  # GET /orders/details/1/edit
  def edit
  end

  # POST /orders/details
  # POST /orders/details.json
  def create
    @orders_detail = Orders::Detail.new(orders_detail_params)

    respond_to do |format|
      if @orders_detail.save
        format.html { redirect_to @orders_detail, notice: 'Detail was successfully created.' }
        format.json { render :show, status: :created, location: @orders_detail }
      else
        format.html { render :new }
        format.json { render json: @orders_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/details/1
  # PATCH/PUT /orders/details/1.json
  def update
    respond_to do |format|
      if @orders_detail.update(orders_detail_params)
        format.html { redirect_to @orders_detail, notice: 'Detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @orders_detail }
      else
        format.html { render :edit }
        format.json { render json: @orders_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/details/1
  # DELETE /orders/details/1.json
  def destroy
    @orders_detail.destroy
    respond_to do |format|
      format.html { redirect_to orders_details_url, notice: 'Detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orders_detail
      @orders_detail = Orders::Detail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orders_detail_params
      params.require(:orders_detail).permit(:name, :email, :mobile, :order_id)
    end
end
