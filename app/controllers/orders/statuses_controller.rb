class Orders::StatusesController < ApplicationController
  before_action :set_orders_status, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /orders/statuses
  # GET /orders/statuses.json
  def index
    @orders_statuses = Orders::Status.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /orders/statuses/1
  # GET /orders/statuses/1.json
  def show
  end

  # GET /orders/statuses/new
  def new
    @orders_status = Orders::Status.new
  end

  # GET /orders/statuses/1/edit
  def edit
  end

  # POST /orders/statuses
  # POST /orders/statuses.json
  def create
    @orders_status = Orders::Status.new(orders_status_params)

    respond_to do |format|
      if @orders_status.save
        format.html { redirect_to @orders_status, notice: 'Status was successfully created.' }
        format.json { render :show, status: :created, location: @orders_status }
      else
        format.html { render :new }
        format.json { render json: @orders_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/statuses/1
  # PATCH/PUT /orders/statuses/1.json
  def update
    respond_to do |format|
      if @orders_status.update(orders_status_params)
        format.html { redirect_to @orders_status, notice: 'Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @orders_status }
      else
        format.html { render :edit }
        format.json { render json: @orders_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/statuses/1
  # DELETE /orders/statuses/1.json
  def destroy
    @orders_status.destroy
    respond_to do |format|
      format.html { redirect_to orders_statuses_url, notice: 'Status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orders_status
      @orders_status = Orders::Status.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orders_status_params
      params.require(:orders_status).permit(:name)
    end
end
