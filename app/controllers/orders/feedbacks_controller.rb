class Orders::FeedbacksController < ApplicationController
  before_action :set_orders_feedback, only: [:show, :edit, :update, :destroy]
  before_filter :require_login, only: [:edit, :update, :destroy, :index, :show]
  layout 'employee'
  
  def require_login
    if loggedin_employee.nil?
      @errors = "Please login for this action"
      redirect_to employee_sign_in_path
    end
  end

  def check_permission
    false    
  end
  # GET /orders/feedbacks
  # GET /orders/feedbacks.json
  def index
    @orders_feedbacks = Orders::Feedback.all
  end

  # GET /orders/feedbacks/1
  # GET /orders/feedbacks/1.json
  def show
  end

  # GET /orders/feedbacks/new
  def new
    @orders_feedback = Orders::Feedback.new
  end

  # GET /orders/feedbacks/1/edit
  def edit
  end

  # POST /orders/feedbacks
  # POST /orders/feedbacks.json
  def create
    @orders_feedback = Orders::Feedback.new(orders_feedback_params)

    respond_to do |format|
      if @orders_feedback.save
        format.html { redirect_to @orders_feedback, notice: 'Feedback was successfully created.' }
        format.json { render :show, status: :created, location: @orders_feedback }
      else
        format.html { render :new }
        format.json { render json: @orders_feedback.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/feedbacks/1
  # PATCH/PUT /orders/feedbacks/1.json
  def update
    respond_to do |format|
      if @orders_feedback.update(orders_feedback_params)
        format.html { redirect_to @orders_feedback, notice: 'Feedback was successfully updated.' }
        format.json { render :show, status: :ok, location: @orders_feedback }
      else
        format.html { render :edit }
        format.json { render json: @orders_feedback.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/feedbacks/1
  # DELETE /orders/feedbacks/1.json
  def destroy
    @orders_feedback.destroy
    respond_to do |format|
      format.html { redirect_to orders_feedbacks_url, notice: 'Feedback was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orders_feedback
      @orders_feedback = Orders::Feedback.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orders_feedback_params
      params.require(:orders_feedback).permit(:order_id, :message, :status_id, :note)
    end
end
