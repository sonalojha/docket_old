class Orders::Delivery::PartnersController < ApplicationController
  before_action :set_orders_delivery_partner, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /orders/delivery/partners
  # GET /orders/delivery/partners.json
  def index
    @orders_delivery_partners = Orders::Delivery::Partner.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /orders/delivery/partners/1
  # GET /orders/delivery/partners/1.json
  def show
  end

  # GET /orders/delivery/partners/new
  def new
    @orders_delivery_partner = Orders::Delivery::Partner.new
  end

  # GET /orders/delivery/partners/1/edit
  def edit
  end

  # POST /orders/delivery/partners
  # POST /orders/delivery/partners.json
  def create
    @orders_delivery_partner = Orders::Delivery::Partner.new(orders_delivery_partner_params)

    respond_to do |format|
      if @orders_delivery_partner.save
        format.html { redirect_to @orders_delivery_partner, notice: 'Partner was successfully created.' }
        format.json { render :show, status: :created, location: @orders_delivery_partner }
      else
        format.html { render :new }
        format.json { render json: @orders_delivery_partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/delivery/partners/1
  # PATCH/PUT /orders/delivery/partners/1.json
  def update
    respond_to do |format|
      if @orders_delivery_partner.update(orders_delivery_partner_params)
        format.html { redirect_to @orders_delivery_partner, notice: 'Partner was successfully updated.' }
        format.json { render :show, status: :ok, location: @orders_delivery_partner }
      else
        format.html { render :edit }
        format.json { render json: @orders_delivery_partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/delivery/partners/1
  # DELETE /orders/delivery/partners/1.json
  def destroy
    @orders_delivery_partner.destroy
    respond_to do |format|
      format.html { redirect_to orders_delivery_partners_url, notice: 'Partner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orders_delivery_partner
      @orders_delivery_partner = Orders::Delivery::Partner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orders_delivery_partner_params
      params.require(:orders_delivery_partner).permit(:name)
    end
end
