class Orders::TracksController < ApplicationController
  before_action :set_orders_track, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /orders/tracks
  # GET /orders/tracks.json
  def index
    @orders_tracks = Orders::Track.includes(:order).all.paginate(page: params[:page], per_page: 30)
  end

  # GET /orders/tracks/1
  # GET /orders/tracks/1.json
  def show
  end

  # GET /orders/tracks/new
  def new
    @orders_track = Orders::Track.new
  end

  # GET /orders/tracks/1/edit
  def edit
  end

  # POST /orders/tracks
  # POST /orders/tracks.json
  def create
    @orders_track = Orders::Track.new(orders_track_params)

    respond_to do |format|
      if @orders_track.save
        format.html { redirect_to @orders_track, notice: 'Track was successfully created.' }
        format.json { render :show, status: :created, location: @orders_track }
      else
        format.html { render :new }
        format.json { render json: @orders_track.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/tracks/1
  # PATCH/PUT /orders/tracks/1.json
  def update
    respond_to do |format|
      if @orders_track.update(orders_track_params)
        format.html { redirect_to @orders_track, notice: 'Track was successfully updated.' }
        format.json { render :show, status: :ok, location: @orders_track }
      else
        format.html { render :edit }
        format.json { render json: @orders_track.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/tracks/1
  # DELETE /orders/tracks/1.json
  def destroy
    @orders_track.destroy
    respond_to do |format|
      format.html { redirect_to orders_tracks_url, notice: 'Track was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orders_track
      @orders_track = Orders::Track.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orders_track_params
      params[:orders_track].permit(:verified, :printed, :drafted, :delivered, :signed, :notary, :order_id, :verified_at, :printed_at, :drafted_at, :delivered_at, :signed_at, :notary_at, :created_at, :updated_at, :user_status)
    end
end
