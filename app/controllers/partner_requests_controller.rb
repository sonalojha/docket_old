class PartnerRequestsController < ApplicationController
  before_action :set_partner_request, only: [:show, :edit, :update, :destroy]
  before_filter :require_login, only: [:edit, :update, :destroy, :index, :show]
  layout 'employee'
  
  def require_login
    if loggedin_employee.nil?
      @errors = "Please login for this action"
      redirect_to employee_sign_in_path
    end
  end

  def check_permission
    false    
  end
  # GET /partner_requests
  # GET /partner_requests.json
  def index
    @partner_requests = PartnerRequest.all.paginate(:page => params[:page], :per_page => 30).order('id')
  end

  # GET /partner_requests/1
  # GET /partner_requests/1.json
  def show
  end

  # GET /partner_requests/new
  def new
    @partner_request = PartnerRequest.new
  end

  # GET /partner_requests/1/edit
  def edit
  end

  # POST /partner_requests
  # POST /partner_requests.json
  def create
    # params[:uploads][:cancelled_cheque].path
    @partner_request = PartnerRequest.new(partner_request_params)
    if @partner_request.save
      if !params[:uploads].nil?
        params[:uploads].each do |key, val|
          file = params[:uploads][key.to_sym]
          url = PartnerRequest.upload(file.path)
          @partner_request.attachments.create(s3_url: url, is_processed: true, name: file.original_filename.to_s, mime_type: file.original_filename.split('.').last )
        end
      end
      if params[:redirect] && params[:redirect] == 'true'
        respond_to do |format|
          format.html { redirect_to @partner_request, notice: 'Partner request was successfully created.' }
          format.json { render :show, status: :created, location: @partner_request }
        end
      else
        render json: ::Api::ApiStatusList::OK
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @partner_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partner_requests/1
  # PATCH/PUT /partner_requests/1.json
  def update
    respond_to do |format|
      if @partner_request.update(partner_request_params)
        format.html { redirect_to @partner_request, notice: 'Partner request was successfully updated.' }
        format.json { render :show, status: :ok, location: @partner_request }
      else
        format.html { render :edit }
        format.json { render json: @partner_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partner_requests/1
  # DELETE /partner_requests/1.json
  def destroy
    @partner_request.destroy
    respond_to do |format|
      format.html { redirect_to partner_requests_url, notice: 'Partner request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner_request
      @partner_request = PartnerRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partner_request_params
      params.require(:partner_request).permit(:name, :email, :phone, :profession, :address, :message, 
        :status_id, :note, :callback_request, :degree, :firm_name, :registration_no, :id_proof_type,
        :id_proof_number, :referral, :services, :city, :state, :pincode, :pan, :tan, :service_tax, 
        :account_holder, :account_number, :ifsc_code, :bank_address, :bank_name, :branch_name, :business_category,
        :business_address) 
    end
end
