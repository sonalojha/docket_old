class Notifications::TemplatesController < ApplicationController
  before_action :set_notifications_template, only: [:show, :edit, :update, :destroy]
  layout 'employee'
  # GET /notifications/templates
  # GET /notifications/templates.json
  def index
    @notifications_templates = Notifications::Template.all
  end

  # GET /notifications/templates/1
  # GET /notifications/templates/1.json
  def show
  end

  # GET /notifications/templates/new
  def new
    @notifications_template = Notifications::Template.new
  end

  # GET /notifications/templates/1/edit
  def edit
  end

  # POST /notifications/templates
  # POST /notifications/templates.json
  def create
    @notifications_template = Notifications::Template.new(notifications_template_params)

    respond_to do |format|
      if @notifications_template.save
        format.html { redirect_to @notifications_template, notice: 'Template was successfully created.' }
        format.json { render :show, status: :created, location: @notifications_template }
      else
        format.html { render :new }
        format.json { render json: @notifications_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notifications/templates/1
  # PATCH/PUT /notifications/templates/1.json
  def update
    respond_to do |format|
      if @notifications_template.update(notifications_template_params)
        format.html { redirect_to @notifications_template, notice: 'Template was successfully updated.' }
        format.json { render :show, status: :ok, location: @notifications_template }
      else
        format.html { render :edit }
        format.json { render json: @notifications_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notifications/templates/1
  # DELETE /notifications/templates/1.json
  def destroy
    @notifications_template.destroy
    respond_to do |format|
      format.html { redirect_to notifications_templates_url, notice: 'Template was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notifications_template
      @notifications_template = Notifications::Template.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notifications_template_params
      params.require(:notifications_template).permit(:name, :message)
    end
end
