class WebsiteController < ApplicationController
  before_action :init_variables
  layout 'website'
	include HomeHelper
	def check_permission
		return false
	end

	def check_login
		if current_user
			return true
		else
			redirect_to user_login_path
		end
	end

	##### initialize the following variables ######
	def init_variables
		if Rails.env.production?
		  @include_analytics = true
		elsif Rails.env.development?
		  @include_analytics = false
		end
		@payu = ENV['payu_url']
		@base_url = ENV['base_url']
		@site_state = params[:site_state] || params[:action] 
		@localities = Locality.includes(city: [state: [:country]]).all.order('name ASC')
		@cities = City.includes(state: [:country]).all.order('name ASC')
		@states = State.includes(:country).all.order('name ASC')
		@countries = Country.all.order('name ASC')
		@services = Service.all
		if current_user && (User.exists? current_user)
		  @current_user = User.includes(:addresses, :profile).where(id: current_user).first
		else
			session[:user] = nil
		end
	end

	def index
		@shop = Document.predictions('Establishment').first
 	end


 	def gst_registration
 		@document = Document.where(name: "GST Registration").first
 	end


 	def gst_compliance
 		@document1 = Document.where(name: "Annual Compliances for Private Limited Companies").first rescue nil
		@document2 = Document.where(name: "Annual Filings for LLPs").first rescue nil
		@document3 = Document.where(name: "Increase in Authorized Capital").first rescue nil
		@document4 = Document.where(name: "Adding a Director OR Removing a Director OR Add a Designated Partner").first rescue nil
		@document5 = Document.where(name: "Change Objectives of Your Business").first rescue nil
		@document6 = Document.where(name: "Change Registered Office Address").first rescue nil
		@document7 = Document.where(name: "Change Company Name").first rescue nil
		@document8 = Document.where(name: "Change LLP Agreement").first rescue nil
		@document9 = Document.where(name: "Closure - Private Limited Company").first rescue nil
		@document10 = Document.where(name: "Closure - Limited Liability Partnership").first rescue nil
 		
 	end
end
