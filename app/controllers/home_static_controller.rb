class HomeStaticController < ApplicationController
	# layout 'dvault'

	before_action :init_variables, except: [:reset_password, :reset, :activate]

	def check_permission
		return false
	end

	def init_variables
		if Rails.env.production?
		  @include_analytics = true
		elsif Rails.env.development?
		  @include_analytics = false
		end
		@payu = ENV['payu_url']
		@countries = Country.all
		@states = State.includes(:country).all
		@cities = City.includes(state: [:country]).all
		@localities = Locality.includes(city: [state: [:country]]).all
		@site_state = params[:site_state] || params[:action] 
		@services = Service.all
		if current_user && (User.exists? current_user)
		  @current_user = User.find(current_user)
		else
			session[:user] = nil
		end
		flash[:notice] = nil if flash[:notice].nil?
		flash[:alert] = nil if flash[:alert].nil?
	end

	def login
		if current_user
			redirect_to root_path
		end
	end

	def signup
		@countries = Country.all
		if current_user
			redirect_to root_path
		end
	end
	
	def terms_conditions
		@site_state = 'static'
	end

	def about
		@site_state = 'static'
	end

	def careers
		@site_state = 'static'
	end

	def contact
		@site_state = 'static'
	end

	def clients
		@site_state = 'static'
	end

	def summary
		@site_state = 'static'
	end

	def policies
		@site_state = 'static'
	end

	def terms_of_use
		@site_state = 'static'
	end

	def feedback
		if !current_user
			redirect_to user_login_path
			flash[:alert] = 'Login to give feedback'
		end
		@site_state = 'static'
	end
	
	def summary
		@site_state = 'static'
	end

	def help
		@site_state = 'static'
	end

	def partner_with_us
		@site_state = 'static'
	end

	def services
		@site_state = 'static'
	end
	def services_checklist
		@site_state = 'static'
	end
	def services_dvault
		@site_state = 'static'
	end
	def services_dashboard
		@site_state = 'static'
	end

	def settings
		@site_state = 'settings_dashboard_inner_bg'
	end

	def press
		@site_state = 'static'
	end
	
	def blog
		@site_state = 'static'
	end

	def testimonial
		@site_state = 'static'
	end

	def freelancer
		@site_state = 'static'
	end
	def review
		@site_state = 'static'
	end

	def team
		@site_state = 'static'
	end

	def cancellation_policy
		@site_state = 'static'
	end
	
	def forgot_password
		user = User.where(email: params[:email]).first
		if user 
			@verification = user.verifications.create(verification_type: 'Reset')
		else
			@forgot = true
		end
		render 'activate', layout: false
	end

	def confirm_draft
		@order = Order.where(txnid: params[:txnid]).first
		@site_state = 'confirm_draft'
	end

	def reset
		destroy_user_session
		if params[:hash]
			verification = Verification.where(code: params[:hash], verification_type: 'Reset',status_id: Status.where(name: 'ACTIVE').first.id)
			if verification.length > 0
				@verification = verification.first
				@user = verification.first.user
				render layout: false
			else
				render 'activate', layout: false
			end
		else
			render 'activate', layout: false
		end
	end

	def reset_password
		destroy_user_session
		if params[:user_id] && params[:reset_code]
			user = User.find(params[:user_id])
			verification = Verification.where(user_id: params[:user_id], code: params[:reset_code],status_id: Status.where(name: 'ACTIVE').first.id).first
			if verification && user && (params[:password] == params[:confirm_password]) 
				user.password = params[:password] 
				user.save!
				verification.status_id = Status.where(name: 'INACTIVE').first.id
				verification.save!
				redirect_to user_login_path, notice: 'Your Password has been changed'
			end
		else
			render 'activate', layout: false
		end
	end

	def activate
		@verification = Verification.where(code: params[:hash], verification_type: ['Activation', 'Activate' ],status_id: Status.where(name: 'ACTIVE').first.id).first
		if @verification
			@verification.user.profile.email_verified = true
			@verification.user.profile.save!
			@verification.status_id = Status.where(name: 'INACTIVE').first.id
			@verification.save!
			UserSignupJob.perform_later(@verification.user_id,'activated',nil)
			render layout: false
		else
			redirect_to root_path
		end
	end
end
