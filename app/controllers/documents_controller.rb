class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]
 
  layout 'employee'

  def check_permission
    return true
  end
  # GET /documents
  # GET /documents.json 
  def index
    @documents = Document.all.paginate(:page => params[:page], :per_page => 30).order('name ASC')
    respond_to do |format|  
      format.html
      format.xls do
      send_data(
        Document.joins(:service)
          .select('documents.id, documents.name, services.name as service_name,
                   documents.tat, documents.automated, 
                   documents.default_price, documents.description, documents.priority'
                 ).to_xls, 
        content_type: 'application/vnd.ms-excel',
        filename: 'documents.xls' 
      )
    end
    end
  end
 

  # GET /documents/1
  # GET /documents/1.json
  def show
  end

  #GET/document/search
  def search_by
    if params[:name]
      @documents = Document.predictions(params[:name]).limit(10)
    else
      render json: ::Api::ApiStatusList::INVALID_REQUEST
    end
    render 'documents', layout: false 
  end 

  def search
  end

  def get_questions
    if params[:document_id]
      @document = Document.includes(questions:[:response_type,:options]).where(id: params[:document_id]).first
      @documents_questions = @document.questions
      render partial: 'documents/questions_table'
    else
      render json: ::Api::ApiStatusList::INVALID_REQUEST, status: 404
    end
  end


  # GET /documents/new
  def new
    @document = Document.new
    # @document.user_infos.build
    # @document.requirements.build
    # @user_infos = UserInfo.all
    # @requirements = Requirement.all
  end

  # GET /documents/1/edit
  def edit
    @documents_questions = Documents::Question.where(document_id: @document.id)
  end

  
  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(document_params)

    respond_to do |format|
      if @document.save
        format.html { redirect_to @document, notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @document }
      else
        format.html { render :new }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to @document, notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: @document }
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:name, :service_id, :stamp_required, :default_price,
        :tat, :automated, :template_url, :status_id, :priority, :description,
        :state_requirements_attributes => [:state_id, :requirement_id,:_destroy,:id],
        state_prices_attributes: [:state_id, :price,:_destroy,:id],
        document_user_infos_attributes: [:user_info_id,:_destroy,:id],
        document_requirements_attributes: [:requirement_id,:_destroy,:id])
    end

     
end
