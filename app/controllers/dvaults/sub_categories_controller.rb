class Dvaults::SubCategoriesController < ApplicationController
  before_action :set_dvaults_sub_category, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /dvaults/sub_categories
  # GET /dvaults/sub_categories.json
  def index
    @dvaults_sub_categories = Dvaults::SubCategory.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /dvaults/sub_categories/1
  # GET /dvaults/sub_categories/1.json
  def show
  end

  # GET /dvaults/sub_categories/new
  def new
    @dvaults_sub_category = Dvaults::SubCategory.new
  end

  # GET /dvaults/sub_categories/1/edit
  def edit
  end

  # POST /dvaults/sub_categories
  # POST /dvaults/sub_categories.json
  def create
    @dvaults_sub_category = Dvaults::SubCategory.new(dvaults_sub_category_params)

    respond_to do |format|
      if @dvaults_sub_category.save
        format.html { redirect_to @dvaults_sub_category, notice: 'Sub category was successfully created.' }
        format.json { render :show, status: :created, location: @dvaults_sub_category }
      else
        format.html { render :new }
        format.json { render json: @dvaults_sub_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dvaults/sub_categories/1
  # PATCH/PUT /dvaults/sub_categories/1.json
  def update
    respond_to do |format|
      if @dvaults_sub_category.update(dvaults_sub_category_params)
        format.html { redirect_to @dvaults_sub_category, notice: 'Sub category was successfully updated.' }
        format.json { render :show, status: :ok, location: @dvaults_sub_category }
      else
        format.html { render :edit }
        format.json { render json: @dvaults_sub_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dvaults/sub_categories/1
  # DELETE /dvaults/sub_categories/1.json
  def destroy
    @dvaults_sub_category.destroy
    respond_to do |format|
      format.html { redirect_to dvaults_sub_categories_url, notice: 'Sub category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dvaults_sub_category
      @dvaults_sub_category = Dvaults::SubCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dvaults_sub_category_params
      params.require(:dvaults_sub_category).permit(:name, :category_id)
    end
end
