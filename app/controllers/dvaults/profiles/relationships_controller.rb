class Dvaults::Profiles::RelationshipsController < ApplicationController
  before_action :set_dvaults_profiles_relationship, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /dvaults/profiles/relationships
  # GET /dvaults/profiles/relationships.json
  def index
    @dvaults_profiles_relationships = Dvaults::Profiles::Relationship.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /dvaults/profiles/relationships/1
  # GET /dvaults/profiles/relationships/1.json
  def show
  end

  # GET /dvaults/profiles/relationships/new
  def new
    @dvaults_profiles_relationship = Dvaults::Profiles::Relationship.new
  end

  # GET /dvaults/profiles/relationships/1/edit
  def edit
  end

  # POST /dvaults/profiles/relationships
  # POST /dvaults/profiles/relationships.json
  def create
    @dvaults_profiles_relationship = Dvaults::Profiles::Relationship.new(dvaults_profiles_relationship_params)

    respond_to do |format|
      if @dvaults_profiles_relationship.save
        format.html { redirect_to @dvaults_profiles_relationship, notice: 'Relationship was successfully created.' }
        format.json { render :show, status: :created, location: @dvaults_profiles_relationship }
      else
        format.html { render :new }
        format.json { render json: @dvaults_profiles_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dvaults/profiles/relationships/1
  # PATCH/PUT /dvaults/profiles/relationships/1.json
  def update
    respond_to do |format|
      if @dvaults_profiles_relationship.update(dvaults_profiles_relationship_params)
        format.html { redirect_to @dvaults_profiles_relationship, notice: 'Relationship was successfully updated.' }
        format.json { render :show, status: :ok, location: @dvaults_profiles_relationship }
      else
        format.html { render :edit }
        format.json { render json: @dvaults_profiles_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dvaults/profiles/relationships/1
  # DELETE /dvaults/profiles/relationships/1.json
  def destroy
    @dvaults_profiles_relationship.destroy
    respond_to do |format|
      format.html { redirect_to dvaults_profiles_relationships_url, notice: 'Relationship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dvaults_profiles_relationship
      @dvaults_profiles_relationship = Dvaults::Profiles::Relationship.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dvaults_profiles_relationship_params
      params.require(:dvaults_profiles_relationship).permit(:name)
    end
end
