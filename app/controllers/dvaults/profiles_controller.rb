class Dvaults::ProfilesController < ApplicationController
  before_action :set_dvaults_profile, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /dvaults/profiles
  # GET /dvaults/profiles.json
  def index
    @dvaults_profiles = Dvaults::Profile.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /dvaults/profiles/1
  # GET /dvaults/profiles/1.json
  def show
  end

  # GET /dvaults/profiles/new
  def new
    @dvaults_profile = Dvaults::Profile.new
  end

  # GET /dvaults/profiles/1/edit
  def edit
  end

  # POST /dvaults/profiles
  # POST /dvaults/profiles.json
  def create
    @dvaults_profile = Dvaults::Profile.new(dvaults_profile_params)

    respond_to do |format|
      if @dvaults_profile.save
        format.html { redirect_to @dvaults_profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @dvaults_profile }
      else
        format.html { render :new }
        format.json { render json: @dvaults_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dvaults/profiles/1
  # PATCH/PUT /dvaults/profiles/1.json
  def update
    respond_to do |format|
      if @dvaults_profile.update(dvaults_profile_params)
        format.html { redirect_to @dvaults_profile, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @dvaults_profile }
      else
        format.html { render :edit }
        format.json { render json: @dvaults_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dvaults/profiles/1
  # DELETE /dvaults/profiles/1.json
  def destroy
    @dvaults_profile.destroy
    respond_to do |format|
      format.html { redirect_to dvaults_profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dvaults_profile
      @dvaults_profile = Dvaults::Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dvaults_profile_params
      params.require(:dvaults_profile).permit(:name, :dvault_id, :relationship_id)
    end
end
