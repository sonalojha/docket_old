class Dvaults::CategoriesController < ApplicationController
  before_action :set_dvaults_category, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /dvaults/categories
  # GET /dvaults/categories.json
  def index
    @dvaults_categories = Dvaults::Category.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /dvaults/categories/1
  # GET /dvaults/categories/1.json
  def show
  end

  # GET /dvaults/categories/new
  def new
    @dvaults_category = Dvaults::Category.new
  end

  # GET /dvaults/categories/1/edit
  def edit
  end

  # POST /dvaults/categories
  # POST /dvaults/categories.json
  def create
    @dvaults_category = Dvaults::Category.new(dvaults_category_params)

    respond_to do |format|
      if @dvaults_category.save
        format.html { redirect_to @dvaults_category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @dvaults_category }
      else
        format.html { render :new }
        format.json { render json: @dvaults_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dvaults/categories/1
  # PATCH/PUT /dvaults/categories/1.json
  def update
    respond_to do |format|
      if @dvaults_category.update(dvaults_category_params)
        format.html { redirect_to @dvaults_category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @dvaults_category }
      else
        format.html { render :edit }
        format.json { render json: @dvaults_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dvaults/categories/1
  # DELETE /dvaults/categories/1.json
  def destroy
    @dvaults_category.destroy
    respond_to do |format|
      format.html { redirect_to dvaults_categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dvaults_category
      @dvaults_category = Dvaults::Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dvaults_category_params
      params.require(:dvaults_category).permit(:name)
    end
end
