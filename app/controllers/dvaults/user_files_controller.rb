class Dvaults::UserFilesController < ApplicationController
  before_action :set_dvaults_user_file, only: [:show, :edit, :update, :destroy]
  layout 'employee'

  # GET /dvaults/user_files
  # GET /dvaults/user_files.json
  def index
    @dvaults_user_files = Dvaults::UserFile.all.paginate(:page => params[:page], :per_page => 30)
  end

  # GET /dvaults/user_files/1
  # GET /dvaults/user_files/1.json
  def show
  end

  # GET /dvaults/user_files/new
  def new
    @dvaults_user_file = Dvaults::UserFile.new
  end

  # GET /dvaults/user_files/1/edit
  def edit
  end

  # POST /dvaults/user_files
  # POST /dvaults/user_files.json
  def create
    @dvaults_user_file = Dvaults::UserFile.new(dvaults_user_file_params)

    respond_to do |format|
      if @dvaults_user_file.save
        format.html { redirect_to @dvaults_user_file, notice: 'User file was successfully created.' }
        format.json { render :show, status: :created, location: @dvaults_user_file }
      else
        format.html { render :new }
        format.json { render json: @dvaults_user_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dvaults/user_files/1
  # PATCH/PUT /dvaults/user_files/1.json
  def update
    respond_to do |format|
      if @dvaults_user_file.update(dvaults_user_file_params)
        format.html { redirect_to @dvaults_user_file, notice: 'User file was successfully updated.' }
        format.json { render :show, status: :ok, location: @dvaults_user_file }
      else
        format.html { render :edit }
        format.json { render json: @dvaults_user_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dvaults/user_files/1
  # DELETE /dvaults/user_files/1.json
  def destroy
    @dvaults_user_file.destroy
    respond_to do |format|
      format.html { redirect_to dvaults_user_files_url, notice: 'User file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dvaults_user_file
      @dvaults_user_file = Dvaults::UserFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dvaults_user_file_params
      params.require(:dvaults_user_file).permit(:filename, :tag, :mime_type, :vault_id, :size, :profile_id, :dvault_id, :thumbnail_image, :sub_category_id)
    end
end
