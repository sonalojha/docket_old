class UserMailer < ApplicationMailer
	default :from => ENV['email_user']

	def signup_email(user,activation_link)
	  	@user = User.find(user)
	   	@base_url = ENV['base_url']
	   	@url = activation_link
	  	mail( :to => @user.email, bcc: '', :subject => 'Welcome to Docket' )
	  	puts 'Signup email sent'
	end

	def welcome_email(user,activation_link)
	  	@user = User.find(user)
	   	@base_url = ENV['base_url']
	   	@url = activation_link
	    render 'welcome_email'
	  	mail( :to => @user.email, bcc: '', :subject => 'Welcome to Docket' )
	  	puts 'Welcome mail sent'
	end
	
	def password_reset(user,activation_link)
	   @base_url = ENV['base_url']
	   @user = User.find(user)
	   @url = activation_link
	   mail(to: @user.email, bcc: '', :subject => "Docket Account Password Reset")
	   puts 'Email password reset mail sent'
	end

	def activation_email(user)
	   @base_url = ENV['base_url']
	   @user = User.find(user)
	   mail(to: @user.email, bcc: '', :subject => "Docket Email Verified")
	   puts 'Email Activated mail sent'

	end

	def activate_email(user,url)
	   @base_url = ENV['base_url']
	   @user = User.find(user)
	   @url = url
	   mail(to: @user.email, bcc: '', :subject => "Verify Your Docket Email")
	   puts 'Email verification mail sent'

	end

	def verify_account(user)
	   @base_url = ENV['base_url']
	   @user = User.find(user)
	   mail(to: @user.email, bcc: '', :subject => "Verify Your Docket Account")
		puts 'Verify mail sent'
	end

	def send_file user_id,email,file_ids
		@user = User.find(user_id)
		files = Dvaults::UserFile.where(id: file_ids.split(','))
		files.each do |file|
			attachments["#{file.filename+ '.' +file.mime_type}"] = open("#{file.vault_uid}",'rb') {|f| f.read }
		end
		mail(to: email, bcc: '', :subject => "Your Documents from Docket DVault")
		puts 'dvault file mail sent'
	end

	def verify_otp user_id,code
		@code = code
		@user = User.find(user_id)
		mail(to: @user.email, bcc: '', :subject => "Your OTP For Docket")
		puts 'otp mail sent'
	end

	def pin_reset user_id,code
		@code = code
		@user = User.find(user_id)
		mail(to: @user.email, :subject => "Your OTP For Docket DVault")
		puts 'otp mail sent'
	end

	def dvault_email user_id
		@user = User.find(user_id)
		@base_url = 'http://edocketapp.com' || ENV['base_url']
		mail(to: @user.email, bcc: '', :subject => "Welcome to Docket DVault")
		puts 'dvault welcome mail sent'
	end
end