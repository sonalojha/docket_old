class OrderMailer < ApplicationMailer
	default :from => ENV['email_user']

	def order_invoice(order_id)
		@base_url = ENV['email_base_url']
		@order = Order.includes(:status, :user, payment: [:status]).where(id: order_id).first
		email = @order.detail ? @order.detail.email : @order.user.email
		if (['Notary', 'Attestation','Franking'].include? @order.service.name)
			@no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i 
		else
			@no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i + 1
		end
		mail( :to => email,
			:cc => '', :bcc => "#{ENV['email_bcc']},#{ENV['sales_email']}", :subject => 'Your Payment has been Received' )
		puts "Invoice Sent #{order_id}"
	end

	def order_dispatched(order_id)
		@base_url = ENV['base_url']
		@order = Order.includes(:status, :user, payment: [:status]).where(id: order_id).first
		mail( :to => @order.user.email,
		:cc => '', :bcc => ENV['email_bcc'], :subject => "Order Dispatched - Your Order No. #{@order.txnid} has been successfully shipped out by dockettech.com" )
	end

	####### url for authenticating and confirming order document ####
	def order_template(order_id,path,url)
		# if ENV['base_url'] == 'http://edocketapp.com' 
			# @url = ENV['base_url'] +  '/dashboard' 
		# else
			@url = url
		# end
		@order = Order.where(id: order_id).first
		@user = @order.user
		attachments["#{@order.document.name}.pdf"] = open("#{path.to_s}") {|f| f.read } #s3.get_object(bucket_name:ENV['S3_BUCKET'], key:'Acknowledgment_To_Save_LimitationDT201601027.docx')
		email = @order.detail ? @order.detail.email : @order.user.email
		mail( :to => email, :bcc => ENV['email_bcc'], :subject => 'Please confirm the document!' )
		puts "Template Sent #{order_id}"
	end

	def error_template(order_id,path)
		@order = Order.where(id: order_id).first
		attachments["#{@order.document.name}.docx"] = open("#{path.to_s}") {|f| f.read } #s3.get_object(bucket_name:ENV['S3_BUCKET'], key:'Acknowledgment_To_Save_LimitationDT201601027.docx')
		mail( :to => ENV['email_bcc'], :subject => 'Please correct the draft ASAP' )
		puts "Error In Draft #{order_id}"
	end

	def generate_invoice(order_id)
		@base_url = ENV['base_url']
		@order = Order.includes(:status, :user, payment: [:status]).where(id: order_id).first
		if (['Notary', 'Attestation','Franking'].include? @order.service.name)
			@no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i 
		else
			@no_of_copies = ((@order.answers.where(question_id: [37,15]).length > 0) ? @order.answers.where(question_id: [37,15]).first.body : 0).to_i + 1
		end
		mail( :to => ENV['email_bcc'],
			:cc => '', :bcc => ENV['email_bcc'], :subject => 'Your Payment has been Received' )
		puts 'invoice sent'
	end

	def error_mailer(order_id,body)
		@body = body
		@order = Order.where(id: order_id).first
		mail( :to => @order.user.email, :bcc => ENV['email_bcc'], :subject => "Regarding you order no #{@order.txnid}")
		puts "Template Sent #{order_id}"
	end

	def thankyou_email(order_id)
		@order = Order.where(id: order_id).first
		@base_url = ENV['base_url']
		email = @order.detail ? @order.detail.email : @order.user.email
		mail( :to => email, :bcc => ENV['email_bcc'], :subject => "Thankyou For ordering #{@order.product} with Docket")
		puts "Thankyou email sent #{order_id}"
	end
end