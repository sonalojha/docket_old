class DocumentPreview < ApplicationMailer
	default :from => ENV['email_user'],
					:content_type => 'multipart/alternative',
            		:parts_order => [ "text/html", "text/enriched", "text/plain", "application/pdf", 
            							'application/docx', 'application/doc' ]

	def order_template(path=nil)
		attachments['test.docx'] = open('https://s3-ap-southeast-1.amazonaws.com/notarymama-dev/template1.docx') {|f| f.read }
	  	mail( :to => ('technology@notarymama.com'),
	  		:cc => '', :bcc => ENV['email_bcc'], :subject => 'Please confirm the document!' )
	end
end
