class SalesMailer < ApplicationMailer
	default :from => ENV['email_user'],
					:content_type => 'multipart/alternative',
            		:parts_order => [ "text/html", "text/enriched", "text/plain", "application/pdf", 
            							'application/docx', 'application/doc' ]

	def new_request(id)
		@request = ContactRequest.find(id)
		if @request.request_type.downcase == 'careers' 
		  	mail( :to => ENV['careers_email'],
		  		:cc => '', :bcc => ENV['email_bcc'], :subject => "New Careers Request - #{@request.name} " )
		else
			mail( :to => ENV['sales_email'],
		  		:cc => '', :bcc => ENV['email_bcc'], :subject => "New Contact Request - #{@request.name} " )
		end

	end
end
