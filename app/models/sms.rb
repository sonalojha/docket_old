class Sms
	def self.send_msg(id,template,code,msg)
    user = self.find(id)
    vars = {'name' => user.name,
            'phone' => user.phone,
            'email' => user.email,
            'code' => code,
            'msg' => msg
        }
    response = Net::HTTP.post_form( URI('http://localhost:3001/api/v0/send_sms'),
      "template"=> 'send_otp',
      "key"=>'docket_sms',
      "high_priority" => true,
      'data' => vars.to_json
    )
    response.body
  end
end
