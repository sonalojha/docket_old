class Address < ActiveRecord::Base
	belongs_to :user
	# validates :tag, presence: true
	after_create :set_status
	belongs_to :city
	belongs_to :locality
	has_many :order, class_name: "::Order"

	def set_status
		self.status_id = Status.where(name: 'ACTIVE').first.id
		self.save
	end
end
