class Notifications::Template < ActiveRecord::Base
	has_many :notifications, class_name: "Notification"
end
