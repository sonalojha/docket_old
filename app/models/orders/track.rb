class Orders::Track < ActiveRecord::Base
	validates :order_id, presence: true
	belongs_to :order, class_name: "::Order"
end
