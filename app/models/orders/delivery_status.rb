class Orders::DeliveryStatus < ActiveRecord::Base
	has_many :orders, class_name: '::Order'
end
