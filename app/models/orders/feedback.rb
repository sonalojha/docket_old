class Orders::Feedback < ActiveRecord::Base
	belongs_to :order, class_name: '::Order'
	validates :order_id, presence: true
end
