class Orders::Delivery::Partner < ActiveRecord::Base
	has_many :orders, class_name: '::Order'
end
