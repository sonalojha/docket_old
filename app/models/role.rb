class Role < ActiveRecord::Base
	validates :name, :role_type, :presence => true
  
  has_and_belongs_to_many :users, :join_table => "users_roles"
  has_many :role_column_accesses
  has_and_belongs_to_many :resources, :join_table => "permissions"
  
  has_many :children, :class_name => "Role", :foreign_key => "parent_id"
  belongs_to :parent, :class_name => "Role"

  def delete_column_accesses(table_name, column_name, cascade)
    if cascade
      Role.find_by_sql('WITH RECURSIVE children AS ( select * from roles where id = '+self.id.to_s+' UNION ALL select roles.* from roles,children where roles.parent_id = children.id) select * from children').each do |role|
        if column_name == '*'
          role.role_column_accesses.where(:table_name => table_name).delete_all
        else
          role.role_column_accesses.where(:table_name => table_name, :column_name => column_name).delete_all
        end
      end
    else
      if column_name == '*'
          self.role_column_accesses.where(:table_name => table_name).delete_all
        else
          self.role_column_accesses.where(:table_name => table_name, :column_name => column_name).delete_all
        end
    end
  end

  def get_children
    return Role.find_by_sql('WITH RECURSIVE children AS ( select * from roles where id = '+self.id.to_s+' UNION ALL select roles.* from roles,children where roles.parent_id = children.id) select * from children')
  end

  def has_resource?(resource)
    return ((resources and resource.get_parents).count > 0) ? true : false
  end

  def add_resource(resource_id)
    resource = Resource.where(:id => resource_id).first
    return false if resource.nil?
    return false if has_resource?(resource)
    self.resources.delete(resource.get_children)
    self.resources.push(resource)
  end

  def delete_resource(resource, cascade)
    if cascade
      Role.find_by_sql('WITH RECURSIVE children AS ( select * from roles where id = '+self.id.to_s+' UNION ALL select roles.* from roles,children where roles.parent_id = children.id) select * from children').each do |role|
        role.resources.delete(resource.get_children)
      end
    else
      self.resources.delete(resource.get_children)
    end
  end
end
