class Services::Orders::Answer < ActiveRecord::Base
	belongs_to :order, class_name: '::Order'
	belongs_to :question, class_name: '::Services::Question'
end
