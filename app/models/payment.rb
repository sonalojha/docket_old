class Payment < ActiveRecord::Base
	belongs_to :order, class_name: '::Order'
	belongs_to :status, class_name: '::Payments::Status'

	def self.generate_paytm_hash_new order_id,type='wap'
		params = ActiveSupport::HashWithIndifferentAccess.new
		order = Order.find(order_id)
		params = {
			'MID': ENV['paytm_mid'],
			'ORDER_ID': order.txnid,
			'CUST_ID': order.user_id,
			'TXN_AMOUNT': (order.final_amount.to_f - order.discount.to_f*1.18).round(2).to_s,
			'CHANNEL_ID':  type.upcase,
			'INDUSTRY_TYPE_ID': ENV['industry_id'],
			'WEBSITE': "#{'Docket'}" + "#{type}",
			'MOBILE_NO': order.user.phone,
			'EMAIL': order.user.email,
			'CALLBACK_URL': ENV['paytm_app_url']
		}
		hash = generate_hash params.stringify_keys
		# order.checksumhash = hash
		# order.save!
		return hash 
	end


	def self.generate_paytm_hash order_id,type='web'
		params = ActiveSupport::HashWithIndifferentAccess.new
		order = Order.find(order_id)
		params = {
			# 'REQUEST_TYPE': 'DEFAULT',
			'MID': ENV['paytm_mid'],
			'ORDER_ID': order.txnid,
			'CUST_ID': order.user_id,
			'TXN_AMOUNT': (order.final_amount.to_f - order.discount.to_f*1.18).round(2).to_s,
			'CHANNEL_ID':  type.upcase,
			'INDUSTRY_TYPE_ID': ENV['industry_id'],
			'WEBSITE': "#{'Docket' + type}",
			'MOBILE_NO': order.detail.mobile,
			'EMAIL': order.detail.email,
			'CALLBACK_URL': ENV['paytm_callback']
		}
		hash = generate_hash params.stringify_keys
		params['CHECKSUMHASH'] = hash
		uri = URI(ENV['paytm_server'])
		uri.query = URI.encode_www_form(params)
		return uri
	end

	def self.generate_hash paytmHASH
		hash = Paytm::ChecksumTool.new.get_checksum_hash(paytmHASH).gsub("\n",'')
	end

	def self.generate_paytm_hash_verify params
		hash = Paytm::ChecksumTool.new.get_checksum_verified_array(params)
	end

	def payment_url params
		uri = URI(ENV['paytm_server'])
		uri.query = URI.encode_www_form(params)
		res = Net::HTTP.get_response(uri)
	end   
end
 