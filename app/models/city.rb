class City < ActiveRecord::Base
	belongs_to :state
	has_many :locality
	has_many :users
	has_many :addresses
	before_create :set_status

	def set_status
		self.status_id = Status.where(name: 'ACTIVE').first.id
	end
	def self.predictions keyword
		self.where("lower(name) like ?", "%#{keyword.to_s.downcase}%")
	end

end
