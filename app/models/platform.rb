class Platform < ActiveRecord::Base
	has_many :orders
	has_many :device_registrations
end
