class Coupon < ActiveRecord::Base
	has_many :orders
	validates :code, presence: true, uniqueness: true
	validates :coupon_type, presence: true
	before_create :set_attributes

	def set_attributes
		self.code = self.code.upcase
	end

	def self.validate_coupon code
		coupon = self.where(code: code.upcase)
		if coupon.length > 0
			if check_authenticity(coupon.first)
				return coupon.first.id
			else
				return false
			end
		else
			return false
		end
	end

	def self.get_discount_amount id,order_id,total_amount=nil
		coupon = self.where(id: id).first
		discount_amount coupon,order_id,total_amount
	end

	def self.discount_amount coupon,order_id,total_amount=nil
		if coupon.coupon_type == 'fixed'
			discount = coupon.value
		elsif coupon.coupon_type == 'percentage'
			if order_id
				order = Order.find(order_id)
				discount = order.total_amount.to_f / coupon.value.to_f
			else
				discount = total_amount.to_f * (coupon.value.to_f/100)
			end
		end
		return discount
	end

	def self.check_authenticity coupon
		if coupon.active && check_limit(coupon) && check_validity(coupon)
			true
		else
			false
		end
	end

	def self.check_validity coupon
		if coupon.end_date >= Date.today
			return true
		else
			return false
		end
	end

	def self.update_count id
		coupon = self.find(id)
		coupon.count += 1 
		coupon.save!
	end

	def self.check_limit coupon
		if coupon.limit == 0
			return true
		elsif coupon.limit > coupon.count.to_i
			return true
		else
			return false
		end
	end
end
