class Documents::Questions::ResponseType < ActiveRecord::Base
	has_many :questions, class_name: "::Documents::Question"
end
