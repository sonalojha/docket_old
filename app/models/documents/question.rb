class Documents::Question < ActiveRecord::Base
	# default_scope { order('question_number ASC') } 

	has_many :options, class_name: "::Documents::Questions::Option"
	has_many :answer, class_name: "::Documents::Answer"
	has_many :groups, class_name: '::Documents::Sections::QuestionGroup'
	has_many :sections, through: :groups
	belongs_to :response_type, class_name: "::Documents::Questions::ResponseType"
	belongs_to :document, class_name: "::Document"
	# belongs_to :group, class_name: "::Documents::Questions::QuestionGroup"
	# belongs_to :status, class_name: '::Status'
end
