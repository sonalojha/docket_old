class Documents::Answer < ActiveRecord::Base
	belongs_to :question, class_name: "::Documents::Question"
	belongs_to :order, class_name: "::Document"
end
