class Documents::Sections::QuestionGroup < ActiveRecord::Base
	default_scope { order('priority ASC') } 
	belongs_to :question, class_name: "::Documents::Question"
	belongs_to :section, class_name: "::Documents::Section"
end
