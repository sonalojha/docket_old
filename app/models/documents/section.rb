class Documents::Section < ActiveRecord::Base
	default_scope { order('section_no ASC') } 

	has_many :groups, class_name: '::Documents::Sections::QuestionGroup'
	has_many :questions, through: :groups
	belongs_to :document, class_name: "::Document"
end
