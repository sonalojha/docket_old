class Payments::Status < ActiveRecord::Base
	has_many :payments, class_name: '::Payment'
end
