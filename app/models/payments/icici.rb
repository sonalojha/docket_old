class Payments::Icici < ActiveRecord::Base
	############ ICICI PAYMENT GATEWAY ##############
	# def icici_payment_url params
	# 	uri = URI(ENV['icici_server'])
	# 	uri.query = URI.encode_www_form(params)
	# 	res = Net::HTTP.get_response(uri)
	# end
	def self.getDateTime
		@date = Time.now.strftime("%Y:%m:%d-%H:%M:%S")
		return @date
   
	end
	
	def self.createHash(chargetotal, currency)
		@storeId = ENV['ICICI_STORE_ID']
		@sharedSecret = ENV["ICICI_STORE_SECRET"]
		@stringToHash = @storeId + getDateTime + chargetotal + currency + @sharedSecret
		@hash = @stringToHash.unpack('H*').first
		@lasthast = Digest::SHA1.hexdigest @hash
		return @lasthast
	end

	def self.icici_payment order_id
		@order = Order.find(order_id)
		params = {
			timezone: 'IST',
			authenticateTransaction: 'true',
			txntype: 'sale',
			txndatetime: Time.now.strftime("%Y:%m:%d-%H:%M:%S"),
			hash: createHash((@order.final_amount.to_f - @order.discount.to_f*1.18).round(2).to_s, '356'),
			currency: '356',
			mode: 'payonly',
			txnid: @order.txnid,
			storename: ENV['ICICI_STORE_ID'],
			chargetotal: (@order.final_amount.to_f - @order.discount.to_f*1.18).round(2).to_s,
			sharedsecret: ENV["ICICI_STORE_SECRET"],
			language: 'en_EN',
			responseSuccessURL: ENV['ICICI_SUCCESS_URL'],
			responseFailURL: ENV['ICICI_FAIL_URL'],
			hash_algorithm: 'SHA1'
		}
		return params
		# uri = URI(ENV['ICICI_SERVER'])
		# uri.query = URI.encode_www_form(params)
		# return uri
		# res = Net::HTTP.get_response(uri)
	end
end
