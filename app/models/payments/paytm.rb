class Payments::Paytm < ActiveRecord::Base
	# def self.check_paytm_status txnid
	# 	order = Order.where(txnid: txnid).first
	# 	url = URI("https://pguat.paytm.com/oltp/HANDLER_INTERNAL/TXNSTATUS?JsonData={%22MID%22:%22#{ENV['paytm_mid']}%22,%22ORDERID%22:%22#{order.txnid}%22}")
	# 	res = Net::HTTP.get_response(url)
	# 	response = JSON.parse(res.body)
	# 	if (response['STATUS'] == 'TXN_SUCCESS') && (response["TXNAMOUNT"].to_i == order.total_amount.to_i) && (response["RESPCODE"] == '01')
	# 		return true
	# 	else
	# 		return false
	# 	end
	# end

	def self.check_paytm_status txnid
		params = {
			'MID': ENV['paytm_mid'],
			'ORDER_ID': txnid,
		} 
		hash = Payment.generate_hash params.stringify_keys 
		order = ::Order.where(txnid: txnid).first
		checksumhash = CGI.escape(hash)
		url = URI("#{ENV['paytm_txn_status_url']}?JsonData={%22MID%22:%22#{ENV['paytm_mid']}%22,%22ORDERID%22:%22#{order.txnid}%22,%22CHECKSUMHASH%22=%22#{checksumhash}%22}")
		res = Net::HTTP.get_response(url)
		response = JSON.parse(res.body) 
		response.delete("TXNTYPE")
		response.delete("REFUNDAMT")
		transaction = save_paytm(response,checksumhash,order.id)
		if (response['STATUS'] == 'TXN_SUCCESS') && (response["TXNAMOUNT"].to_i == order.final_amount.to_i) && (response["RESPCODE"] == '01')
			return true
		elsif (response['STATUS'] == 'PENDING') && (response["TXNAMOUNT"].to_i == order.final_amount.to_i) 
			return true
		else
			return false
		end
	end


	def self.save_paytm(response,checksumhash,order_id)
		paytm_response = Payments::Paytm.new(response)
		paytm_response.MID = ENV['paytm_mid']
		paytm_response.CHECKSUMHASH = checksumhash
		paytm_response.save! 
		payment = Payment.find_or_initialize_by(txnid: response['ORDERID'],
						amount: response['TXNAMOUNT'],
						mihpayid: response['TXNID'],
						pg_type: response['GATEWAYNAME'],
						bankcode: response['BANKNAME'],
						mode: response['PAYMENTMODE'], 
						secret_hash: response['CHECKSUMHASH'],
						order_id: order_id
					)
		if payment.save!
			if response['RESPCODE'] == '01'
				payment.status_id = Payments::Status.where(name: 'SUCCESS').first.id
				::Order.update_status(response['ORDERID'],'success')
			else
				payment.status_id = Payments::Status.where(name: 'FAILED').first.id
			end
			payment.save!
		end
	end 
end
