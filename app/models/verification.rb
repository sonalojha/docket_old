class Verification < ActiveRecord::Base
	belongs_to :user
	after_create :create_code
	belongs_to :status
	protected
	
	def generate_token
      token = SecureRandom.urlsafe_base64
	end

	def generate_otp
		otp = ((0..9).to_a.shuffle[0,5]).shuffle.join
	end

	def set_status
		self.status_id = Status.where(name: 'ACTIVE').first.id
	end
	
	def create_code
		if self.verification_type == 'Activation'
			self.code = generate_token
			self.valid_till = (Time.now + 2.day)
			activation_link = ENV['base_url'] + '/activation/' + self.code
			UserSignupJob.perform_later(self.user_id,'signup',activation_link)
		elsif self.verification_type == 'Reset'
			self.code = generate_token
			self.valid_till = (Time.now + 2.hour)
			activation_link = ENV['base_url'] + '/reset/' + self.code
			UserSignupJob.perform_later(self.user_id,'password',activation_link)
		elsif self.verification_type == 'Otp'
			self.code = generate_otp
			self.valid_till = (Time.now + 30.minute)
			SmsJob.perform_later(self.user_id,self.code,'sms_verify',nil)
			UserSignupJob.perform_now(self.user_id,'otp',self.code)
		elsif self.verification_type == 'Login'
			self.code = generate_otp
			self.valid_till = (Time.now + 30.minute)
			SmsJob.perform_now(self.user_id,self.code,'sms_verify',nil)
			UserSignupJob.perform_now(self.user_id,'otp',self.code)
		elsif self.verification_type == 'Draft'
			self.code = generate_token
			self.valid_till = (Time.now + 7.day)
			# activation_link = ENV['base_url'] + '/confirm/draft/' + self.code
			# OrderJob.perform_later(self.user_id)
		elsif self.verification_type == 'Activate'
			self.code = generate_token
			self.valid_till = (Time.now + 2.day)
			activation_link = ENV['base_url'] + '/activation/' + self.code
			UserSignupJob.perform_later(self.user_id,'email_verify',activation_link)
		elsif self.verification_type == 'Pin::Reset'
			self.code = generate_otp
			self.valid_till = (Time.now + 15.minute)
			SmsJob.perform_later(self.user_id,self.code,'sms_verify',nil)
			UserSignupJob.perform_later(self.user_id,'pin_reset',self.code)
		end
		set_status
		self.save!
	end
end