class Attachment < ActiveRecord::Base
	belongs_to :file, polymorphic: true
	mount_uploader :attachment, FileUploader
end
