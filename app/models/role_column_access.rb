class RoleColumnAccess < ActiveRecord::Base
	validates :role_id, :table_name, :column_name, :presence => true
	belongs_to :role
end
