class Promotion < ActiveRecord::Base
	validates :name, presence: true
	before_create :check_validity

	def self.check_validity id
		promotion = self.where(id: id)
						.where('valid_by > ?', DateTime.now)
		if promotion && (promotion.length == 1)
			return true
		else
			return false
		end
	end

	def self.valid_offers
		self.where('valid_by > ?', DateTime.now)
	end
end