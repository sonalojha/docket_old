class Locality < ActiveRecord::Base
	belongs_to :city
	has_many :addresses
	def self.predictions keyword
		self.where("lower(name) like ?", "%#{keyword.to_s.downcase}%")
	end

end
