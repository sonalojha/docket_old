class User < ActiveRecord::Base
	before_create :create_and_update_password
  before_update :update_password
  validates :email, :presence => true, :uniqueness => true#, email: true
  # validates :phone, :presence => true, :uniqueness => true
  has_many :addresses
  has_many :orders, class_name: '::Order'
  has_one :dvault, class_name: "::Dvault"
  has_many :notifications
  has_many :verifications
  has_many :devices, class_name: "DeviceRegistration"
  has_and_belongs_to_many :roles, :join_table => "users_roles"
  has_many :children, :class_name => "User", :foreign_key => "parent_id"
  has_many :attachments, as: :file
  belongs_to :parent, :class_name => "User"
  belongs_to :city
  belongs_to :profile, polymorphic: true, dependent: :destroy
  after_create :send_email

  def self.check_user_status detail
    # if !self.order.user
      users = User.where(email: detail.email, profile_type: 'Users::Profile')
      if users.length != 0
        # self.order.user_id = users.first.id
        # self.order.save!
        user = users.first
      else
        user = User.new(email: detail.email, phone: detail.mobile, name: detail.name, profile_type: 'Users::Profile')
        user.save!
        profile = Users::Profile.new(first_name: detail.name.split(' ').first, last_name: detail.name.split(' ').last)
        profile.country_id = Country.where(name: 'India').first.id
        profile.save!
        user.profile_id = profile.id
        user.save!
      end
    # end
    return user
  end

  def self.check_valid email,mobile
    mobile_users = User.where(phone: mobile, profile_type: "Users::Profile")
    email_users = User.where(email: email, profile_type: "Users::Profile")
    if (mobile_users.length == email_users.length) && (mobile_users.length == 0)
      return true
    else
      return false
    end
  end
  #### On create user send email to non-employee ####
  def send_email
    if self.profile_type != ('Employee')
      self.verifications.create(verification_type: 'Activation')
    end
  end
  ##### Upload File with hashing ######
  def self.upload path,extension
    s3 = AWS::S3.new
    key = SecureRandom.urlsafe_base64 32
    obj = s3.buckets[ENV['S3_BUCKET']].objects[key.to_s + '.' + extension.to_s].write(:file => path)
    FileUtils.rm(path)
    return obj.public_url.to_s
  end
  ###### Authenticate User with password
  def self.authenticate(email, password)
    @user = User.where('email' => email)
    if @user.count == 1 and Password::check(password, @user[0].password)
      @user[0]
    else
      nil
    end
  end
  ####### get employees under the following role #####
  def get_children
    users = User.find_by_sql('WITH RECURSIVE children AS ( select * from users where id = '+self.id.to_s+' UNION ALL select users.* from users,children where users.parent_id = children.id) select * from children')
    return users - [self]
  end
  ####### get roles of the employees under the current role
  def get_children_roles
    my_roles = self.roles.collect { |role| role.id  }.join(",")
    roles =  Role.find_by_sql('WITH RECURSIVE children AS ( select * from roles where id in ('+my_roles+') UNION ALL select roles.* from roles,children where roles.parent_id = children.id) select * from children') 
    unless self.roles.include?(Role.where(:name => 'admin')[0])
      roles = roles - self.roles
    end
    return roles
  end
  ####### check parent of the employee #######
  def check_parent parent_id
    if self.roles.include?(Role.where(:name => 'admin').first)
      return true
    end
    if User.find(parent_id).get_children.include?(self)
      return true
    else
      return false
    end
  end
  ######## check employees roles #####
  def check_child_role role_id
    if get_children_roles.include?(Role.find(role_id))
      return true
    else
      return false
    end
  end



  protected 
  ####### Update password of user #######
  def update_password
    self.password = Password::update(self.password) if self.password_changed?
  end
  ####### hash password of users after create ######
  def create_and_update_password
    self.roles.push(Role.where(:name => "open_user", :role_type => "U", :parent_id => 1).first_or_create)
    self.password = SecureRandom.base64(6).gsub(/[$=+\/]/,65.+(rand(25)).chr) if self.password.nil?
    self.password = Password::update(self.password)
  end
end
