class Notification < ActiveRecord::Base
	validates :template_id, presence: true
	belongs_to :user
	belongs_to :template, class_name: "Notifications::Template"
	after_create :check_user_mobile

	def self.get_user_notifications user_id
		self.includes(:template)
			.where(user_id: user_id)
			.order('id desc')
			.map{|notify| { id: notify.id, read: notify.read, templates: notify.template.name,
					notice_type: notify.notice_type, order_id: notify.order_id }
				}
	end

	def check_user_mobile
		return true
	end

	def self.push_not
		apn = P8push::Client.production
		token = "1456C9CB6C1E3C96E16416D5E3194F995544170F13282FB9DE1BD61FEC358FD4"
		notification = P8push::Notification.new(device: token)
		notification.alert = 'Hello, World!'
		notification.badge = 7
		notification.sound = 'sosumi.aiff'
		# notification.category = 'INVITE_CATEGORY'
		# notification.content_available = true
		# notification.mutable_content = true
		# notification.custom_data = { foo: 'bar' }
		apn.push(notification)
	end
end
