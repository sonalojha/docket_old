class Dvault < ActiveRecord::Base
	belongs_to :user, class_name: '::User'
	has_many :profiles, class_name: "::Dvaults::Profile"
	has_many :files, class_name: "::Dvaults::UserFile"
	before_create :create_and_update_pin
  	before_update :update_password
	
	####### Update password of user #######
  	def update_password
    	self.pin = Password::update(self.pin) if self.pin_changed?
	end
	def create_and_update_pin
	    self.pin = SecureRandom.base64(6).gsub(/[$=+\/]/,65.+(rand(25)).chr) if self.pin.nil?
	    self.pin = Password::update(self.pin)
  	end

  	def self.authenticate(id, pin)
		@vault = self.where(id: id)
		if @vault.count == 1 and Password::check(pin, @vault[0].pin)
		  @vault[0]
		else
		  nil
		end
	end

end
