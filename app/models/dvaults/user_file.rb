class Dvaults::UserFile < ActiveRecord::Base
	belongs_to :dvault, class_name: "::Dvault"
	belongs_to :profile, class_name: "::Dvaults::Profile"
	belongs_to :sub_category, class_name: "::Dvaults::SubCategory"
	
	
	def predictions dvault_id,keyword
		self.where(dvault_id: dvault_id)
			.where("lower(filename) like ?", "%#{keyword.to_s.downcase}%")
	end

	def self.create_zip files=nil
		# files = ['/Users/theonekedia/office/docket/db/templates/Trust_Deed.docx']
		files = ["https://docket-dev.s3.amazonaws.com/Assignment_Of_Bond_DebtDT201601042.docx"]
		path = './tmp/documents.zip'
		directory = './tmp/dvault/'
		# Zip::ZipOutputStream.open(path) do |zos|
	 #      files.each do |photo|
	 #        path = 'photo.docx'
	 #        zos.put_next_entry(path)
	 #        zos.write File.read(photo)
	 #      end
	 #    end
		Zip::File.open(path, Zip::File::CREATE) do |zipfile|
			files.each do |file|
				file_path = directory+file.split('/').last
				open(file_path, 'w') do |file1|
				  	file1 << open(file).read
		  			zipfile.add(File.basename(file), file1)
				end
				# open(file) do |f|
		  		# end
		  	end
		end
		# FileUtils.rm(path)
	end

	def self.upload path,extension
		s3 = AWS::S3.new
		key = SecureRandom.urlsafe_base64 32
		obj = s3.buckets[ENV['S3_BUCKET']].objects[(key+'.'+extension.to_s)].write(:file => path)
		FileUtils.rm(path)
		return obj.public_url.to_s
	end

	def generate_hash
		
	end
end
