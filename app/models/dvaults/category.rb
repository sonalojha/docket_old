class Dvaults::Category < ActiveRecord::Base
	has_many :sub_categories, class_name: "::Dvaults::SubCategory"
end
