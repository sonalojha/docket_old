class Dvaults::SubCategory < ActiveRecord::Base
	belongs_to :category, class_name: '::Dvaults::Category'
	has_many :user_files, class_name: "::Dvaults::UserFile"
end
