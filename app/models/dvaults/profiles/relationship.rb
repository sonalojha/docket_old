class Dvaults::Profiles::Relationship < ActiveRecord::Base
	has_many :profiles, class_name: '::Dvaults::Profile'
end
