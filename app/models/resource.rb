class Resource < ActiveRecord::Base
  validates :name, :presence => true
  
  has_and_belongs_to_many :roles, :join_table => "permissions"
  
  has_many :children, :class_name => "Resource", :foreign_key => "parent_id"
  belongs_to :parent, :class_name => "Resource"

  def get_children
    return Resource.find_by_sql("WITH RECURSIVE children AS ( select * from resources where id = #{self.id} UNION ALL select resources.* from resources,children where resources.parent_id = children.id) select * from children")
  end

  def get_parents
    return Resource.find_by_sql("WITH RECURSIVE parent AS ( select * from resources where id = #{self.id} UNION ALL select resources.* from resources,parent where resources.id = parent.parent_id) select * from parent")
  end

  def get_children_tree
    children = Hash.new { |hash, key| hash[key] = [] }
    get_children().each do |child|
      children[(child.parent_id rescue "root")].push(ResourceNode.new(child.id,child.name))
    end
    parent_id = self.parent_id rescue "root"
    children_tree = ResourceNode.new(self.id,self.name)
    children_tree.id = self.id
    children_tree.name = self.name
    children_tree.children = children[self.id]
    new_children = children[self.id]
    while new_children.count > 0
      temp_new_children = Array.new
      new_children.each do |child|
        child.children = children[child.id]
        temp_new_children |= children[child.id]
      end
      new_children = temp_new_children
    end
    return children_tree
  end


  def self.get_controller_actions controller,path,mod=nil
    if controller =~ /_controller.rb\z/ 
      cont = mod ? mod+controller.camelize.gsub(".rb","") : controller.camelize.gsub(".rb","")
      print cont+"\n"
      @controller = Resource.where(:name => cont, :parent_id => @root.id).first_or_create()
       (eval("#{cont}.new.methods") - ApplicationController.methods - Object.methods -  ApplicationController.new.methods + [:new]).sort.each {|met| 
           print met.to_s+"\n"
          Resource.where(:name => met.to_s, :parent_id => @controller.id).first_or_create() unless met.to_s[0] == '_'
        } 
    else
      self.check_for_more_entries controller,path,mod
    end
  end
  def self.check_for_more_entries controller,path,mod
    path = Dir.new(path.path.to_s + '/' + controller.to_s)
    mod = mod ? (mod+controller.capitalize + '::') : controller.capitalize + '::'
    entries = path.entries
    entries = entries - ['.', '..', '.DS_Store']
    entries.each do |entry|
      self.get_controller_actions entry,path,mod
    end
  end

  private
  def self.populate_resources()
    @root = Resource.where(:name => "root").first
    path = Dir.new("#{Rails.root}/app/controllers")
    controllers = path.entries
    controllers = controllers - ['.', '..', '.DS_Store', 'concerns']
    controllers.each do |controller|
      self.get_controller_actions controller,path
    end
  end
end

class ResourceNode
  def initialize(id,name)
    @id = id
    @name = name
    @children = []
  end
  attr_accessor :id, :name, :children
end
