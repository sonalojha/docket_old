require 'libreconv'
class Document < ActiveRecord::Base
	validates :name, presence: true#, uniqueness: true
	belongs_to :service
	belongs_to :status, class_name: 'Status'
   
	has_many :orders
	has_many :sections, class_name: 'Documents::Section', dependent: :destroy
	has_many :questions, class_name: '::Documents::Question', dependent: :destroy


	def self.predictions keyword
		self.where("lower(name) like ?", "%#{keyword.to_s.downcase}%")
	end 

	def tokenize   
		{:id => self.id, :name => "#{self.name}, #{self.category.name}"}
	end


	def self.upload path,file_name,order_id,mail=false
		s3 = AWS::S3.new
		key = file_name
		obj = s3.buckets[ENV['S3_BUCKET']].objects[key].write(:file => path)
		OrderJob.perform_later(order_id,'order_template',obj.public_url.to_s) if mail
		FileUtils.rm(path)
		return obj.public_url.to_s
	end

	def self.create_template order_id #answers,template_url,name='word_doc'
		## TODO fix autoload of lib files
		::Humanize # Loading the humanize module here
		::Libreconv # Loading the libreconv module here 
		order = Order.where(id: order_id)
		array = Documents::Answer.all.joins(:question)
					.where(order_id: order_id)
					.pluck('documents_questions.placeholder,documents_answers.body')
  		answers = Hash[array.map {|key, value| [key, value]}]
  		template_url = order.first.document.template_url
  		##### docx replace code Here ######
  		doc = ::DocxReplace::Doc.new(template_url, "#{Rails.root}/tmp/orders/template/")
		answers.each do |key,value|
			if key == 'MONTHLYRENT'
				value = value + '/-' + ' (RUPEES ' + value.to_i.humanize + ' only) '
			end
			if key.include? 'DATE'
				date = Date.parse(value)
				day = date.day.ordinalize
				value = date.strftime("#{day} %B, %Y")
				doc.replace("$$#{key.upcase}$$", value.upcase, true)
			end
			count = doc.document_content.scan("$$#{key.upcase}$$").count
			count.times do
				doc.document_content.force_encoding("UTF-8").sub!("$$#{key.upcase}$$", value.upcase)
			end
			#### doc.replace("$$#{key.upcase}$$", value.upcase, true)
		end
		tmp_file = Tempfile.new('word_tempate', "#{Rails.root}/tmp/orders/template/")
		name = order.first.document.name.split(' ').join('_')+ '_' + (order ? order.first.txnid.to_s : '') + '.docx'
		pdfname = order.first.document.name.split(' ').join('_')+ '_' + (order ? order.first.txnid.to_s : '') + '.pdf'
		new_file_path = "#{Rails.root}/tmp/orders/template/" + name
		pdf_file_path = "#{Rails.root}/tmp/orders/template/" + pdfname
		doc.commit(new_file_path)
		####### end ########
		if (doc.document_content.scan('$').count == 0)
			::Libreconv.convert("#{new_file_path}", "#{pdf_file_path}")
			s3_url = self.upload(new_file_path,name,order_id,false)
			pdf_url = self.upload(pdf_file_path,pdfname,order_id,true)
			FileUtils.rm(tmp_file.path)
			if s3_url && order
				order.first.draft_url = pdf_url
	  			order.first.draft_created = true
	  			order.first.track.drafted = true
	  			order.first.track.save!
	  			order.first.save!
	  		end
			return s3_url
		else
			s3_url = self.upload(new_file_path,name,order_id,false)
			OrderJob.perform_later(order_id,'error_template',s3_url)
			doc = nil
			puts 'document error'
		end
	end
end
