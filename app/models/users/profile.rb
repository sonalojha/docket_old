class Users::Profile < ActiveRecord::Base
	has_one :user, as: :profile
	belongs_to :country, class_name: '::Country'
	after_create :set_country

	def set_country
		if self.country_id == nil
			self.country_id = Country.where(name: 'India').first.id
			self.save!
		end
	end
end
