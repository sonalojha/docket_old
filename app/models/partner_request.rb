class PartnerRequest < ActiveRecord::Base
	validates :name, presence: true
	has_many :attachments, as: :file
	def self.upload path
		s3 = AWS::S3.new
		key = SecureRandom.urlsafe_base64 32
		obj = s3.buckets[ENV['S3_BUCKET']].objects[key].write(:file => path)
		return obj.public_url.to_s
	end
end
