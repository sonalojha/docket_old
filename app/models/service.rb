class Service < ActiveRecord::Base
	has_many :documents
	# has_and_belongs_to_many :states, :join_table => "state_services"
	validates :name, presence: true
	has_many :orders
	has_many :questions, class_name: '::Services::Question'
	has_many :answers, class_name: '::Services::Orders::Answer'

	accepts_nested_attributes_for :questions, :allow_destroy => true
end
 