class ContactRequest < ActiveRecord::Base
	validates :email, presence: true
	after_create :send_sales_email

	def send_sales_email
		SalesJob.perform_later(self.id)
	end
end
