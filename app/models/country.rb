class Country < ActiveRecord::Base
	has_many :states
	has_many :profiles, class_name: '::Users::Profile'
end
