class Order < ActiveRecord::Base
	# validates 	:user_id, presence: true
	validates 	:service_id, presence: true
	has_one  	:payment, class_name: '::Payment'
	belongs_to 	:document
	belongs_to 	:service
	belongs_to 	:coupon
	belongs_to 	:user
	has_many 	:answers, class_name: '::Services::Orders::Answer'
	has_many 	:automated_answers, class_name: '::Documents::Answer'
	belongs_to 	:delivery_partner, class_name: '::Orders::Delivery::Partner'
	belongs_to 	:status, class_name: '::Orders::Status'
	belongs_to 	:delivery_status, class_name: '::Orders::DeliveryStatus'
	has_many 	:attachments, as: :file
	after_create :set_attributes
	belongs_to 	:address, class_name: "::Address"
	has_one 	:track, class_name: "Orders::Track"
	has_one 	:detail, class_name: "Orders::Detail"
	belongs_to  :platform 
	after_save	:update_delivery
	has_one		:feedback, class_name: 'Orders::Feedback'

	accepts_nested_attributes_for :attachments, :allow_destroy => true

	CHECKSUM_FIELDS = [ :txnid, :amount, :productinfo, :firstname, :email, :udf1, :udf2, :udf3, :udf4,
                        :udf5, :udf6, :udf7, :udf8, :udf9, :udf10 ]


	def set_attributes
		self.txnid = 'DT' + Date.today.strftime('%Y%m%d') + "#{self.id}"
		self.status_id = ::Orders::Status.where(name: 'INCOMPLETE').first.id
		automation_service_list = Service.where(name: ['Affidavit','Bond', 'Deed', 'Agreement']).pluck(:id)
		if !(automation_service_list.include? self.service_id) || (self.document.automated == false)
			self.draft_required = false
		end
		self.delivery_partner_id = ::Orders::Delivery::Partner.all.first.id
		self.save!
		track = Orders::Track.new(verified: false, order_id: self.id)
		track.save!
	end
 
	def self.generate_payu_url id,address_id=nil
		query = pay_u_api(id,address_id)
		get_url(ENV['payu_key'], ENV['payu_salt'], query)
	end

	def self.get_url(key, salt, options = {})
      @key, @salt, @options = key, salt, options
      form_fields
    end

    def self.form_fields
      @options.merge(:hash => generate_checksum)
    end

    def self.generate_checksum
      checksum_payload_items = CHECKSUM_FIELDS.map { |field| @options[field] }
      checksum(@key, @salt, checksum_payload_items )
    end

	def self.checksum(merchant_id, secret_key, payload_items )
	    Digest::SHA512.hexdigest([merchant_id, *payload_items, secret_key].join("|"))
	end

	def self.pay_u_api id,udf1
		order = self.find(id)
		query = {
			key: ENV['payu_key'],
			txnid: order.txnid, 
			amount: (order.final_amount.to_f - order.discount.to_f*1.18).round(2).to_s, 
			productinfo: order.product,
			firstname: order.detail.name, 
			email: order.detail.email,
			phone: order.detail.mobile, 
			surl: ENV['surl'], 
			furl: ENV['furl'],
			service_provider: 'payu_paisa'# not required in test ,
			# curl: ENV['curl'], 
			# udf1: udf1 
			}
	    query
	end

	def self.generate_url(query)
		url = ENV['payu_url']
		response = Net::HTTP.post_form(URI.parse(url), query)
		
	end
    # CHECKSUM_FIELDS = [ :txnid, :amount, :productinfo, :firstname, :email]#, :udf1, :udf2, :udf3, :udf4,
                        # :udf5, :udf6, :udf7, :udf8, :udf9, :udf10 ]

    # def sanitize_fields
    #   [:address1, :address2, :city, :state, :country, :productinfo, :email, :phone].each do |field|
    #     @options[field].gsub!(/[^a-zA-Z0-9\-_@\/\s.]/, '') if @options[field]
    #   end
    # end

    def self.is_valid_transaction? txnid,amount
    	order = self.where(txnid: txnid).first
    	if (order.status.name == 'INCOMPLETE') && ((order.final_amount.to_f.round(2) - order.discount.to_f.round(2)) == amount.to_f.round(2) )
    		return true
    	else
    		return false
    	end
    end

    def self.update_status txnid,status
    	order = self.where(txnid: txnid).first
		if status == 'success'
			order.status_id = ::Orders::Status.where(name: 'INPROGRESS').first.id
			# OrderJob.perform_later(order.id,'order_invoice')
        	# SmsJob.perform_later(order.user_id,nil,'order_invoice',order.id)
        else
			order.status_id = ::Orders::Status.where(name: 'INCOMPLETE').first.id
        end
		order.save!
    end

    def self.upload path,key=false,mime_type=nil
		s3 = AWS::S3.new
		key = SecureRandom.urlsafe_base64 32 if !key
		key = key + '.' + mime_type if mime_type
		obj = s3.buckets[ENV['S3_BUCKET']].objects[key].write(:file => path)
		FileUtils.rm(path)
		return obj.public_url.to_s
	end

	def update_delivery
		if self.delivery_status_id == ::Orders::DeliveryStatus.where(name: 'DISPATCHED').first.id
			OrderJob.perform_later(self.id,'order_dispatch')
		end
	end
end
