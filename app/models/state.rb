class State < ActiveRecord::Base
	has_many :cities
	has_many :orders
	has_many :documents
	belongs_to :country
	# has_and_belongs_to_many :services, :join_table => "state_services"
	validates :name, presence: true
end
