module HomeHelper
	
	def check_current_time
		time = Time.now.hour
		if time < 9 
			return 1
		elsif time < 12
			return 2
		elsif time < 15
			return 3
		elsif time < 18
			return 4
		else
			return 5
		end
	end

	def slot_details slot_number
		case slot_number
			when 1
				return "9AM - 12PM"
			when 2
				return "12PM - 3PM"
			when 3
				return "3PM - 6PM"
			when 4
				return "6PM - 9PM"
		end
	end

	def check_dates number
		if Date.today.strftime('%a') == 'Sun'
			number += number
		end 
		date = Date.today + number.day
		return date.strftime('%d/%m/%Y')
	end

	def get_time_slots starting_slot
		if(starting_slot == 1)
			days_slots = {	
				"Today "+ slot_details(1) => "#{check_dates(0)}",
				"Today "+ slot_details(2) => "#{check_dates(0)}",
				"Today "+ slot_details(3) => "#{check_dates(0)}",
				"Today "+ slot_details(4) => "#{check_dates(0)}"
				}
		elsif(starting_slot == 2)
			days_slots = {	
				"Today "+ slot_details(2) => "#{check_dates(0)}",
				"Today "+ slot_details(3) => "#{check_dates(0)}",
				"Today "+ slot_details(4) => "#{check_dates(0)}",
				"Tomorrow "+ slot_details(1) => "#{check_dates(1)}"
				}
		elsif(starting_slot == 3)
			days_slots = {	
				"Today "+ slot_details(3) => "#{check_dates(0)}",
				"Today "+ slot_details(4) => "#{check_dates(0)}",
				"Tomorrow "+ slot_details(1) => "#{check_dates(1)}",
				"Tomorrow "+ slot_details(2) => "#{check_dates(1)}"
				}
		elsif(starting_slot == 4)
			days_slots = {	
				"Today "+ slot_details(4) => "#{check_dates(0)}",
				"Tomorrow "+ slot_details(1) => "#{check_dates(1)}",
				"Tomorrow "+ slot_details(2) => "#{check_dates(1)}",
				"Tomorrow "+ slot_details(3) => "#{check_dates(1)}"
				}
		elsif(starting_slot == 5)
			days_slots = {	
				"Tomorrow "+ slot_details(1) => "#{check_dates(1)}",
				"Tomorrow "+ slot_details(2) => "#{check_dates(1)}",
				"Tomorrow "+ slot_details(3) => "#{check_dates(1)}",
				"Tomorrow "+ slot_details(4) => "#{check_dates(1)}"
				}
		end
		return days_slots
	end

	def time_slots
		starting_slot = check_current_time
		get_time_slots(starting_slot)
	end
end
