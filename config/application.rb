require File.expand_path('../boot', __FILE__)
require 'csv'
require 'rails/all'


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Docket
  class Application < Rails::Application
    config.middleware.use Mobvious::Manager
    config.autoload_paths += %W(#{config.root}/lib)
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.encoding = "utf-8"
    # config.assets.paths << Rails.root.join("app", "assets", "fonts",'images')
    config.filter_parameters += [:password,:pin]
    config.assets.paths << Rails.root.join("app", "assets", 'images', 'fonts')
    config.assets.precompile += %w( employee.css application.css campaign.css campaign.js employee.js application.js website.css website.js)
    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    config.time_zone = 'Mumbai'
    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.active_job.queue_adapter = :resque
    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    config.paths["config/routes.rb"].concat(Dir[Rails.root.join("config/routes/*.rb")])
  end
end
