$redis = Redis.new(:host => ENV['redis_host'], :port => ENV['redis_port'])
Resque.redis = $redis
Resque.after_fork = Proc.new { 
	ActiveRecord::Base.establish_connection 
}
# Resque.schedule = YAML.load_file(File.join(RAILS_ROOT, 'resque_schedule.yml'))