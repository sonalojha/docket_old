# CarrierWave.configure do |config|
# 	config.fog_credentials = {
# 		:provider               => 'AWS',
# 		:aws_access_key_id      => ENV['S3_KEY'],
# 		:aws_secret_access_key  => ENV['S3_SECRET'],
# 		:region                 => ENV['S3_REGION'],
# 		:path_style 			=> true
# 	}
# 	config.fog_directory  = ENV['S3_BUCKET']
# 	config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}
# 	config.fog_public = false
# 	config.storage = :fog
# end
AWS.config(
  :access_key_id => ENV['S3_KEY'],
  :secret_access_key => ENV['S3_SECRET']
)