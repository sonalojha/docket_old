Docket::Application.routes.draw do
	scope "/admin" do
  		resources :apps
  		resources :coupons
		namespace :orders do
		    resources :delivery_statuses
			resources :feedbacks
		    resources :statuses
			resources :tracks
		    resources :details
		    namespace :delivery do
		    	resources :partners
		    end
		end 
	
		match 'dbmetrics' 				=> 'dbmetrics#index', via: [:put,:post,:get]
		match 'dbmetrics/service_order_count' => 'dbmetrics#service_order_count', via: [:put, :post, :get]
		match 'orders/:id/invoice' 		=> 'orders#order_invoice', as: 'order_invoice', via: [:put,:post,:get]
		match 'orders/:id/details' 		=> 'orders#order_details', as: 'order_details', via: [:put,:post,:get]
		match 'orders/update_status' 	=> 'orders#update_status', via: [:put,:post,:get]
		match 'orders/update_delivery' 	=> 'orders#update_delivery', via: [:put,:post,:get]
		match 'orders/search'			=> 'orders#filter_order', via: [:put,:post,:get]
		match 'orders/search_by'		=> 'orders#search_by',  via: [:put,:post,:get]
		match 'orders/edit_draft' 	    => 'orders#edit_draft', via: [:put,:post,:get]
		match 'orders/create_draft' 	=> 'orders#create_draft', as: 'orders_draft', via: [:put,:post,:get]
		match 'orders/upload_soft_copy' => 'orders#upload_soft_copy', via: [:put,:post,:get]
		match 'orders/get_track_details'=> 'orders#get_track_details', via: [:put,:post,:get]
		match 'users/search'            => 'users#search', via: [:put, :post, :get]
		match 'users/search_by'         => 'users#search_by', via: [:put, :post, :get]
		match 'users/:id/orders'        => 'users#user_orders', as: 'user_orders', via: [:put,:post,:get]
		match 'user_verification'		=> 'user_verification#index',  via: [:put,:post,:get]
		match 'user_verification/verify_profile'		=> 'user_verification#verify_profile',  via: [:put,:post,:get]
		match 'user_verification/details_incorrect'		=> 'user_verification#details_incorrect',  via: [:put,:post,:get]
		match 'orders/order_email' 		=> 'orders#order_email', via: [:put,:post,:get]
		match 'orders/order_sms'		=> 'orders#order_sms', via: [:put,:post,:get]
		match 'orders/order_notification'	=> 'orders#order_notification', via: [:put,:post,:get]

		
		resources :orders
		resources :localities 
		resources :statuses
		resources :states
		resources :cities
		resources :countries
		resources :permissions
		resources :resources
		# resources :employees
		resources :authentications
		resources :addresses
  		namespace :payments do
    		resources :statuses
  		end
  		resources :attachments
  		namespace :users do
    		resources :profiles
  		end
  		namespace :payments do
		    resources :paytms
		    resources :icicis
  		end
  		resources :payments
  		resources :users
		resources :contact_requests
		resources :partner_requests
		namespace :documents do
			namespace :questions do
				resources :response_types
			end
		end
		namespace :documents do
			resources :statuses
			resources :sections do
				resources :question_groups
			end
			resources :questions do
				resources :options
			end
			resources :answers
		end
		match '/documents/search'    => 'documents#search',  via: [:put,:post,:get]
		match '/documents/search_by' => 'documents#search_by',  via: [:put,:post,:get]
		match '/documents/get_questions' => 'documents#get_questions',  via: [:put,:post,:get]
		resources :documents
		resources :device_registrations
		match '/' => 'employees#index', via: [:post, :get]
		match "employees/logout" => "employees#employee_logout", :as => "logout_employee", via: [:post, :get]
		match "employees/sign_in" => "employees#employee_sign_in", :as => "employee_sign_in", via: [:post, :get]
		match "employees/login" => "employees#employee_login", :via => [:post]
		match "employees/:id/edit_role" => "employees#edit_role", via: [:post, :get]
		match "employees/:id/add_role" => "employees#add_role", :via => [:put]
		match "employees/:id/remove_role" => "employees#remove_role", :via => [:put]
		resources :employees do
			resources :roles
		end

		match "roles/:id/edit_column_access"              => "roles#edit_column_access", :as => 'edit_column_access', via: [:post, :get]
		match "roles/:id/toggle_column_access"            => "roles#toggle_column_access", :via => [:put]
		match "roles/:id/edit_permission"                 => "roles#edit_permission", via: [:post, :get]
		match "roles/:id/update_permission"               => "roles#update_permission", via: [:post, :get]
		post  "roles/:id/add_permission/:resource_id"     => "roles#add_permission", via: [:post, :get]
		post  "roles/:id/delete_permission/:resource_id"  => "roles#delete_permission", via: [:post, :get]
		resources :roles do
			resources :roles_column_accesses
		end
		resources :users_roles
		namespace :services do
			namespace :orders do
		    	resources :answers
			end
			resources :questions
		end
		resources :services
		match 'services/:id/add_question' => "services#add_question", as: 'add_questions_service', via: [:post,:get,:put]
		namespace :dvaults do
			namespace :profiles do
				resources :relationships
			end
			resources :user_files
			resources :sub_categories
			resources :categories
			resources :profiles
		end
		resources :dvaults
		resources :verifications
		namespace :notifications do
    		resources :templates
  		end
  		resources :promotions
		resources :notifications
		resources :platforms
	end
end