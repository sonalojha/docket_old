Docket::Application.routes.draw do
  namespace :api do
	namespace :v0 do
		# ############ MOBILE API ##############
		match 'mobile/register' 					=> 'mobile_api#register',			via: [:put, :get, :post] 
		match 'mobile/signup' 						=> 'mobile_api#signup', 			via: [:put, :get, :post] # create a new user
		match 'mobile/login' 						=> 'mobile_api#login', 				via: [:put, :get, :post] # create a new user
		match 'mobile/update_user' 					=> 'mobile_api#update_user', 		via: [:put, :get, :post] # create a new user
		match 'mobile/profile_data' 				=> 'mobile_api#profile_data', 		via: [:put, :get, :post] # create a new user
		match 'mobile/send_notification' 			=> 'mobile_api#send_notifications', via: [:put, :get, :post] # create a new user
		match 'mobile/upload_id_proof' 				=> 'mobile_api#upload_id_proof', 	via: [:put, :get, :post] # create a new user
		
		match 'mobile/services'						=> 'mobile_api#service_all',		via: [:put, :get, :post]
		match 'mobile/search' 						=> 'mobile_api#search', 			via: [:put, :get, :post] # create a new user
		
		match 'mobile/add_address' 					=> 'mobile_api#add_address', 		via: [:put, :get, :post] # create a new user
		match 'mobile/update_address' 				=> 'mobile_api#update_address', 	via: [:put, :get, :post] # create a new user
		match 'mobile/delete_address' 				=> 'mobile_api#delete_address', 	via: [:put, :get, :post] # create a new user
		match 'mobile/get_address' 					=> 'mobile_api#get_address', 		via: [:put, :get, :post] # create a new user
		
		match 'mobile/mobile_verify' 				=> 'mobile_api#verify_mobile', 		via: [:put, :get, :post] # create a new user
		match 'mobile/verify_otp' 					=> 'mobile_api#verify_otp', 		via: [:put, :get, :post] # create a new user
		match 'mobile/verify_email' 				=> 'mobile_api#verify_email', 		via: [:put, :get, :post] # create a new user
		# match 'mobile/add_mobile' 					=> 'mobile_api#add_mobile', 		via: [:put, :get, :post] # create a new user
		# match 'mobile/mobile_exists' 				=> 'mobile_api#mobile_exists', 		via: [:put, :get, :post] # create a new user
		match 'mobile/change_phone' 				=> 'mobile_api#change_phone',		via: [:put, :get, :post] # create a new user
		match 'mobile/country_code' 				=> 'mobile_api#country_code',		via: [:put, :get, :post] # create a new user
		
		match 'mobile/forgot_password' 				=> 'mobile_api#forgot_password', 	via: [:put, :get, :post] # create a new user
		match 'mobile/change_password' 				=> 'mobile_api#change_password',	via: [:put, :get, :post] # create a new user
		match 'mobile/request_call' 				=> 'mobile_api#request_call',		via: [:put, :get, :post] # create a new user
		match 'mobile/update_order' 				=> 'mobile_api#update_order',		via: [:put, :get, :post] # create a new user
		
		match 'mobile/create_order' 				=> 'order_api#create_order',		via: [:put, :get, :post] # create a new user
		match 'mobile/validate_coupon' 				=> 'order_api#validate_coupon',		via: [:put, :get, :post] # create a new user
		match 'mobile/remove_coupon' 				=> 'order_api#remove_coupon',		via: [:put, :get, :post] # create a new user
		match 'mobile/delete_order' 				=> 'order_api#delete_order',		via: [:put, :get, :post] # create a new user
		match 'mobile/order_list' 					=> 'order_api#order_list',			via: [:put, :get, :post] # create a new user
		match 'mobile/delivery_info' 				=> 'order_api#delivery_info',		via: [:put, :get, :post] # create a new user
		match 'mobile/soft_copy' 					=> 'order_api#get_soft_copy',		via: [:put, :get, :post] # create a new user
		match 'mobile/make_payment' 				=> 'order_api#make_payment',		via: [:put, :get, :post] # create a new user
		
		match 'mobile/:service_id/question'			=> 'question_api#service_questions',via: [:put, :get, :post]
		match '/payu/:status/callback'    			=> 'payment_api#payu_callback', 	via: [:put, :get, :post]

		######## Automation ####### 
		match 'mobile/get_questions'          		=> 'automation_api#get_questions',  via: [:put, :get, :post]
		match 'mobile/order_answers'          		=> 'automation_api#answer_capture', via: [:put, :get, :post]
		match 'mobile/track'          				=> 'order_api#order_tracking',		via: [:put, :get, :post]
		match 'mobile/track_list'          			=> 'order_api#tracking_list',		via: [:put, :get, :post]
		match 'mobile/confirm_draft'          		=> 'mobile_api#confirm_draft',		via: [:put, :get, :post]

		########### Dvault Api #########
		
		match 'mobile/dvault_check_active' 			=> 'dvault_api#check_dvault_active',via: [:put, :get, :post]
		match 'mobile/dvault_create_pin' 			=> 'dvault_api#create_pin', 		via: [:put, :get, :post]
		match 'mobile/dvault_login' 				=> 'dvault_api#login', 				via: [:put, :get, :post]
		match 'mobile/dvault_forgot_pin' 			=> 'dvault_api#forgot_pins', 		via: [:put, :get, :post]
		
		match 'mobile/dvault_categories'			=> 'dvault_api#categories',			via: [:put, :get, :post]
		match 'mobile/dvault_files' 				=> 'dvault_api#get_files', 			via: [:put, :get, :post]
		
		match 'mobile/dvault_upload_file' 			=> 'dvault_api#upload_file', 		via: [:put, :get, :post]
		match 'mobile/dvault_edit_file' 			=> 'dvault_api#edit_file', 			via: [:put, :get, :post]
		
		match 'mobile/dvault_email_zip' 			=> 'dvault_api#email_zip', 			via: [:put, :get, :post]
		match 'mobile/dvault_search_doc' 			=> 'dvault_api#search_doc', 		via: [:put, :get, :post]
		match 'mobile/dvault_send_file' 			=> 'dvault_api#send_file', 			via: [:put, :get, :post]
		match 'mobile/dvault_delete_file' 			=> 'dvault_api#delete_file', 		via: [:put, :get, :post]
		match 'mobile/dvault_reset_pin_request' 	=> 'dvault_api#reset_pin_request', 	via: [:put, :get, :post]
		match 'mobile/dvault_reset_pin_otp' 		=> 'dvault_api#pin_reset_otp', 		via: [:put, :get, :post]
		match 'mobile/dvault_reset_pin' 			=> 'dvault_api#pin_reset', 			via: [:put, :get, :post]

		########## NEW API #######
		match 'mobile/recent_orders' 				=> 'order_api#recent_orders',			via: [:put, :get, :post] # create a new user
		match 'mobile/pending_orders' 				=> 'order_api#current_pending_order',	via: [:put, :get, :post] # create a new user
		match 'mobile/current_order_tracking' 		=> 'order_api#current_order_tracking',	via: [:put, :get, :post] # create a new user
		match 'mobile/generate_hash'		 		=> 'order_api#generate_payu_hash',		via: [:put, :get, :post] # create a new user
		match 'mobile/s3_url'		 				=> 'order_api#presigned_s3_url',		via: [:put, :get, :post] # create a new user
		######### NOTIFICATION #######
		match 'mobile/notifications'				=> 'notification_api#notifications',via: [:put,:get,:post]
		match 'mobile/read_notification'			=> 'notification_api#read_notification', via: [:put,:get,:post]

		######### PROMOTION #######
		match 'mobile/promotions'					=> 'promotion_api#promotions', 		via: [:put,:get,:post]
		match 'mobile/check_promotion'				=> 'promotion_api#check_promotion', via: [:put,:get,:post]	
	end
  end  
  namespace :api do
	namespace :v1 do
		# ############ MOBILE API ##############
		match 'mobile/register' 					=> 'mobile_api#register',			via: [:put, :get, :post] 
		match 'mobile/signup' 						=> 'mobile_api#signup', 			via: [:put, :get, :post] 
		match 'mobile/login' 						=> 'mobile_api#login', 				via: [:put, :get, :post] 
		match 'mobile/login_otp' 					=> 'mobile_api#login_otp', 			via: [:put, :get, :post] 
		match 'mobile/update_user' 					=> 'mobile_api#update_user', 		via: [:put, :get, :post] 
		match 'mobile/profile_data' 				=> 'mobile_api#profile_data', 		via: [:put, :get, :post] 
		match 'mobile/send_notification' 			=> 'mobile_api#send_notifications', via: [:put, :get, :post] 
		match 'mobile/upload_id_proof' 				=> 'mobile_api#upload_id_proof', 	via: [:put, :get, :post] 
		
		match 'mobile/services'						=> 'mobile_api#service_all',		via: [:put, :get, :post]
		match 'mobile/search' 						=> 'mobile_api#search', 			via: [:put, :get, :post] 
		
		match 'mobile/add_address' 					=> 'mobile_api#add_address', 		via: [:put, :get, :post] 
		match 'mobile/update_address' 				=> 'mobile_api#update_address', 	via: [:put, :get, :post] 
		match 'mobile/delete_address' 				=> 'mobile_api#delete_address', 	via: [:put, :get, :post] 
		match 'mobile/get_address' 					=> 'mobile_api#get_address', 		via: [:put, :get, :post] 
		
		match 'mobile/mobile_verify' 				=> 'mobile_api#verify_mobile', 		via: [:put, :get, :post] 
		match 'mobile/verify_otp' 					=> 'mobile_api#verify_otp', 		via: [:put, :get, :post] 
		match 'mobile/verify_email' 				=> 'mobile_api#verify_email', 		via: [:put, :get, :post] 
		# match 'mobile/add_mobile' 					=> 'mobile_api#add_mobile', 		via: [:put, :get, :post] 
		# match 'mobile/mobile_exists' 				=> 'mobile_api#mobile_exists', 		via: [:put, :get, :post] 
		match 'mobile/change_phone' 				=> 'mobile_api#change_phone',		via: [:put, :get, :post] 
		match 'mobile/country_code' 				=> 'mobile_api#country_code',		via: [:put, :get, :post] 
		
		match 'mobile/forgot_password' 				=> 'mobile_api#forgot_password', 	via: [:put, :get, :post] 
		match 'mobile/change_password' 				=> 'mobile_api#change_password',	via: [:put, :get, :post] 
		match 'mobile/request_call' 				=> 'mobile_api#request_call',		via: [:put, :get, :post] 
		# match 'mobile/update_order' 				=> 'mobile_api#update_order',		via: [:put, :get, :post] 
		
		match 'mobile/create_order' 				=> 'order_api#create_order',		via: [:put, :get, :post] 
		match 'mobile/update_order' 				=> 'order_api#update_order',		via: [:put, :get, :post] 
		match 'mobile/update_order_address' 	   	=> 'order_api#update_order_address',via: [:put, :get, :post] 
		match 'mobile/validate_coupon' 				=> 'order_api#validate_coupon',		via: [:put, :get, :post] 
		match 'mobile/remove_coupon' 				=> 'order_api#remove_coupon',		via: [:put, :get, :post] 
		match 'mobile/delete_order' 				=> 'order_api#delete_order',		via: [:put, :get, :post] 
		match 'mobile/order_list' 					=> 'order_api#order_list',			via: [:put, :get, :post] 
		match 'mobile/delivery_info' 				=> 'order_api#delivery_info',		via: [:put, :get, :post] 
		match 'mobile/soft_copy' 					=> 'order_api#get_soft_copy',		via: [:put, :get, :post] 
		match 'mobile/make_payment' 				=> 'order_api#make_payment',		via: [:put, :get, :post] 
		match 'mobile/order_status'					=> 'order_api#paytm_response_status',via: [:put, :get, :post]
		
		match 'mobile/:service_id/question'			=> 'question_api#service_questions',via: [:put, :get, :post]
		match '/payu/:status/callback'    			=> 'payment_api#payu_callback', 	via: [:put, :get, :post]

		######## Automation ####### 
		match 'mobile/get_questions'          		=> 'automation_api#get_questions',  via: [:put, :get, :post]
		match 'mobile/order_answers'          		=> 'automation_api#answer_capture', via: [:put, :get, :post]
		match 'mobile/track'          				=> 'order_api#order_tracking',		via: [:put, :get, :post]
		match 'mobile/track_list'          			=> 'order_api#tracking_list',		via: [:put, :get, :post]
		match 'mobile/confirm_draft'          		=> 'mobile_api#confirm_draft',		via: [:put, :get, :post]

		########### Dvault Api #########
		
		match 'mobile/dvault_check_active' 			=> 'dvault_api#check_dvault_active',via: [:put, :get, :post]
		match 'mobile/dvault_create_pin' 			=> 'dvault_api#create_pin', 		via: [:put, :get, :post]
		match 'mobile/dvault_login' 				=> 'dvault_api#login', 				via: [:put, :get, :post]
		match 'mobile/dvault_forgot_pin' 			=> 'dvault_api#forgot_pins', 		via: [:put, :get, :post]
		
		match 'mobile/dvault_categories'			=> 'dvault_api#categories',			via: [:put, :get, :post]
		match 'mobile/dvault_files' 				=> 'dvault_api#get_files', 			via: [:put, :get, :post]
		
		match 'mobile/dvault_upload_file' 			=> 'dvault_api#upload_file', 		via: [:put, :get, :post]
		match 'mobile/dvault_edit_file' 			=> 'dvault_api#edit_file', 			via: [:put, :get, :post]
		
		match 'mobile/dvault_email_zip' 			=> 'dvault_api#email_zip', 			via: [:put, :get, :post]
		match 'mobile/dvault_search_doc' 			=> 'dvault_api#search_doc', 		via: [:put, :get, :post]
		match 'mobile/dvault_send_file' 			=> 'dvault_api#send_file', 			via: [:put, :get, :post]
		match 'mobile/dvault_delete_file' 			=> 'dvault_api#delete_file', 		via: [:put, :get, :post]
		match 'mobile/dvault_reset_pin_request' 	=> 'dvault_api#reset_pin_request', 	via: [:put, :get, :post]
		match 'mobile/dvault_reset_pin_otp' 		=> 'dvault_api#pin_reset_otp', 		via: [:put, :get, :post]
		match 'mobile/dvault_reset_pin' 			=> 'dvault_api#pin_reset', 			via: [:put, :get, :post]

		########## NEW API #######
		match 'mobile/recent_orders' 				=> 'order_api#recent_orders',			via: [:put, :get, :post] # create a new user
		match 'mobile/pending_orders' 				=> 'order_api#current_pending_order',	via: [:put, :get, :post] # create a new user
		match 'mobile/current_order_tracking' 		=> 'order_api#current_order_tracking',	via: [:put, :get, :post] # create a new user
		match 'mobile/generate_hash'		 		=> 'order_api#generate_payu_hash',		via: [:put, :get, :post] # create a new user
		match 'mobile/paytm_hash'		 			=> 'payment_api#paytm_hash',			via: [:put, :get, :post] # create a new user
		match 'mobile/paytm_verify'		 			=> 'payment_api#paytm_verify',			via: [:put, :get, :post] # create a new user

		match 'mobile/s3_url'		 				=> 'order_api#presigned_s3_url',		via: [:put, :get, :post] # create a new user
		######### NOTIFICATION #######
		match 'mobile/notifications'				=> 'notification_api#notifications',	via: [:put,:get,:post]
		match 'mobile/read_notification'			=> 'notification_api#read_notification',via: [:put,:get,:post]

		######### PROMOTION #######
		match 'mobile/promotions'					=> 'promotion_api#promotions', 		via: [:put,:get,:post]
		match 'mobile/check_promotion'				=> 'promotion_api#check_promotion', via: [:put,:get,:post]	
	end
  end  
end


